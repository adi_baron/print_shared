require "print_shared/engine"

module PrintShared
  # Your code goes here...

  class Engine < ::Rails::Engine
    # config.assets.precompile += %w( my_component_manifest.js )

    # config.assets.paths << File.expand_path("./../print_shared/assets", __FILE__)
    config.assets.paths << File.expand_path("./../print_shared/assets", __FILE__)

    initializer "print_shared.assets.precompile" do |app|
      app.config.assets.precompile << "print_shared_manifest.js"
    end

  end
  
end
