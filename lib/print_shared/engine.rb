module PrintShared
  class Engine < ::Rails::Engine
    isolate_namespace PrintShared

    initializer 'PrintShared', before: :load_config_initializers do
      
      # Rails.application.config.i18n.load_path += Dir["#{config.root}/config/locales/**/*.yml"]        
      # config.i18n.load_path += Dir["#{config.root}/locales/**/*.yml"]

      # config.i18n.load_path += Dir[config.root.join('config', 'locales', '*.{yml}')]
      config.i18n.load_path += Dir["#{config.root.to_s}/config/locales/**/*.{rb,yml}"]

    end
  end
end
