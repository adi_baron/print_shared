$( document ).on('ready', function(ev) {	

	$( document ).on('click', ".mobile-covers-selector .cover-selector img", function(ev) {
		ev.preventDefault();
		ev.stopPropagation();

		$(".mobile-covers-selector .cover-selector img").removeClass("selected");
		$(this).addClass("selected");
	});

	$( document ).on('click', ".mobile-covers-selector .mobile-cover-selector-submit", function(ev) {
		ev.preventDefault();
		ev.stopPropagation();

		var template_id	= $(".mobile-covers-selector .cover-selector img.selected").closest(".cover-selector").attr("data-template-id");
		var page_id = $(".mobile-covers-selector").attr("data-page-id");
		var left_or_right = $(".mobile-covers-selector").attr("data-left-or-right");


		// var img_container_count = Object.keys( g_all_album_pages[page_id].toJSON( {
		// 	imgs_only:true, 
		// 	used_img:false,
		// 	left_or_right: left_or_right,
		// }) ).length

		var template = g_auto_template.get_template_by_id(template_id, true);

		if (template) {
			// debugger;
			g_auto_template.current_page_from_template(template, {
				add_margin: 		false,
				left_or_right: 	left_or_right,

				page_id:        page_id, // used only for mobile
				album_page:     pages_page_container(page_id), // used only for mobile
				selector:       $(".page[data-page-id='" + page_id + "']").closest(".page-previewer")[0],
				editor:         false,
			});

		} else {
			console.error("template not found: " + template_id);
		}

		do_back_btn();
	});

});