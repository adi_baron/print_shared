/* removed for mobile and desktop integration for photoprint


var g_pages_last_save_copy = {};
var g_pages_timed_save_changes_interval = null;

function mobile_start_timed_save() {
  g_pages_last_save_copy = {};

  var iterval_period_in_seconds = 15;
	
	// do not run for photoprint
  if (g_album.type != "PhotoPrint") {
		if (g_pages_timed_save_changes_interval == null) {
			g_pages_timed_save_changes_interval = setInterval(mobile_save_all_changed, iterval_period_in_seconds*1000);
		}
	}

}

function mobile_stop_timed_save() {
	g_pages_last_save_copy = {};

  if (g_pages_timed_save_changes_interval) {
		clearInterval( g_pages_timed_save_changes_interval );
		g_pages_timed_save_changes_interval = null;
	}
}

function mobile_update_page(page_id) {
	
	console.warn("moved to periodic multiple save");
	return;

	var page = pages_page_container(page_id);

	var page_data = page.toJSON( { } );

	var path = "/products/" + g_product_id + "/pages/" + page_id;
	var url = g_editor_url + path;

	$.ajax({
    url: url,
    method: "PUT",
    async: true,
    cache: false,
    contentType: 'application/json',

	  data: JSON.stringify( {
	  	data: page_data,
	  	// authenticity_token: window._token
	  }),

	  success: function(data) {
	  	// saving last copy (this is a deep copy - see implementation if needed in page.js & base_el.js of picndo_print)
	  	g_pages_last_save_copy[ this.page_id ] = this.page_data;
	  	
	  	console.log("page updated successfully");
		
		}.bind( { page_id: page_id, page_data: page_data } ),
		error: function() {
			console.error("failed updateing page !!!"); 
			console.error("failed updateing page !!!");
			console.error("failed updateing page !!!"); 
		}

	}).done(function( msg ) {
		
  });

}

//*********************
//	     TODO ...
//*********************  

// use timeout to call this function every period (30 seconds?)
// - when doing mobile_update_page - save all previous save (do deep copy)
// - do campare with current page data and if change add to array of changed page ids
// - then call mobile_update_pages with the changed array
function mobile_update_pages(page_ids_arr, cb) {
	var data = {}
	for (var i=0; i<page_ids_arr.length; i++) {
		
		var page_id = page_ids_arr[i];
		var page = pages_page_container(page_id);
		var page_data = page.toJSON( { } );

		if (page_data) { // does not add if null
			data[page_id] = page_data;
		}

	}

	if (Object.keys(data).length > 0) {

		var path = "/products/" + g_product_id + "/pages/update_multiple";
		var url = g_editor_url + path;

		$.ajax({
	    url: url,
	    method: "PUT",
	    async: true,
	    cache: false,
	    contentType: 'application/json',
		  data: JSON.stringify( {
		  	data: data,
		  	// authenticity_token: window._token
		  }),

		  success: function(data) {
		  	
		  	// saving last copy (this is a deep copy - see implementation if needed in page.js & base_el.js of picndo_print)
		  	var page_ids = Object.keys(this.data);
		  	for (var i=0; i<page_ids.length; i++) {
		  		
		  		var page_id = page_ids[i];
		  		var page_data = this.data[ page_id ];
		  		
		  		g_pages_last_save_copy[ page_id ] = page_data;
		  	}
		  	
		  	console.log("page updated successfully");
		  	
		  	if (this.cb) { this.cb(true) }
			
			}.bind( { data: data, cb:cb, } ),
			error: function() {
				console.error("failed updateing page !!!"); 
				console.error("failed updateing page !!!");
				console.error("failed updateing page !!!"); 

				if (this.cb) { this.cb(false) }
			}

		}).done(function( msg ) {
			
	  });	

	} else {
		if (cb) {
			cb(true);
		}
	}

}

function mobile_save_all_changed( cb = null ) {

	var pages_ids = pages_get_all_ids();
	var changed_ids = [];

	// find all changed pages
	for (var i=0; i<pages_ids.length; i++) {
		var page_id = pages_ids[i];
		var page = pages_page_container(page_id);

		var page_data = page.toJSON();
		var stored_data = g_pages_last_save_copy[page_id];

		if ( !( JSON.stringify(page_data) === JSON.stringify(stored_data) ) ) { // if changed
			changed_ids.push(page_id);
		}
	}

	// save all changed
	if (changed_ids.length > 0) {
		mobile_update_pages(changed_ids, cb);
	} else {
		if (cb) {
			cb( true );
		}
	}

}

removed for mobile and desktop integration for photoprint */
