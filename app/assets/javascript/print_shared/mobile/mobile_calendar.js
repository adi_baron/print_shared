function calendar_configuration_load(product_id ,container_el) {

	var html = handlebars_compile_and_execute("configuration-page", {
    modal: false,
    
    disabled: false,

    show_month_selector: true,

    data: g_album,
  });
	$(container_el).html(html);

}

$( document ).on("submit", ".calendar-start-date-form", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	// return if allready disabled to avoid double click
	if ( $(this).find('[type="submit"]').hasClass("disabled") ) { return; }
	$(this).find('[type="submit"]').addClass("disabled");

	var data = ConvertFormToJSON(this);
	var path = "/customers/" + g_customer_id + "/tokenized_users/" + g_tokenized_user_id + "/products/" + g_album.id + ".json"; 
	var url = g_editor_url + path;

	$.ajax({
    url: url,
    method: "PUT",
    async: true,
    cache: false,
    // contentType: 'application/json',

	  // data: JSON.stringify(data),
	  data: data,

	  success: function(data) {
	  	start_editor(g_product_id, g_user_item_id, { replace: true });
		},
		error: function() {
			console.error("failed updateing Calendar");  
		},
		complete: function() {
			$(this).find('[type="submit"]').removeClass("disabled");
		}.bind(this),

	}).done(function( msg ) {
		
  });	

});

$(document).on("click", ".calendar_date .calendar_delete_date", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	var uuid = $(this).closest(".calendar_date").attr("data-uuid");
	
	var path = "/customers/" + g_customer_id + "/tokenized_users/" + g_tokenized_user_id + "/products/" + g_album.id + "/calendar_dates?uuid=" + uuid; 
	var url = g_editor_url + path;

	$.ajax({
    url: url,
    method: "delete",
    async: true,
    cache: false,

	  beforeSend : function (xhr) {
			xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		},

	  success: function(res) {
	  	store_append_flash_message( res.str, "success" );
	  	
	  	if (res.pages) {
		  	for (var i=0; i<res.pages.length; i++) {
			  	var page = pages_page_container(res.pages[i]);
			  	var el_uuid = page && page.get_first_el_of_type("CalendarEl")

			  	if (el_uuid) {
			  		page.element(el_uuid).update_calendar_html();
			  	} else {
			  		console.warn("no calendar el found");
			  	}
			  }
			}

	  	delete g_album.private_dates[this.uuid];
	  	
	  	calendar_configuration_modal();

		}.bind( { uuid:uuid, } ),
		error: function(jqXHR, ajaxOptions, thrownError) {
			debugger;
		}

	}).done(function( msg ) {
		
  });



});

$( document ).on("submit", "#calendar-new-event-form", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	// return if allready disabled to avoid double click
	if ( $(this).find('[type="submit"]').hasClass("disabled") ) { return; }
  $(this).find('[type="submit"]').addClass("disabled");

	var data = ConvertFormToJSON(this);
	data.uuid = uuid();

	var path = "/customers/" + g_customer_id + "/tokenized_users/" + g_tokenized_user_id + "/products/" + g_album.id + "/calendar_dates"; 
	var url = g_editor_url + path;

	$.ajax({
    url: url,
    method: "post",
    async: true,
    cache: false,

	  data: data,

	  beforeSend : function (xhr) {
			xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		},

	  success: function(res) {
	  	store_append_flash_message( res.str, "success" );
	  	
	  	g_album.private_dates[res.id] = { txt: res.data.txt, date: res.data.date, media_url: res.data.media_url };

	  	if (res.pages) {
		  	for (var i=0; i<res.pages.length; i++) {
			  	var page = pages_page_container(res.pages[i]);
			  	var el_uuid = page && page.get_first_el_of_type("CalendarEl")

			  	if (el_uuid) {
			  		page.element(el_uuid).update_calendar_html();
			  	} else {
			  		console.warn("no calendar el found");
			  	}
			  }
			}

	  	calendar_configuration_modal();

		}.bind(data),
		error: function(jqXHR, ajaxOptions, thrownError) {
			if (jqXHR.responseJSON && jqXHR.responseJSON.str) {
				store_append_flash_message( jqXHR.responseJSON.str, "error" );
			}
		},
    complete: function() {
    	$(this).find('[type="submit"]').removeClass("disabled");
    }.bind(this),

	}).done(function( msg ) {
		
  });

});

$( document ).on("change", "#calendar-new-event-form input[type=file]", function(ev) {

	var file = this.files[0];

	$("#calendar-new-event-form :input").prop('disabled', true);

	// Create an image
  var img = document.createElement("img");
  
  img.onload = function() {

		var canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);

    var MAX_WIDTH = 500;
    var MAX_HEIGHT = 400;
    var width = img.width;
    var height = img.height;

    if (width > height) {
      if (width > MAX_WIDTH) {
        height *= MAX_WIDTH / width;
        width = MAX_WIDTH;
      }
    } else {
      if (height > MAX_HEIGHT) {
        width *= MAX_HEIGHT / height;
        height = MAX_HEIGHT;
      }
    }
    canvas.width = width;
    canvas.height = height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, width, height);

    var dataurl = canvas.toDataURL("image/jpeg", 0.9);
    
    $("#calendar-new-event-form input[name=upload]").val(dataurl);
    $("#calendar-new-event-form :input").prop('disabled', false);
  }

  // Create a file reader
  var reader = new FileReader();
  // Set the image once loaded into file reader
  reader.onload = function(e)
  {
      img.src = e.target.result;
  }
  // Load files into file reader
  reader.readAsDataURL(file);	

})
