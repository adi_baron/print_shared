$(document).on('click', '.pages-container-configuration', function(ev) {
  ev.preventDefault();
  ev.stopPropagation();

  calendar_configuration_modal();
});

function calendar_configuration_modal() {

  var html = handlebars_compile_and_execute("configuration-page", {
    modal: true,
    
    // disabled: true,
    disabled: false,

    show_month_selector: false,
    show_dates: true,

    private_dates: g_album.private_dates,
    // data: g_album,

  });
  $(".mobile-modal").html(html);
  $(".mobile-modal").removeClass("hidden");  
}

$(document).on('click', '.pages-container-actions-panels-header .panel-btn', function(ev) {

  activate_bottom_panel_btn(ev);
  return false;	

});


function activate_bottom_panel_btn(ev) {

  var target = ev ? ev.target : 
                    ( $(".pages-container-actions-panels-header .panel-btn.active").length > 0 ) ?
                    null : 
                    $(".pages-container-actions-panels-header .panel-btn")[0];

  if (target == null) { return false }

  $(".pages-container-actions-panels-header .panel-btn")[0];

  var container_name = $(target).attr('data-container-name');
  $('.pages-container-actions-panels-header .panel-btn').removeClass("active");
  $(target).addClass("active");

  var el = $(".pages-container-actions-panels .pages-container-actions-panels-panel[data-container-name='" + container_name + "']")[0];
  $(".pages-container-actions-panels .pages-container-actions-panels-panel").css( { "display":"none" } );

  if (el) {
    $(el).css( { "display":"block" } );
    
    if ($(el).find(".panel-inner").length == 0) {
      window[("mobile_panels_load_" + container_name)](el);
    }
  }

  if ( !$(target).closest(".pages-container-actions").hasClass("open") ) {
    !$(target).closest(".pages-container-actions").addClass("open")
  }

  $(".clone").remove();

  mobile_actions_load_interact();


}

function mobile_panels_load_photos(el) {
	if ( !( $(el).find("panel-inner").length > 0 ) ) {
		var html = handlebars_compile_and_execute("mobile_images_panel", {
			images: g_uploader.get_images(),
		});
		$(el).html(html);

    // First update of used photos counter
    var data = pages_data_for_media_count();
    g_uploader.set_used_images(data);
	}
}

function mobile_panels_load_templates(el) {
	var data = g_auto_template.get_templates_data();
	var html = handlebars_compile_and_execute("mobile_templates_panel", {
		data: data,
	});
	$(el).html(html);	
}

// const colors_palette = ["#ffffff", "#000000", "#333333", "#666666", "#999999", "#ff0000", "#00ff00", "#0000ff", "#ffff00", "#00ffff", "#ff00ff"];
const colors_palette = [
  "rgb(2,0,2)",
  "rgb(66,64,66)",
  "rgb(131,128,131)",
  "rgb(194,191,194)",
  "rgb(255,254,255)",
  "rgb(255,222,221)",
  "rgb(209,165,125)",
  "rgb(255,255,222)",
  "rgb(148,235,237)",
  "rgb(82,113,123)",
  "rgb(227,224,251)",
  "rgb(254,177,195)",
  "rgb(62,1,58)",
  "rgb(105,100,179)",
  "rgb(41,161,43)",
  "rgb(144,212,86)",
  "rgb(84,199,81)",
  "rgb(212,71,193)",
  "rgb(231,77,142)",
  "rgb(253,183,71)",
  "rgb(236,138,62)",
  "rgb(237,66,65)",
  "rgb(180,47,60)",
  "rgb(11,42,106)",
  "rgb(42,67,138)",
  "rgb(35,93,167)",
  "rgb(39,127,177)",
  "rgb(47,176,199)",
  "rgb(62,192,182)",
  // "rgb(,,)",
  // "rgb(,,)",
  // "rgb(,,)",
  // "rgb(,,)",
  // "rgb(,,)",
  // "rgb(,,)",
];

function mobile_panels_load_backgrounds(el) {
	var html = handlebars_compile_and_execute("mobile_backgrounds_panel", { 
		colors: colors_palette,
	});
	$(el).html(html);	
}

var g_mobile_actions_interact_loaded = false;
var g_mobile_actions_interacting_for_inbounce = false;
function mobile_actions_load_interact() {
  
	if (g_mobile_actions_interact_loaded) { return true; }
	
	$(".images-container-el")[0].addEventListener('touchstart', ignoreTouch);
  $(".images-container-el")[0].addEventListener('touchmove', ignoreTouch);
  $(".images-container-el")[0].addEventListener('touchend', ignoreTouch);


	interact(".images-container-el")
    .draggable({ 
      manualStart: true,

      inertia: false,
      
      autoScroll: false,

      listeners: {
        start (event) {
          console.log("actions drag started");
          console.log(event.type, event.target);

          mobile_pages_position.x = 0;
          mobile_pages_position.y = 0;
          mobile_pages_beforeY    = 0;

        },
        end (event) {
          
          try {
            console.log("actions end drag");
            g_mobile_actions_interacting_for_inbounce = false;
            
            if (event.target && mobile_pages_current_draggable_origin) {
              $(event.target).remove();

              // this is a workaround to remove all gost clones who might have been "lost"...
              $(".pages-container .clone").remove();
              $("body .clone").remove();

              if ( $(mobile_pages_current_draggable_origin).hasClass("background") && $('.can-drop').hasClass("page") ) {
                var page_id = $(".can-drop").closest(".page-previewer").find(".page").attr("data-page-id");
                var page = pages_page_container(page_id);
                var color = $(mobile_pages_current_draggable_origin).attr("data-color");
              
                var el_id = page.get_or_new_background_el();
                var el = page.element(el_id);

                el.background_color( {
                  color: color,
                  opacity: 1,
                } );
              }

              if ( $(mobile_pages_current_draggable_origin).hasClass("template") && $('.can-drop').hasClass("page") ) {
                var template_id = $(mobile_pages_current_draggable_origin).attr("data-id");
                var page_id = $(".can-drop").closest(".page-previewer").find(".page").attr("data-page-id");

                var page = pages_page_container(page_id);
                if ( (page.page_type == 'album_cover') && (g_album.customer_product.product_group.render_as_pages == false) ) {

                  mobile_ui_alert_modal({
                    title: I18n.t("mobile.alerts.template_does_not_match_cover_title"),
                    txt: I18n.t("mobile.alerts.template_does_not_match_cover_txt"),
                  });

                } else {

                  var page_el = $(".can-drop").closest(".page-previewer").find(".page")[0];
                  var left_or_right = $(page_el).hasClass("left") ? "left" :
                                        $(page_el).hasClass("right") ? "right" :
                                          "both";

                  var template = g_auto_template.get_template_by_id(template_id);
                  
                  if (template) {

                    g_auto_template.current_page_from_template(template, {
                      add_margin:     false,
                      left_or_right:  left_or_right,

                      page_id:        page_id, // used only for mobile
                      album_page:     pages_page_container(page_id), // used only for mobile
                      selector:       $(".can-drop").closest(".page-previewer")[0],
                      editor:         false,
                    });

                  }
                } 

              }
              
              if ( $(mobile_pages_current_draggable_origin).hasClass("image") ) {
   
                var image_uuid = $(mobile_pages_current_draggable_origin).attr("data-uuid");

                if ( $('.can-drop').hasClass("image-container") ) {
                  
                  var page_id = $(".can-drop").closest(".page-previewer").find(".page").attr("data-page-id");
                  var page = pages_page_container(page_id);
                  var el_id = $('.can-drop').attr("data-id");
                  var el = page.element(el_id);

                  var image = g_uploader.image_from_ids(null, image_uuid);

                  el.mobile_img(image);

                } else if ( 
                            $('.can-drop').hasClass("mobile-add-images-btn-container")  && 
                            $('.can-drop').hasClass("delete") 
                          ) {
                  var images_ids_arr = [ { id:"", uuid:image_uuid }];
                  delete_images(images_ids_arr);

                }
              }

            }

            $(".can-drop").removeClass("can-drop");
            $(".mobile-add-images-btn-container").removeClass("delete");
            mobile_pages_current_draggable_origin = null;
            console.log("all can-drop removed");


            // do pages general dropables areas
            $(".page-previewer .page-actions-container").addClass("visible");

            return;

          } catch (e) {
            console.error(e);
          }  

        }, 
        move (event) {

          try {

            // event.preventDefault();
          	// event.stopPropagation();
          	
            // console.log("actions move drag");
            
            // if (false) {
            if (event.interaction.interacting()) {
              // console.log('moving');
              mobile_pages_position.x += event.dx
              mobile_pages_position.y += event.dy

              event.target.style.transform =
                `translate(${mobile_pages_position.x}px, ${mobile_pages_position.y}px)`;

              do_move_draggable(event);
                
            } else {
              console.log('not interacting')
            }

            // return false;

          } catch (e) {
            console.error(e);
          }

        }        
      }
    }
  );

	interact(".images-container-el").pointerEvents({
	  holdDuration: 100,
	}).on('hold',   
    function (event) { 
      
      try {
        event.preventDefault();
        event.stopPropagation();

        $(".mobile-add-images-btn-container").addClass("delete");

        console.log("hold actions images-container-el");

        var interaction = event.interaction;
        
        var target = $(event.target).closest(".images-container-el")[0];

        var width =  $(target).width();
        var height = $(target).height();

        var clone = target.cloneNode(true);
        
        // do not copy id
        $(clone).attr("id", "dummy-123");

        var top = $(".pages-container").scrollTop() + ( $(target).offset().top - $(".pages-container").offset().top );
        var left  = $(target).offset().left - $(".pages-container").offset().left;

        var resize_ratio = ( ((width >= height) && (width < 80)) || ((height > width) && (height < 80)) ) ?
                            1.35 : // very small => make it bigger
                            ( ((width >= height) && (width < 120)) || ((height > width) && (height < 120)) ) ?
                              1.2 : // little small => make it little bigger  
                              ( ((width >= height) && (width < 150)) || ((height > width) && (height < 150)) ) ?
                              1.1 : // medium => make it little smaller
                              0.85; // big...

        var clone_width   = parseInt(width*resize_ratio);
        var clone_height  = parseInt(height*resize_ratio);

        var clone_left  = left - (clone_width - width)/2;
        var clone_top   = top - (clone_height - height)/2;

        $(clone).css( { 

          position:"absolute",

          width: clone_width,
          height: clone_height,

          top: clone_top,
          left: clone_left,

          opacity: 0.7,

          "z-index": 99999,

          "box-shadow":
            "0 1px 0.5px rgba(0, 0, 0, 0.28),\
            0 2.7px 1.5px rgba(0, 0, 0, 0.4),\
            0 3.5px 2.5px rgba(0, 0, 0, 0.48),\
            0 4.8px 3.9px rgba(0, 0, 0, 0.60),\
            0 7.8px 5.4px rgba(0, 0, 0, 0.68),\
            0 15px 12px rgba(0, 0, 0, 0.96)",

          "-webkit-box-shadow":  
            "0 1px 0.5px rgba(0, 0, 0, 0.28),\
            0 2.7px 1.5px rgba(0, 0, 0, 0.4),\
            0 3.5px 2.5px rgba(0, 0, 0, 0.48),\
            0 4.8px 3.9px rgba(0, 0, 0, 0.60),\
            0 7.8px 5.4px rgba(0, 0, 0, 0.68),\
            0 15px 12px rgba(0, 0, 0, 0.96)",

            "-ms-touch-action": "none",
            "touch-action": "none",
        });

        $(clone).find("img").css( { "width":"100%", "height":"100%", "-ms-touch-action": "none", "touch-action": "none", } );
        // $(".mobile-edior").css( { "-ms-touch-action": "none", "touch-action": "none", } );
        // $(".mobile-edior *").css( { "-ms-touch-action": "none", "touch-action": "none", } );
        // jQuery( "*" ).css( { "-ms-touch-action": "none", "touch-action": "none", } );
        // $(clone).find('*').css( { "-ms-touch-action": "none", "touch-action": "none",} );

        // $(".body-tab-panel *").css( { "-ms-touch-action": "none", "touch-action": "none",} );
        // $(".body-tab-panel *").css( { "overflow": "hidden" } );

        $(clone).addClass("clone");

        $(clone).css( { "pointer-events": "none" } );

        // this would be disabled on drag end
        // $(".pages-container-actions .panel-inner").css( { "overflow-x":"hidden", "overflow-y":"hidden" } );


        //$(".pages-container").append(clone);
        $("body").append(clone);

        // event.interaction.stop()
        // event.interaction.end();

        interaction.start({ name: 'drag' }, event.interactable, clone);

        mobile_pages_current_draggable = clone;
        mobile_pages_current_draggable_origin = target;
        
        g_mobile_actions_interacting_for_inbounce = true;
        // return false;

      } catch (e) {
        console.error(e);
      }  
        
    }
  );

  if (false) {
    $(document).on('touchmove mousemove', '.images-container-el', function(ev) {
    // $(document).on('pointermove', '.images-container-el', function(ev) {  
        
      console.log("pointermove !!!" );

      // ev.stopPropagation();
      // ev.preventDefault();  
      
      var els = $(".can-drop");
      els.removeClass("left");
      els.removeClass("right");
      $(".can-drop").removeClass("can-drop");

      if (mobile_pages_current_draggable_origin) {
        console.log("images-container-el moving");

        if (ev.originalEvent && ev.originalEvent.touches && ev.originalEvent.touches[0]) {
          var touch = ev.originalEvent.touches[0];  
        } else {
          var touch = {
            clientX: ev.pageX,
            clientY: ev.pageY,
          };
        }

        // debugger;
        var els = document.elementsFromPoint(touch.clientX, touch.clientY);
        
        $(".can-drop").removeClass("can-drop");

        if ( $(ev.currentTarget).closest(".images-container-el").hasClass("image") ) {
          for (var i=0; i<els.length; i++) {
            var parent = $(els[i]).closest(".el-container-parent");
            if ( (parent.length > 0) && !( $(parent[0]).hasClass("clone") ) ) {
              
              $(parent[0]).addClass("can-drop");
              break;
              
            }
          }
        }

        if ( $(ev.currentTarget).closest(".images-container-el").hasClass("template") ) {
          console.log("dragging a template !!!");
          for (var i=0; i<els.length; i++) {
            console.log("we have " + els.length + " below !!!");
            var parent = $(els[i]).closest(".page");
            if ( (parent.length > 0) && !( $(parent[0]).hasClass("clone") ) ) {

              $(parent[0]).addClass("can-drop");
              var parent_rect = parent[0].getBoundingClientRect
              var parent_middle = ( parent[0].getBoundingClientRect().x + (parent[0].getBoundingClientRect().width/2) )
           
              $(parent[0]).removeClass("left");
              $(parent[0]).removeClass("right");

              
              if (g_album.type == "Album") {
                if (touch.clientX > parent_middle) {
                  $(parent[0]).addClass("right");
                } else {
                  $(parent[0]).addClass("left");
                }
              }

              break;
              
            }
          }
        }

        if ( $(ev.currentTarget).closest(".images-container-el").hasClass("background") ) {
          for (var i=0; i<els.length; i++) {
            var parent = $(els[i]).closest(".page");
            if ( (parent.length > 0) && !( $(parent[0]).hasClass("clone") ) ) {
              $(parent[0]).addClass("can-drop");
              break;
              
            }
          }
        }      

      } else {
        console.log("NO mobile_pages_current_draggable_origin!!!")
      }

      //document.addEventListener('pointercancel', (event) => {
      //  console.log('Pointer event cancelled')
      //  event.preventDefault();
      //  event.stopPropagation();
      //  return false;   
      //});

      // return false;   
       
    });
  }

  // debugger;
  // interact('.panel-inner').on('scroll', function (event) {
    // console.log("scroll pages-container");
  // });
  $(document).on('scroll', ".panel-inner", function(ev) { 
    console.log(".panel-inner scrolled"); 
  });

	g_mobile_actions_interact_loaded = true;
	return true;
} 

function do_move_draggable(interact_event) {

  // console.log("pointermove !!!" );

  // ev.stopPropagation();
  // ev.preventDefault();  
  
  // debugger;

  if (true) {
    var els = $(".can-drop");
    els.removeClass("left");
    els.removeClass("right");
    $(".can-drop").removeClass("can-drop");

    var touch = null;

    if (mobile_pages_current_draggable_origin) {
      console.log("images-container-el moving");

      // if (ev.originalEvent && ev.originalEvent.touches && ev.originalEvent.touches[0]) {
      //   var touch = ev.originalEvent.touches[0];  
      // } else {
      //   var touch = {
      //     clientX: ev.pageX,
      //     clientY: ev.pageY,
      //   };
      // }
      if (interact_event.client && interact_event.client.x && interact_event.client.y) {
        touch = {
          clientX: interact_event.client.x,
          clientY: interact_event.client.y,
        };
      }

      if (touch) {

        var els = document.elementsFromPoint(touch.clientX, touch.clientY);
        
        $(".can-drop").removeClass("can-drop");

        // if ( $(ev.currentTarget).closest(".images-container-el").hasClass("image") ) {
        if ( $(interact_event.target).closest(".images-container-el").hasClass("image") ) {  
          for (var i=0; i<els.length; i++) {
            var parent = $(els[i]).closest(".el-container-parent");
            var dropable = $(els[i]).closest(".drop_area");

            if ( ( (parent.length > 0) && !( $(parent[0]).hasClass("clone") ) ) || (dropable.length > 0) ) {
              if (dropable.length > 0) {
                $(dropable[0]).addClass("can-drop");  
              } else {
                $(parent[0]).addClass("can-drop");  
              }
              break;
            }
          }
        }

        // if ( $(ev.currentTarget).closest(".images-container-el").hasClass("template") ) {
        if ( $(interact_event.target).closest(".images-container-el").hasClass("template") ) {  
          console.log("dragging a template !!!");
          for (var i=0; i<els.length; i++) {
            console.log("we have " + els.length + " below !!!");
            var parent = $(els[i]).closest(".page");
            if ( (parent.length > 0) && !( $(parent[0]).hasClass("clone") ) ) {

              $(parent[0]).addClass("can-drop");
              var parent_rect = parent[0].getBoundingClientRect
              var parent_middle = ( parent[0].getBoundingClientRect().x + (parent[0].getBoundingClientRect().width/2) )
           
              $(parent[0]).removeClass("left");
              $(parent[0]).removeClass("right");
              
              if ( (g_album.type == "Album") &&
                   (g_album.customer_product.product_group.render_as_pages == true) &&
                   ( !($(parent).closest(".page-previewer").hasClass("single")) ) // for single keep the full page highlighted
                 )
              {
                if (touch.clientX > parent_middle) {
                  $(parent[0]).addClass("right");
                } else {
                  $(parent[0]).addClass("left");
                }

              } else {
                // do none !!!
              }

              break;
              
            }
          }
        }

        // if ( $(ev.currentTarget).closest(".images-container-el").hasClass("background") ) {
        if ( $(interact_event.target).closest(".images-container-el").hasClass("background") ) {
          for (var i=0; i<els.length; i++) {
            var parent = $(els[i]).closest(".page");
            if ( (parent.length > 0) && !( $(parent[0]).hasClass("clone") ) ) {
              $(parent[0]).addClass("can-drop");
              break;
              
            }
          }
        } 
      } else {
        console.warn("move dragable with no touch value");
      }   

    } else {
      console.log("NO mobile_pages_current_draggable_origin!!!")
    }

  } // if false

}
