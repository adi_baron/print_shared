var g_cropper_el;

$( document ).on('ready', function(ev) {	

	$( document ).on('click', ".mobile-image-apply-button", function(ev) {
		ev.preventDefault();
		ev.stopPropagation();
		console.log(".mobile-image-apply-button click");

		var page_id = $(this).closest(".mobile-image-editor").attr("data-page-id");
		var el_id 	= $(this).closest(".mobile-image-editor").attr("data-el-id");

		var el = pages_page_container(page_id).element(el_id);

		var canvas_data = g_cropper_el.getCanvasData();
		var crop_box_data = g_cropper_el.getCropBoxData();
			
		var width_px = crop_box_data.width;
			
		// var detail 		= g_cropper_el.getCanvasData();		
		var detail 		= g_cropper_el.getData();

		el.set_crop_data( {
			crop_box_data: crop_box_data,
			canvas_data: canvas_data,
			detail: detail,
			width_px: width_px,
		});

		el.rerender_html();
		el.init( function() {

			this.el.post_init();

			var el_text = this.text;
			var page_container = pages_page_container(this.page_id);

			// if (pages_page_container(this.page_id).page_type == "album_cover") {

			// 	var text_el_id = page_container.mobile_get_first_text_el();
			// 	var text_el = text_el_id ? page_container.element(text_el_id) : null;

			// 	if (text_el) {
			// 		text_el.attrs.data.text = el_text;
			// 		text_el.rerender_html();
			// 		text_el.init();
			// 		text_el.post_init();
			// 	}

			// } else {
				this.el.set_innet_text_data(el_text);
			// }

			mobile_update_page(page_id)

		}.bind( 
						{ 
							el:el, 
							text:$(this).closest(".mobile-image-editor").find("#image-text").val(), 
							page_id: page_id,
						} 
					) 
		);
		
		do_back_btn();

	});

	$( document ).on('click', ".make-black-white", function(ev) {
		ev.preventDefault();
		ev.stopPropagation();

		var page_id = $(".image-edit.opened").find(".mobile-image-editor").attr("data-page-id");
		var el_id 	= $(".image-edit.opened").find(".mobile-image-editor").attr("data-el-id");
		var el = pages_page_container(page_id).element(el_id);

		if (el && el.attrs.filters && el.attrs.filters.grayscale && 
					(parseInt(el.attrs.filters.grayscale) > 0) ) 
		{
			
			var data = {
				"grayscale": 0,
			};

		
		} else {

			var data = {
				"grayscale": "100",
			};

		}

		if (el) {
			el.set_filters(data);
			var filter_str = get_filter_str(el);
			image_editor_set_filter(filter_str);
			// $(".mobile-image-cropper-container").css( { "filter": filter_str, } );
		} else {
			console.error( `cant find page el page:${page_id} el:${el_id}` );
		}

	});

	$( document ).on('click', ".delete-image-from-page-btn", function(ev) {
		ev.preventDefault();
		ev.stopPropagation();
		console.log(".mobile-image-apply-button click");

		var page_id = $(".image-edit.opened").find(".mobile-image-editor").attr("data-page-id");
		var el_id 	= $(".image-edit.opened").find(".mobile-image-editor").attr("data-el-id");
		var el = pages_page_container(page_id).element(el_id);

		el.mobile_remove_img();
		do_back_btn();

	});
	

});

function get_filter_str(el) {
	if (el && el.attrs.filters && el.attrs.filters.grayscale) {
		var filter_str = `grayscale(${el.attrs.filters.grayscale}%)`
	} else {
		var filter_str = `grayscale(0%)`
	}

	return filter_str;
}

function image_editor_set_filter(filter_str) {
	$(".mobile-image-cropper-container").css( { "filter": filter_str, } );
}

function image_set_cropper_size( el ) {
	var width 	= $(".mobile-image-cropper").width();
	var height 	= $(".mobile-image-cropper").height();
	
	var ratio = el.ratio();

	if ( (height/ratio) < width ) {

		$(".mobile-image-cropper .mobile-image-cropper-container-holder").css( {
			"width": 	( (height/ratio) + "px"),
			"height": "100%",
		});

	} else {

		$(".mobile-image-cropper .mobile-image-cropper-container-holder").css( {
			"width": 	"100%",
			"height": ( (width*ratio) + "px" ),
		});

	}

}

function image_load_edit(page_id, el_id ,container_el) {

	var page_container = pages_page_container(page_id);
	var el = page_container.element(el_id);

	var data = { page_container: page_container, el: el };

	var html = handlebars_compile_and_execute("mobile-image-edit", data);

	$(container_el).html(html);

	image_set_cropper_size(el);

	// if (page_container.page_type == "album_cover") {
	if (false) {
		
		var text_el_id = page_container.mobile_get_first_text_el();
		var text_el = text_el_id ? page_container.element(text_el_id) : null;

		if (text_el) {

			$(container_el).find(".mobile-image-text-editor input").val( 
				( text_el.attrs && text_el.attrs.data && text_el.attrs.data.text && (text_el.attrs.data.text.length > 0) ) ? text_el.attrs.data.text : ""
			);

		}

	} else {

		var filter_str = get_filter_str(el);
		image_editor_set_filter(filter_str);

		$(container_el).find(".mobile-image-text-editor input").val( 
			( el.attrs.inner_text && el.attrs.inner_text.text && (el.attrs.inner_text.text.length > 0) ) ? el.attrs.inner_text.text : ""
		);

	}


	var image = $(container_el).find('img')[0];

	g_cropper_el = new Cropper(image, {
	  
	  aspectRatio: (1/el.ratio()),
	  
	  guides: true,
	  // guides: false,
	  highlight: false,

	  responsive: false,
	  // restore: true,
	  restore: false,

		// wheelZoomRatio: 0.04,

		checkCrossOrigin: true,
		checkOrientation: false, // will go with checkCrossOrigin: true while img tag has crossorigin="anonymous"
		// => see https://github.com/fengyuanchen/cropperjs/issues/413

		viewMode: 3,
		zoomable: true,
		zoomOnTouch: true,
		scalable: true,

		dragMode: 'move',
		// dragMode: 'none',
	  // dragMode: 'crop',
	  
	  toggleDragModeOnDblclick: false,

	  center: false,
	  // center: true,


	  autoCrop: true,
	  autoCropArea: 1,
	  
	  cropBoxMovable: false,
	  // cropBoxMovable: true,
	  
	  cropBoxResizable: false,
	  // cropBoxResizable: true,
	  
	  // fill / fit
	  // movable: (this.fit == false) ? true : false,		
	  movable: true,
	  // movable: false,

	  data: 	( (el.attrs && el.attrs.canvas && el.attrs.canvas.detail) ? el.attrs.canvas.detail : null ),
	  // canvas: ( (el.attrs && el.attrs.canvas && el.attrs.canvas.canvas) ? el.attrs.canvas.canvas : null ),

	  crop(event) {
	  	console.log("crop");

	  	// var canvas_data = g_cropper_el.getCanvasData();
			// var crop_box_data = g_cropper_el.getCropBoxData();
			
			// var width_px = crop_box_data.width;
			
			// // if (Object.keys(crop_box_data).length > 0) {
			// // 	this.attrs.canvas.crop_box 	= crop_box_data;
			// // }

			// var detail 		= event.detail;

	  },
	  ready: function (event) {
	  	console.log("ready");

	  	var w = $(".mobile-image-cropper-container").width();
	  	var h = $(".mobile-image-cropper-container").height();

	  	g_cropper_el.setAspectRatio(w/h);
			g_cropper_el.setCropBoxData({
				left:0,
				top:0, 
				// width:this.width,
				width:w,
				// height:this.height 
				height:h,
			});

			var old_data = ( (el.attrs && el.attrs.canvas && el.attrs.canvas.canvas) ? el.attrs.canvas.canvas : null );

			if (old_data) {

				var cropBox = g_cropper_el.getCropBoxData();

				// var ratio    = (this.parent_obj.offsetWidth / this.prev_width);
				var ratio    = (cropBox.width / this.el.attrs.size.width_px);
				
				var old_manipulated = {};
				
				Object.keys(old_data).forEach(function(item, index) {
          old_manipulated[item] = (old_data[item] * ratio);
        });

				var saved_data = el.attrs.canvas.canvas;
				Object.keys(saved_data).forEach(function(item, index) {
					old_manipulated[item] = (saved_data[item] * ratio);
				})

				// Fixing 1/2 => this is to fix a bug where cropper is reset due to canvas (including left / top) is smaller and cannot fill the cropbox!!!
				// This can happen due to none accurate canvas calculations in cropper js due to very small float numbers.
				// We Force success by resizing it

				// Fixing 1!!!
				// if (old_manipulated.width < cropBox.width) {
				if ( ( old_manipulated.width - (cropBox.width - old_manipulated.left) ) < 1 ) {
					// ratio = (cropBox.width / old_manipulated.width);
					ratio = ( ( (cropBox.width - old_manipulated.left) + 1 ) / old_manipulated.width );

					Object.keys(old_manipulated).forEach(function(item, index) {
	          old_manipulated[item] = old_manipulated[item] * ratio;
	        });
				} 
				

				// Fixing 2!!!
				// if (old_manipulated.height < cropBox.height) {
				if ( ( old_manipulated.height -  (cropBox.height - old_manipulated.top) ) < 1 ) {	
					// ratio = (cropBox.height / old_manipulated.height);
					ratio = ( ((cropBox.height - old_manipulated.top) + 1) / old_manipulated.height );

					Object.keys(old_manipulated).forEach(function(item, index) {
	          old_manipulated[item] = old_manipulated[item] * ratio;
	        });
				}

				// console.log( "value width  = " + ( old_manipulated.width -  (cropBox.width - old_manipulated.left) ) );
				// console.log( "value height = " + ( old_manipulated.height -  (cropBox.height - old_manipulated.top) ) );


				// this.cropper.resize_me(this.prev_width);
				g_cropper_el.setCanvasData(old_manipulated);

			}

	  }.bind( { el:el, } ),
	});

	// function start_move(ev) {
	// 	g_cropper_el.setDragMode("move");
	// 	g_cropper_el.onCropStart(ev, "move");
	// }

	// function end_move() {
	// 	debugger;
	// 	g_cropper_el.setDragMode("none");
	// }

	// $(container_el).find(".inner_mover").on("mousedown touchstart", function(ev) {
	// 	debugger;
	// 	console.error("inner_mover - move");
	// 	start_move(ev);
	// });
	// $(document).off("mouseup touchend", end_move);
	// $(document).on("mouseup touchend", end_move);

}
