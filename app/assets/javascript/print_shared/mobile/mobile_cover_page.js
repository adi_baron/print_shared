$( document ).on('ready', function(ev) {	

	$( document ).on('click', ".mobile-cover-selector .select-image-btn", function(ev) {
		ev.preventDefault();
		ev.stopPropagation();
	});

});



function cover_load_page( container_el, opts ) {

	var html = handlebars_compile_and_execute("mobile-cover-page", {
		images: g_uploader.get_images(),
	});

	$(container_el).html(html);

	var html = handlebars_compile_and_execute("mobile-header", {
		left_icon: 		"<i class='fal fa-chevron-left'></i>",
		left_class: 	"mobile-back-btn",

		title: (opts.title || "עיצוב הכריכה")
	});
	$(".main-container-header").html(html);

	$(".main-container-footer").css( { "display":"none" } );


}
