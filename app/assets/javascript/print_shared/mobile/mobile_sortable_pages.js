var g_sortable = null;

function mobile_start_sortable() {

	// var sort_el = document.getElementById('pages-container-sheets');

	// if (sort_el) {

	var sort_el = $(".pages-container .pages-group.pages-container-sheets")[0];
  
  if (sort_el) {	
  	console.log("Sortable init !!!");
		if (true) {
			var g_sortable = Sortable.create( sort_el, {
				handle: 	".sort-pages-btn",
				
				sort: 		true,
				// sort: 		false,

				// forceFallback: false,
				forceFallback: true,

				draggable: ".page-previewer.sheet",

				delay: 0,
				// delay: 10,
				// animation: 500,
				animation: 350,

				ghostClass:  "mobile-edior-pages-sortable-ghost",
				// chosenClass: "mobile-edior-pages-sortable-chosen",
				dragClass: "mobile-edior-pages-sortable-drag",

				// touchStartThreshold: 4,
				// fallbackTolerance: 4,
				touchStartThreshold: 2,
				fallbackTolerance: 3,
				fallbackOnBody: true,

				forceAutoScrollFallback: false,

				revertOnSpill: true,
				removeOnSpill: true,

				// ghostClass: 'sortable-ghost',

				// direction: 'vertical',
				// direction: 'horizontal',
				
				// animation: 200,


				// Element dragging started
				onStart: function (/**Event*/evt) {
					evt.oldIndex;  // element index within parent
					console.log("sortable onStart");
					$(".mobile-edior-pages-sortable-drag").attr("id", "random");
				},

				// Element dragging ended
				// onEnd: function (/**Event*/evt) {
				// 	// var itemEl = evt.item;  // dragged HTMLElement
				// 	// evt.to;    // target list
				// 	// evt.from;  // previous list
				// 	// evt.oldIndex;  // element's old index within old parent
				// 	// evt.newIndex;  // element's new index within new parent
				// 	// evt.oldDraggableIndex; // element's old index within old parent, only counting draggable elements
				// 	// evt.newDraggableIndex; // element's new index within new parent, only counting draggable elements
				// 	// evt.clone // the clone element
				// 	// evt.pullMode;  // when item is in another sortable: `"clone"` if cloning, `true` if moving
				// 	console.log("sortable onEnd");
				// },

				// Element is dropped into the list from another list
				// onAdd: function (/**Event*/evt) {
				// 	// same properties as onEnd
				// 	console.log("sortable onAdd");
				// },

				// Changed sorting within list
				onUpdate: function (/**Event*/evt) {
					// same properties as onEnd
					console.log("sortable onUpdate");

					g_sortable.destroy();
					g_sortable = null;
					setTimeout( function() {
						mobile_start_sortable();
					}, 500);

					var order = $(evt.to).find(".page").map( function() { return $(this).attr("data-page-id"); } ).get();

					var path = "/customers/" + g_customer_id + "/tokenized_users/" + g_tokenized_user_id + "/products/" + g_product_id + "/pages/order"						
					var url = g_editor_url + path;

		      $.ajax({
		        type: "POST",
		        async: true,
		        url: url,
		        data:     { "order": order },
		        cache:    false,
		        success: function(data) {
		          console.log("Successfully updated sort order");
		        },
		        error: function (jqXHR, ajaxOptions, thrownError) {
		          console.error("Failed updating sort order");
		        }
		      });

					mobile_update_pages_counters();
					
				},

				onMove: function (/**Event*/evt, /**Event*/originalEvent) {
					// Example: https://jsbin.com/nawahef/edit?js,output
					evt.dragged; // dragged HTMLElement
					evt.draggedRect; // DOMRect {left, top, right, bottom}
					evt.related; // HTMLElement on which have guided
					evt.relatedRect; // DOMRect
					evt.willInsertAfter; // Boolean that is true if Sortable will insert drag element after target by default
					originalEvent.clientY; // mouse position
					// return false; — for cancel
					// return -1; — insert before target
					// return 1; — insert after target
					// return true; — keep default insertion point based on the direction
					// return void; — keep default insertion point based on the direction

					console.log("onMove event");
					// return -1;
					// return false;
					// console.log(evt);
					if 	(	!( 
										( (evt.originalEvent.clientY > evt.relatedRect.top) && (evt.originalEvent.clientY < evt.relatedRect.bottom) ) 
										&&
						 				( (evt.originalEvent.clientX > evt.relatedRect.left) && (evt.originalEvent.clientX < evt.relatedRect.right) )
						 		 )
							) {
						return false;
					}

				},

			});

		}

		
		// sortable('.pages-container .pages-container-sheets', {
	 //    items: '.pages-container .pages-container-sheets .page-previewer:not(.cover)',
	 //    forcePlaceholderSize: true,
	 //    handle: ".sort-pages-btn",
	 //    placeholder: '<div class="page-previewer sheet"></div>',
	 //    hoverClass: "hover",
	 //    // itemSerializer: function(serializedItem, sortableContainer) {
	 //    //   debugger;
	 //    //   if ( $(serializedItem.node).find(".page").length == 1) {
	 //    //     return $(serializedItem.node).find(".page").attr("data-page-id");
	 //    //   }
	 //    //   return null;
	 //    // },
	 //  });


	}

	mobile_update_pages_counters();

}

function mobile_update_pages_counters() {
	// document.getElementsByClassName("page")[0].parentElement.classList.contains("cover")
	var els = document.getElementsByClassName("page");
	
	var has_cover = false;
	var single_counter = 0;

	for (var i=0; i<els.length; i++) {
		var el = els[i];
		var parent_el = el.parentElement;
		
		var is_single = false;

		if ( parent_el.classList.contains("cover") ) {
			has_cover = true;
			
			$(parent_el).find(".page-counter-container").attr(
				"data-right-counter", 
				(g_album.direction == "rtl") ? I18n.t("mobile.editor.back_cover") : I18n.t("mobile.editor.front_cover") 
			);
			
			$(parent_el).find(".page-counter-container").attr(
				"data-left-counter", 
				(g_album.direction == "rtl") ? I18n.t("mobile.editor.front_cover") : I18n.t("mobile.editor.back_cover")
			);

		} else {

			var content = "";
			if (g_album.type == "Calendar") {

				var months = [ "January", "February", "March", "April", "May", "June", 
           "July", "August", "September", "October", "November", "December" ];

        var date = new Date(g_album.from_month);   
        date.setMonth( date.getMonth() + (i - 1) );

        content = months[ date.getMonth()%12 ];

        $(parent_el).find(".page-counter-container").attr("data-left-counter", content );
        $(parent_el).find(".page-counter-container").attr("data-right-counter", "");

			} else if (g_album.type == "Album") {
				
				if ( parent_el.classList.contains("single") ) {
					is_single = true;
				}

				var j = has_cover ? (i-1) : i;

				if (is_single) {
					$(parent_el).find(".page-counter-container").attr(
						"data-left-counter", 
						("" + (j*2 + 1 - single_counter))
					);
					
					single_counter += 1;

				} else {

					$(parent_el).find(".page-counter-container").attr(
						"data-right-counter", 
						(g_album.direction == "rtl") ? ("" + (j*2 + 1 - single_counter)) : ("" + (j*2 + 2 -single_counter))
					);
									
					$(parent_el).find(".page-counter-container").attr(
						"data-left-counter", 
						(g_album.direction == "rtl") ? ("" + (j*2 + 2 - single_counter)) : ("" + (j*2 + 1 - single_counter))
					);

				}
			
			} else {
				// single image product ???
			}
			
		}

	}
}


