// $( document ).on('ready', function(ev) {	
// $( document ).on(
// 	( (value_if_defined('g_is_gallery_shop') == true) ? 'turbolinks:load' : 'ready'), // gallery shop use turbolink
// 	function(ev) {	

	window.onpopstate = function(event) {
  	console.log("location: " + document.location + ", state: " + JSON.stringify(event.state));
		
		if (!event.state) {
			console.error("state not available");
			load_root();
		}

		if (event.state) {
  		do_load_page(event.state.page, event.state, "from history", {
  										persistent: event.state.persistent,
  										history: true,
										}
									);
  	}
	};

	$( document ).on('click', ".mobile-back-btn, .gallery-header .back", function(ev) {
		// ev.preventDefault();
		// ev.stopPropagation();
		
		console.log("back !!!");
		var res = do_back_btn();
		console.log("back arrow res = " + res);
		if (res == true) { // handled
			ev.preventDefault();
		}

	});
	
	console.log('history ready');

// });

function is_page_of_name(name) {
	var res = $(".body-tab-panel.opened").hasClass(name);
	return res
}

function validate_current_state(state) {
	// return (history.state.page == "uploader-selector") ? true : false;
	return ( history.state && (history.state.page == state) ) ? true : false;
}

function history_push( data, title, page ) {
	history.pushState( data, "title", ("#" + page) ); 
}

function do_back_btn() {

	var opened_el = $(".main-container-body-tabs .body-tab-panel.opened")[0];
	
	// $(".main-container-body-tabs .body-tab-panel.opened").removeClass("opened");
	$(opened_el).removeClass("opened");

	closed_state = history.state;

	setTimeout( function() {
		if ( this.closed_state && (this.closed_state.page == "root") ) {
			if (this.closed_el) {
				$(this.closed_el).remove();
			}
		}
		if ( this.closed_state && (this.closed_state.page == "pages") ) {
			// $(this.closed_el).remove();
			// $(".main-container-body-tabs .images").remove();
			// $(".main-container-body-tabs .add-images").remove();
			// $(".main-container-body-tabs .image-edit").remove();
			
  		// "remove" the cookie while we do not neet to resume this page

			if ( !value_if_defined('g_is_gallery_shop') ) {  		
  			setCookie("last_p_id", "", 0);
  			setCookie("last_ui_id", "", 0);
  		}

  		mobile_stop_timed_save();
  		mobile_save_all_changed(false);

			remove_all_prev_product_tabs_if_exist();
		}
	}.bind( { closed_state:duplicate_obj(closed_state), closed_el:opened_el, } ), 400);
	


	if ( !closed_state || (closed_state.page == "pages") || (closed_state.page == "root") ) {
		
		if ( !value_if_defined('g_is_gallery_shop') ) {
			setCookie("last_p_id", "", 0);
			setCookie("last_ui_id", "", 0);
		}

		console.log(`do back is gallery == ${value_if_defined('g_is_gallery_shop')}`);
  	// if (g_is_gallery_shop) {
		if ( value_if_defined('g_is_gallery_shop') == true ) {	
			// $(".gallery-header .back").attr("href");
			var url = ( $(".store-menu .menu-link").attr("href") || $(".gallery-header .back").attr("href") );
			window.location = url;
			return false;
		}
		else {
			do_load_page( "root", { data: g_mobile_products }, "", { persistent: false, add_to_history: true, } ); 
			return true;
		}

	} else {
		if ( !ALLOWED_MOBILE_STATES.includes(closed_state.page) ) {	
			// if (g_is_gallery_shop) {
			if (value_if_defined('g_is_gallery_shop') == true) {
				window.location = $(".store-menu .menu-link").attr("href");
			} else {
				do_load_page( "root", { data: g_mobile_products }, "", { persistent: false, add_to_history: true, } ); 
			}
			return false;
		} else {
			history.back();
			return true;
		}
	
	}
}

function remove_pages_product_tabs_if_exist() {
	$(".main-container-body-tabs .pages").remove();
}

function remove_all_prev_product_tabs_if_exist() {
	$(".main-container-body-tabs .pages").remove();
	$(".main-container-body-tabs .images").remove();
	$(".main-container-body-tabs .add-images").remove();
	$(".main-container-body-tabs .image-edit").remove();
	$(".main-container-body-tabs .templates").remove();	
}

var ALLOWED_MOBILE_STATES = ["pages", "templates", "images", "add-images", "text-edit", "calendar-configutation", "image-edit", "image-edit-print", "uploader-selector", "cover-slector", "root", "create_product", "create_product_from_blocks", "page-url"]

// name 	- of the page
// data 	- related data to this loading
// title 	- page title 
// 
// opts:
// persistent - if page (tab) exist allready do not reload (it is updated offline ... even when tab no visible)
// history - true / false => if from back (history operation)
// add_to_history => do not change url => do not add to history
// func 	 - the function doing the actual data loading
function do_load_page( name, data, title, opts ) {
	if ( !ALLOWED_MOBILE_STATES.includes(name) ) {
		console.error(`state ${name} not defined`);
	}

	mobile_close_menu();

	var history_cmd = opts.replace ? 'replaceState' : 'pushState';
	console.log("history_cmd = " + history_cmd);

	// var add_to_history = (opts.add_to_history == false) ? false : true; 
	var add_to_history = (opts.add_to_history == true) ? true : false; 

	// for history
	var is_history = (opts && opts.history) ? opts.history : false;
	data.page = name;
	data.persistent = opts ? opts.persistent : false;

	data.force_rebuild = (opts && opts.force_rebuild) ? opts.force_rebuild : false;

	data.sidbar = (opts.sidebar) ? opts.sidebar : false;

	// client galleries
	// for gallery - some pages overun galleries header (this remark is in several location in code)
	$(".gallery-header .row .col-12").find(".main-container-header-inner").remove();

	switch(name) {
		case "pages":
			var pages_exist = ( $(".main-container-body-tabs .body-tab-panel.pages").length > 0 );
			var pages_has_pages = ( 
															( $(".main-container-body-tabs .body-tab-panel.pages .page").length > 0 )  ||
															( $(".main-container-body-tabs .body-tab-panel.pages .photo-print-content-tile").length > 0 )
														);
			
			if ( !pages_exist ) {
				$(".main-container-body-tabs").append( "<div class='body-tab-panel pages' style=''></div>" );
			}
			var el = $(".main-container-body-tabs .body-tab-panel.pages")[0];
			
			if (data.product_id) {
				
				if 	( 
							( !data.persistent || !pages_exist || !pages_has_pages || data.force_rebuild )
						) {
					

					if ( (g_album.type != "PhotoPrint") || ( (g_album.type == "PhotoPrint") && !pages_exist ) ) {
					// if (g_album.type != "PhotoPrint") {
						let images_to_add = data.add_media;
						
						mobile_load_all_pages(data.product_id, el, 
							function() {
								// do none this is just function complete perliminary run
							}.bind( { add_media: data.add_media, } ),
							function( page_id ) {
								// page loaded & visible
								if ( (pages_count() == 1) && 
										 ( this.add_media || this.add_selected) 
									 )
								{
									var page = pages_page_container( g_album.pages_data[0].id );
									var el_uuid = page.get_first_el_of_type("AlbumImageEl");
									var el = page.element(el_uuid);
									if (el && el.attrs && !el.attrs.img) {
										
										var img = this.add_media ?
																g_uploader.image_from_external(this.add_media) :
																g_uploader.image_from_external(this.selected_images_arr.ids[0], this.selected_images_arr.uuids[0])

										if (img) { el.img(img); }
									}
								}
							}.bind( { add_media: data.add_media, add_selected: data.add_selected, selected_images_arr: data.selected_images_arr } ),
						) ;

						if ( (g_album.type =='PhotoPrint') && (pages_count() == 0) ) {
							mobile_photo_print_add_images( [data.add_media], [null] );
						}


					// } else if ( (g_album.type == "PhotoPrint") && !pages_exist ) {
					// 	mobile_photo_print_add_images( [data.add_media], [null] );
					}
					
				}

				var title = (g_album.type == "Album") ? "עריכת האלבום" :
											(g_album.type == "Calendar") ? "עריכת לוח השנה" :
												"עריכה"

				var html = handlebars_compile_and_execute("mobile-header", {
					left_icon: 		"<i class='fal fa-chevron-left'></i>",
					left_class: 	"mobile-back-btn",
					
					// title: title,
					// info: true,

					// right_icon: 	"<i class='fal fa-cart-plus'></i>",
					// right_class: 	"mobile-add-to-cart-btn",
					// right_txt: "הוסף לעגלה",
					right_icons: 	["<i class='fal fa-cart-plus'></i>", "<i class='fal fa-eye'></i>", "<i class='fal fa-info-circle'></i>"],
					right_classes: ["mobile-add-to-cart-btn", "mobile-preview", "mobile-info-page"],
					right_txts: [I18n.t("mobile.add_to_cart"), I18n.t("mobile.editor.preview"), I18n.t("mobile.editor.info")],
				});
				$(".main-container-header").html(html);	
				// debugger;
				// $(".gallery-header .row .col-12").append(html);		

				if (false) {
					var html = handlebars_compile_and_execute("mobile-footer", {
							left_icon: "<i class='fal fa-cart-plus'></i><br><span>הוסף לעגלה</span>",
							left_class: "mobile-add-to-cart-btn horizontal-vertical-center-absolute",

							// title: " ",
							center_icon: "<i class='fal fa-file'></i><br><span>הוסף דף</span>",
							center_class: "mobile-add-sheet-btn horizontal-vertical-center-absolute",

							right_icon: "<i class='fal fa-images'></i><br><span>תמונות</span>",
							right_class: "mobile-add-images-btn horizontal-vertical-center-absolute",
					}); 								
					$(".main-container-footer").html(html);
					$(".main-container-footer").css( { "display":"flex" } );
				} else {
					$(".main-container-footer").css( { "display":"none" } );
				}
				
				window.getComputedStyle(el).opacity; // added to dom - force reflow !!!

				if ( !is_history && add_to_history ) {
					history[history_cmd]( data, title, ("#" + name) ); 
				}
			
			} else {
				
				var html = handlebars_compile_and_execute("mobile-waiting-spinner", {} );
				$(el).html(html);				
			
			}
			
			$(".main-container-body-tabs .body-tab-panel.opened").addClass("opened-parent");
			$(".main-container-body-tabs .body-tab-panel.opened").removeClass("opened");
			
			$(el).addClass("opened");
			$(el).removeClass("opened-parent");

			break;
		
		case "templates":
			if ( $(".main-container-body-tabs .body-tab-panel.templates").length == 0 ) {
				$(".main-container-body-tabs").append( "<div class='body-tab-panel templates' style=''></div>" );
			}
			var el = $(".main-container-body-tabs .body-tab-panel.templates")[0];

			if (data.product_id) {

				templates_load_page(el, { 
																title: title,
																product_id: data.product_id,
														 } 
												);

				window.getComputedStyle(el).opacity; // added to dom - force reflow !!!

				if ( !is_history && add_to_history ) {
					history[history_cmd]( data, title, ("#" + name) ); 
				}


			} else {

				var html = handlebars_compile_and_execute("mobile-waiting-spinner", {} );
				$(el).html(html);

			}

			$(".main-container-body-tabs .body-tab-panel.opened").addClass("opened-parent");
			$(".main-container-body-tabs .body-tab-panel.opened").removeClass("opened");

			$(el).addClass("opened");
			$(el).removeClass("opened-parent");

			break;

		case "images":
		case "add-images":

			if ( $(".main-container-body-tabs .body-tab-panel.images").length == 0 ) {
				$(".main-container-body-tabs").append( "<div class='body-tab-panel images' style=''></div>" );
			}
			var el = $(".main-container-body-tabs .body-tab-panel.images")[0];

			if (data.product_id) {
				
				var is_external = (value_if_defined('g_is_gallery_shop') == true);

				images_load_page(el, { 
																title: title, 
																is_images: ( (name == "images") && (!(g_album.type == "PhotoPrint")) ),
																page_id: data.page_id,
																product_id: data.product_id,
																element_id: data.element_id,
																left_or_right: data.left_or_right,

																external_images: is_external,
																external_galleries: (is_external ? g_uploader.get_external_galleries() : null),
																
																max_selected_photos: data.max_selected_photos,
																add_selected: data.add_selected,
														 } 
												);

				window.getComputedStyle(el).opacity; // added to dom - force reflow !!!

				if ( !is_history && add_to_history ) {
					history[history_cmd]( data, title, ("#" + name) ); 
				}

				if (is_external) {
					external_images_gallery_selected(null, true);
				}


			} else {

				var html = handlebars_compile_and_execute("mobile-waiting-spinner", {} );
				$(el).html(html);

			}

			$(".main-container-body-tabs .body-tab-panel.opened").addClass("opened-parent");
			$(".main-container-body-tabs .body-tab-panel.opened").removeClass("opened");

			$(el).addClass("opened");
			$(el).removeClass("opened-parent");			

			break;

		case "text-edit":
			if ( $(".main-container-body-tabs .body-tab-panel.text-edit").length == 0 ) {
				$(".main-container-body-tabs").append( "<div class='body-tab-panel text-edit' style=''></div>" );
			}
			var el = $(".main-container-body-tabs .body-tab-panel.text-edit")[0];

			mobile_text_load_edit(data.page_id, data.element_id ,el);

			window.getComputedStyle(el).opacity; // added to dom - force reflow !!!

			$(".main-container-body-tabs .body-tab-panel.opened").addClass("opened-parent");
			$(".main-container-body-tabs .body-tab-panel.opened").removeClass("opened");

			$(el).addClass("opened");
			$(el).removeClass("opened-parent");

			var html = handlebars_compile_and_execute("mobile-header", {
				left_icon: 		"<i class='fal fa-chevron-left'></i>",
				left_class: 	"mobile-back-btn",
				
				title: title,
			});
			$(".main-container-header").html(html);
			
			$(".main-container-footer").css( { "display":"none" } );


			if ( !is_history && add_to_history ) {
				history[history_cmd]( data, title, ("#" + name) ); 
			}

			break;

		case "calendar-configutation":
			if ( $(".main-container-body-tabs .body-tab-panel.calendar-configuration").length == 0 ) {
				$(".main-container-body-tabs").append( "<div class='body-tab-panel calendar-configuration' style=''></div>" );
			}
			var el = $(".main-container-body-tabs .body-tab-panel.calendar-configuration")[0];

			calendar_configuration_load(data.product_id ,el);

			window.getComputedStyle(el).opacity; // added to dom - force reflow !!!

			$(".main-container-body-tabs .body-tab-panel.opened").addClass("opened-parent");
			$(".main-container-body-tabs .body-tab-panel.opened").removeClass("opened");

			$(el).addClass("opened");
			$(el).removeClass("opened-parent");

			var html = handlebars_compile_and_execute("mobile-header", {
				left_icon: 		"<i class='fal fa-chevron-left'></i>",
				left_class: 	"mobile-back-btn",
				
				title: title,
			});
			$(".main-container-header").html(html);
			
			$(".main-container-footer").css( { "display":"none" } );


			if ( !is_history && add_to_history ) {
				history[history_cmd]( data, title, ("#" + name) ); 
			}

			break;
		
		case "image-edit":

			if ( $(".main-container-body-tabs .body-tab-panel.image-edit").length == 0 ) {
				$(".main-container-body-tabs").append( "<div class='body-tab-panel image-edit' style=''></div>" );
			}
			var el = $(".main-container-body-tabs .body-tab-panel.image-edit")[0];

			image_load_edit(data.page_id, data.element_id ,el);

			window.getComputedStyle(el).opacity; // added to dom - force reflow !!!

			$(".main-container-body-tabs .body-tab-panel.opened").addClass("opened-parent");
			$(".main-container-body-tabs .body-tab-panel.opened").removeClass("opened");

			$(el).addClass("opened");
			$(el).removeClass("opened-parent");


			var html = handlebars_compile_and_execute("mobile-header", {
				// left_icon: 		"<i class='fal fa-chevron-left'></i>",
				left_icon: 		"<i class='fal fa-arrow-left'></i>",
				left_class: 	"mobile-back-btn",
				
				title: I18n.t("mobile.image_edit.title"),

				// right_icon:   "<i class='fal fa-trash-alt'></i>",
				// right_class:  "delete-image-from-page-btn",
				// right_txt:    I18n.t("mobile.image_edit.remove_image"),
				right_icons:   ["<i class='fal fa-trash-alt'></i>", "<i class='fal fa-tint-slash'></i>"],
				right_classes:  ["delete-image-from-page-btn", "make-black-white"],
				right_txts:    [I18n.t("mobile.image_edit.remove_image"), "BW"],
			});
			$(".main-container-header").html(html);
			
			// for gallery - some pages overun galleries header (this remark is in several location in code)
			$(".gallery-header .row .col-12").append(html);

			
			$(".main-container-footer").css( { "display":"none" } );

			if ( !is_history && add_to_history ) {
				history[history_cmd]( data, title, ("#" + name) ); 
			}

			break;

		case "image-edit-print":
			
			if ( $(".main-container-body-tabs .body-tab-panel.image-edit").length == 0 ) {
				$(".main-container-body-tabs").append( "<div class='body-tab-panel image-edit-print' style=''></div>" );
			}
			var el = $(".main-container-body-tabs .body-tab-panel.image-edit-print")[0];		

			$(el).html(data.html);

			window.getComputedStyle(el).opacity; // added to dom - force reflow !!!

			$(".main-container-body-tabs .body-tab-panel.opened").addClass("opened-parent");
			$(".main-container-body-tabs .body-tab-panel.opened").removeClass("opened");

			$(el).addClass("opened");
			$(el).removeClass("opened-parent");

			if (data.data) {

				var selector = '.editor-img-container'

				photo_print_set_print_in_container(
					"g_main_album",
					selector,
					data.data,
					{
						editor: 					true,
						force_ready_cb: 	true,
					}
				);

			} else {
				$.ajax({
				  type: 				"get",
				  async: 				true,
				  url: 					data.url,
				  cache:    		false,
				  contentType: 	"application/json",
					dataType: 		"json",
				  success: function(data) {
				  	
						var selector = '.editor-img-container'

						photo_print_set_print_in_container(
							"g_main_album",
							selector,
							data,
							{
								editor: 					true,
								force_ready_cb: 	true,
							}
						);

				  },
				  error: function() {
				  	debugger;
				  }

				});
			}

			if ( !is_history && add_to_history ) {
				history[history_cmd]( data, title, ("#" + name) ); 
			}
			
			break;

		case "uploader-selector":
			if ( $(".main-container-body-tabs .body-tab-panel.uploader-selector").length == 0 ) {
				$(".main-container-body-tabs").append( "<div class='body-tab-panel uploader-selector' style=''></div>" );
			}
			var el = $(".main-container-body-tabs .body-tab-panel.uploader-selector")[0];		

			mobile_uploader_selector(el);

			window.getComputedStyle(el).opacity; // added to dom - force reflow !!!

			$(".main-container-body-tabs .body-tab-panel.opened").addClass("opened-parent");
			$(".main-container-body-tabs .body-tab-panel.opened").removeClass("opened");

			$(el).addClass("opened");
			$(el).removeClass("opened-parent");

			if ( !is_history && add_to_history ) {
				history[history_cmd]( data, title, ("#" + name) );
			}

			break;
			
		case "cover-slector":
			if ( $(".main-container-body-tabs .body-tab-panel.cover-slector").length == 0 ) {
				$(".main-container-body-tabs").append( "<div class='body-tab-panel cover-slector' style=''></div>" );
			}
			var el = $(".main-container-body-tabs .body-tab-panel.cover-slector")[0];		

			var html = handlebars_compile_and_execute("cover-selector", data);

			$(el).html(html);

			var html = handlebars_compile_and_execute("mobile-header", {
				left_icon: 		"<i class='fal fa-chevron-left'></i>",
				left_class: 	"mobile-back-btn",

				title: I18n.t("mobile.template_selector.title", {side:data.left_or_right} ),
			});
			$(".main-container-header").html(html);

			$(".main-container-footer").css( { "display":"none" } );

			window.getComputedStyle(el).opacity; // added to dom - force reflow !!!

			$(".main-container-body-tabs .body-tab-panel.opened").addClass("opened-parent");
			$(".main-container-body-tabs .body-tab-panel.opened").removeClass("opened");

			$(el).addClass("opened");
			$(el).removeClass("opened-parent");

			if ( !is_history && add_to_history ) {
				history[history_cmd]( data, title, ("#" + name) );
			}

			break;

		case "root":

			if ( $(".main-container-body-tabs .body-tab-panel.root").length == 0 ) {
				$(".main-container-body-tabs").append( "<div class='body-tab-panel root opened' style=''></div>" );
			}
			var el = $(".main-container-body-tabs .body-tab-panel.root")[0];

			mobile_root(data.data, el);

			window.getComputedStyle(el).opacity; // added to dom - force reflow !!!

			$(".main-container-body-tabs .body-tab-panel.opened").addClass("opened-parent");
			$(".main-container-body-tabs .body-tab-panel.opened").removeClass("opened");

			$(el).addClass("opened");
			$(el).removeClass("opened-parent");

			if ( !is_history && add_to_history ) {
				history[history_cmd]( data, title, ("#" + name) );
			}

			break;

		case "create_product":

			if ( $(".main-container-body-tabs .body-tab-panel.create_product").length == 0 ) {
				$(".main-container-body-tabs").append( "<div class='body-tab-panel create_product' style=''></div>" );
			}
			var el = $(".main-container-body-tabs .body-tab-panel.create_product")[0];

			mobile_create_product(data.data, el);

			window.getComputedStyle(el).opacity; // added to dom - force reflow !!!

			$(".main-container-body-tabs .body-tab-panel.opened").addClass("opened-parent");
			$(".main-container-body-tabs .body-tab-panel.opened").removeClass("opened");

			$(el).addClass("opened");
			$(el).removeClass("opened-parent");

			if ( !is_history && add_to_history ) {
				history[history_cmd]( data, title, ("#" + name) );
			}

			break;

		case "create_product_from_blocks":

			if ( $(".main-container-body-tabs .body-tab-panel.create_product").length == 0 ) {
				$(".main-container-body-tabs").append( "<div class='body-tab-panel create_product' style=''></div>" );
			}
			var el = $(".main-container-body-tabs .body-tab-panel.create_product")[0];

			mobile_create_product_from_blocks(data.data, el);

			window.getComputedStyle(el).opacity; // added to dom - force reflow !!!

			$(".main-container-body-tabs .body-tab-panel.opened").addClass("opened-parent");
			$(".main-container-body-tabs .body-tab-panel.opened").removeClass("opened");

			$(el).addClass("opened");
			$(el).removeClass("opened-parent");

			if ( !is_history && add_to_history ) {
				history[history_cmd]( data, title, ("#" + name) );
			}

			break;

		case "page-url":

			if ( $(".main-container-body-tabs .body-tab-panel.page-url").length == 0 ) {
				$(".main-container-body-tabs").append( "<div class='body-tab-panel page-url' style=''></div>" );
			}
			var el = $(".main-container-body-tabs .body-tab-panel.page-url")[0];

			var html = handlebars_compile_and_execute("mobile-waiting-spinner", {} );
			$(el).html(html);

			data.sidbar ? $(".main-container-body-tabs .body-tab-panel.opened").addClass("sidebar-parent") : $(".main-container-body-tabs .body-tab-panel.opened").removeClass("sidebar-parent")
			$(".main-container-body-tabs .body-tab-panel.opened").addClass("opened-parent");
			$(".main-container-body-tabs .body-tab-panel.opened").removeClass("opened");

			data.sidbar ?  $(el).addClass("sidebar") : $(el).removeClass("sidebar")
			$(el).addClass("opened");
			$(el).removeClass("opened-parent");

			var url = data.url;
			
			load_url(url, function(res, data) {

				if (res == true) {
					
					setTimeout(function() { 
						if (this.data.template) {
							var html = handlebars_compile_and_execute(this.data.template, data);
							$(this.el).html(html);
						} else {
							$(this.el).html(data);
						}

						if (this.data.url.indexOf('user_items') !== -1) {
							// projects page
							mobile_projects_page_init();
						}
						
						if (this.data.url.indexOf('cart') !== -1) {
							// mobile_cart_update_images();
							mobile_cart_page_init();
						}
						if (this.data.url.indexOf('checkout/address') !== -1) {
							mobile_address_book_init();
						}
						if (this.data.url.indexOf('checkout/payment') !== -1) {
							// mobile_payment_selection_init();
						}
					}.bind(this), 100);

				}

			}.bind( { el:el, data:data } ));

			if (!data.sidbar) { 
				var html = handlebars_compile_and_execute("mobile-header", {
					// left_icon: 		"<i class='fal fa-chevron-left'></i>",
					left_icon: 		"<i class='fal fa-arrow-left'></i>",
					left_class: 	"mobile-back-btn",
					
					title: title,
				});
				$(".main-container-header").html(html);

				// for gallery - some pages overun galleries header (this remark is in several location in code)
				$(".gallery-header .row .col-12").append(html);
			}
			
			// var html = handlebars_compile_and_execute("mobile-footer", {}); // empty footer
			// $(".main-container-footer").html(html);
			// $(".main-container-footer").css( { "display":"flex" } );			
			$(".main-container-footer").css( { "display":"none" } );


			// $(".main-container-body-tabs .body-tab-panel.opened").addClass("opened-parent");
			// $(".main-container-body-tabs .body-tab-panel.opened").removeClass("opened");

			// $(el).addClass("opened");
			// $(el).removeClass("opened-parent");

			if ( !is_history && add_to_history ) {
				history[history_cmd]( data, title, ("#" + name) );
			}

			break;			

	}

	images_set_mimimum_images_note_if_needed( { total_uploading: (g_uploader ? g_uploader.uploading_count() : null), } );

	if (name == "pages") {
		$(".main-container-footer").addClass("shadowed-pages");
	} else if (name == "root") {
		$(".main-container-footer").addClass("shadowed-root");
	}
	else {
		$(".main-container-footer").removeClass("shadowed-root");
		$(".main-container-footer").removeClass("shadowed-pages");
	}


	// => we should push history ???
	// history_push( {}, "editor images", "images" );
	// history_push( {}, "editor pages", "pages" );
}
