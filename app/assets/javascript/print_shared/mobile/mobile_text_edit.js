
function mobile_text_load_edit(page_id, el_id ,container_el) {
	
	var page_container = pages_page_container(page_id);
	var el = page_container.element(el_id);

	// workaround if not set
	el.attrs.data["vertical-align"] = ( el.attrs.data["vertical-align"] ? el.attrs.data["vertical-align"] : "middle");

	var data = { 
		page_container: page_container, 
		el: el, 
		colors: colors_palette, 
		text: ( ( el.attrs && el.attrs.data && el.attrs.data.text && (el.attrs.data.text.length > 1) ) ? el.attrs.data.text : "Type Here"),
	};

	var html = handlebars_compile_and_execute("mobile-text-edit", data);

	$(container_el).html(html);

	helper_mobile_text_container_size(el);
	// helper_mobile_text_size(el);

	text_editor_set_position(el.attrs.data);
	text_editor_set_color(el.attrs.data);
	text_editor_set_font(el.attrs.data);
	helper_mobile_text_size(el)

}

function helper_mobile_text_container_size(el) {
	var el_ratio = el.ratio(); // height / width
	var text_container_ratio = ( $(".mobile-text-editor-type-area").height() / $(".mobile-text-editor-type-area").width() );

	if (el_ratio > text_container_ratio) {
		$(".mobile-text-editor-type-area-input-container").css( { 
			"height":"100%",
			"width": ( $(".mobile-text-editor-type-area").height() * (1/el_ratio) ),
		} );
	} else {
		$(".mobile-text-editor-type-area-input-container").css( { 
			"width":  "100%",
			"height": ( $(".mobile-text-editor-type-area").width() * el_ratio ),
		} );
	}

}	

function helper_mobile_text_size(el, data) {
	// set font size in vw
	// get el.font_size and multiple with the ratio between the el width and the .mobile-text-editor-type-area-input-container width
	// this would show the font size currecly relativly to the size in the element	

	var font_size = el.font_size( (data && data.size) ? data.size : null );
	var el_width = el.pixel_size().width;

	var container_width = $(".mobile-text-editor-type-area-input-container").width();

	var container_font_size = (container_width / el_width)*font_size;

	$(".mobile-text-editor-type-area-input-container .mobile-text-editor-type-area-input").css( { "font-size": (container_font_size + "vw"), } );
}

$( document ).on("input", ".mobile-text-editor-type-area-input", function(ev) {
	if ( $(ev.target).height() > $(event.target).parent().height() ) {
		ev.preventDefault();
		console.warn("text oversize");
		
		$(".mobile-text-editor-type-area-error").css( { "display":"block", } );

		return false;
	} else {
		$(".mobile-text-editor-type-area-error").css( { "display":"none", } );
	}
});

$( document ).on("click", ".mobile-text-editor-top-controllers-main-btn", function(ev) {
	ev.preventDefault();
	
	// hide all
	$(".mobile-text-editor-top-controllers-sub-controllers-element").css( { "display": "none" } ); 

	if ( $(this).hasClass("active") ) {
		$(this).removeClass("active");
		$(".mobile-text-editor-top-controllers-sub-controllers").css( { "display": "none" } ); 
	} else {

		$(".mobile-text-editor-top-controllers-main-btn").removeClass("active");
		$(this).addClass("active");

		// display the parent container
		$(".mobile-text-editor-top-controllers-sub-controllers").css( { "display": "block" } ); 


		var related_class = $(this).attr("data-sub-menu-class")

		// $("." + related_class).css( { "display":"flex" })
		// $("." + related_class).css( { "display":"block" })
		$("." + related_class).css( { "display":"" })

		$(this).css( { "display": "block" } ); 
	}

});

/* getter */
function text_editor_get_position() {
	var data = {
		"align": 					$(".text-edit-position-btn.active[data-css-key='text-align']").attr("data-css-value"),
		"vertical-align": $(".text-edit-position-btn.active[data-css-key='vertical-align']").attr("data-css-value"),
		// "direction":      $(".text-edit-position-btn.active[data-css-key='direction']").attr("data-css-value"),
		"dir":     			 	$(".text-edit-position-btn.active[data-css-key='direction']").attr("data-css-value"),
	}
	return data;
}

$( document ).on("click", ".mobile-text-editor-top-controllers-sub-controllers-position .text-edit-position-btn", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();
	
	$(this).parent().find(".text-edit-position-btn").removeClass("active");
	$(this).addClass("active");

	console.error("TODO handle positions of text in text editor");

	var data = text_editor_get_position();
	text_editor_set_position(data);
});

/* getter */
function text_editor_get_color() {
	var data = {
		"color": $(".mobile-text-editor-top-controllers-sub-controllers-color .mobile-text-editor-top-controllers-sub-controllers-element-btn.active").css("background-color"),
	}
	return data;	
}
$( document ).on("click", ".mobile-text-editor-top-controllers-sub-controllers-color .mobile-text-editor-top-controllers-sub-controllers-element-btn", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();
	
	$(this).parent().find(".mobile-text-editor-top-controllers-sub-controllers-element-btn").removeClass("active");
	$(this).addClass("active");

	var data = text_editor_get_color();
	text_editor_set_color(data);
});

function text_editor_set_position(data) {
	var text_selector = ".mobile-text-editor-type-area-input";

	console.error("do line height the right way on the size function");
	$(text_selector).css( { "line-height": "110%" } );	

	var height = $(text_selector).parent().height();

	$(text_selector).parent().css( { "line-height" : (height + "px") });
	$(text_selector).css( { "vertical-align": data["vertical-align"] } );

	$(text_selector).css( { "text-align": data["align"] } );

	$(text_selector).css( { "direction": data["dir"] } );
}

function text_editor_set_color(data) {
	var text_selector = ".mobile-text-editor-type-area-input";

	$(text_selector).css( { "color": data["color"] } );	
}

/* getter */
function text_editor_get_size() {
	var data = {
		size: $(".mobile-text-edit-size-container input").val(),
	};
	return data;
}

$( document ).on("change input", ".mobile-text-edit-size-container input", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	console.error("TODO handle size slider of text in text editor");

	var page_id = $(".mobile-text-editor").attr("data-page-id");
	var el_id = $(".mobile-text-editor").attr("data-el-id");
	var el = pages_page_container(page_id).element(el_id);

	data = text_editor_get_size() ;
	helper_mobile_text_size(el, data);

});

/* getter */
function text_editor_get_font_family() {
	var data = {
		font: $(".mobile-text-edit-font-container li.active").attr("value"),
	}	
	return data;
}

$( document ).on("click", ".mobile-text-edit-font-container li", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	$(this).parent().find("li.active").removeClass("active");
	$(this).addClass("active");

	var data = text_editor_get_font_family();

	text_editor_set_font(data);

	console.error("TODO handle fonts of text in text editor");
});

function text_editor_set_font(data) {
	var text_selector = ".mobile-text-editor-type-area-input";

	$(text_selector).css( { "font-family": data["font"] } );	
}

/* getter */
function text_editor_get_text() {
	var data = {
		text: $(".mobile-text-editor-type-area-input-container .mobile-text-editor-type-area-input").html(),
	}
	return data;
}

/* getter */
function text_editor_get_all_values() {
	var data = Object.assign(
		text_editor_get_text(),
		text_editor_get_position(),
		text_editor_get_color(),
		text_editor_get_size(),		
		text_editor_get_font_family(),
	)
	
	return data;
}

$( document ).on("click", ".mobile-text-editor-submit", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	var data = text_editor_get_all_values();

	var page_id = $(".mobile-text-editor").attr("data-page-id");
	var el_id = $(".mobile-text-editor").attr("data-el-id");
	var el = pages_page_container(page_id).element(el_id);

	el.attrs.data = data;
	
	el.rerender_html();
	el.init();
	el.post_init();

	console.error("TODO handle submit of text editor");
	do_back_btn();
});


