// var g_product_min_images = 24;
// var g_product_min_images = 12;
// var g_product_min_images = 10;
// var g_product_min_images = 1;
function product_min_images() {
	var res =  ( (g_album.customer_product && g_album.customer_product.num_of_pages) ) ? g_album.customer_product.num_of_pages : 1
	return res;
}


$( document ).on('ready', function(ev) {	

	// $( document ).on('click', ".images-add-images-btn", function(ev) {
	// 	console.log("images-add-images-btn clicked");
	// 	do_load_page("uploader-selector", { }, "מקור העלאה", { persistent: false } );
	// });

	$( document ).on('click', ".images-import-images-btn", function(ev) {

		var html = handlebars_compile_and_execute("mobile-social-import-root");
		$(".mobile-modal").html(html);
		$(".mobile-modal").removeClass("hidden");
		
	});

	$( document ).on('click', ".social-container .social-btn", function(ev) {

		console.log("social import clicked");
		var class_name = $(this).attr("data-class-name");

		window[class_name].instance().import_clicked();
	});

	$( document ).on('click', ".social-container .images-container-el.album-container", function(ev) {

		var class_name = $(this).closest(".social-import-container").attr("data-class-name");

		window[class_name].instance().album_clicked(this);
	});

	$( document ).on('click', ".images-container-page .images-container .images-container-el", function(ev, data={}) {

		// if ( $(ev.target).hasClass('delete-image-btn') || ( $(ev.target).closest(".set-cover-image-btn").length > 0 )  ) {
		if ( $(ev.target).closest(".set-cover-image-btn").length > 0 )  {	

			$(ev.target).closest(".set-cover-image-btn").toggleClass("selected");

		} else if ( $(ev.target).hasClass('delete-image-btn') ) {
			
			if ( $(ev.target).hasClass("selected") ) {
				$(ev.target).removeClass("selected");
			} else {
				$(ev.target).addClass("selected");
			}

			if ( $(".delete-image-btn.selected").length > 0 ) {
				$("a.delete-images-btn").parent().removeClass("hidden");	
			} else {
				$("a.delete-images-btn").parent().addClass("hidden");
			}

		} else {

			if ( $(this).closest(".add-images-to-page").length > 0 ) {
				// this is add images to page
				if ( $(this).hasClass("selected") ) {
					$(this).removeClass("selected");
				} else {
					var selected_count = $(".images-container-page .images-container .images-container-el.selected").length;
					var max_allowed = $(".images-container-page").attr("data-max-selected-photos");	
					if ( max_allowed && ( selected_count >= max_allowed ) ) {
						alert( I18n.t("mobile.images.select_up_to_images", { "count": max_allowed } ) );
					} else {
						$(this).addClass("selected");
					}
				}

			} else {
				
				// this is general images page
				
				// this is not for external images
				if ( $(this).closest(".images-container-page-external").length == 0 ) {
					$(".images-container .images-container-el").not(this).removeClass("selected");
				}
				
				if ( $(this).hasClass("selected") ) {
				
					$(this).removeClass("selected");
				
				} else {

					var selected_count = $(".images-container-page .images-container .images-container-el.selected").length;
					var max_allowed = $(".images-container-page").attr("data-max-selected-photos");	
					if ( max_allowed && ( selected_count >= max_allowed ) ) {
						if (data.alert != false) {
							alert( I18n.t("mobile.images.select_up_to_images", { "count": max_allowed } ) );
						}
					} else {
						$(this).addClass("selected");
					}
				}

				if ( $(this).closest(".images-container-page-external").length > 0 ) { // is external images
				
					selected_count = $(".images-container .images-container-el.selected").length;
					$(".external-selection-counter").html(`
						${selected_count} images selected
					`);

					// var images_ids_arr = jQuery.map( $(".images-container .delete-image-btn.selected"),  function(el) {
					var external_images_arr = jQuery.map( $(".images-container .images-container-el.selected"), function(el) {
						var image = { id: $(el).attr('data-id'), gallery: $(el).closest(".images-container").attr("data-attr-gallery")  };
						return image;
					});
					g_uploader.set_imported_external_images(external_images_arr);

					images_set_mimimum_images_note_if_needed(null);

					if ( g_album.data.auto_design_required ) { 

						if ( g_uploader.images_count() >= product_min_images() )  {
						
							var html = handlebars_compile_and_execute("mobile-footer", {
								title_btn: I18n.t("mobile.images.design_my_album"),
								title_btn_class: "design_book regular-plus language-dir"
							});
							$(".main-container-footer").html(html);	
							$(".main-container-footer").css( { "display":"block" } );

						} else {
							$(".main-container-footer").css( { "display":"none" } );							
						}
					
					} else {
						// do none
					}

				}

			}

		}
		
	});

	$( document ).on('click', ".delete-images-btn", function(ev) {
		ev.preventDefault();
		ev.stopPropagation();
		var images_ids_arr = jQuery.map( $(".images-container .delete-image-btn.selected"),  function(el) {
			var id = $(el).closest(".images-container-el").attr("data-id");
			var uuid = $(el).closest(".images-container-el").attr("data-uuid");
			return { id:id, uuid:uuid };
		})

	  delete_images(images_ids_arr);

	});

	$( document ).on('click', ".social-import-container .images-container .images-container-el", function(ev) {
		console.log("Social images el clicked");

		if ( $(this).hasClass("selected") ) {
			$(this).removeClass("selected");
		} else {
			$(this).addClass("selected");
		}		
	});

	$( document ).on('click', ".social-images-import-btn", function(ev) {
		console.log("social-images-import-btn clicked");
		ev.preventDefault();
		ev.stopPropagation();

		var els = $(".social-import-container .images-container .images-container-el.selected");
		var els_length = $(els).length;


		if (els_length == 0) { alert("Select images to import"); return false; }
	
		var data = [];
		for (var i=0; i<els_length; i++) {
			var el = els[i];
			data.push( {
				url: 				$(el).attr("data-url"),
				created_at: $(el).attr("data-created"),
				name: 			$(el).attr("data-name"),
				width:      $(el).attr("data-width"),
				height: 		$(el).attr("data-height"),
			});
		}	

		var html = handlebars_compile_and_execute("mobile-images-import", {count: els_length} );
		$(".mobile-modal").removeClass("hidden");
		$(".mobile-modal").html(html);
		import_social_images(data);
	});

	$(document).on("click", ".images-container-page-controllers .images-container-page-controllers_select_all", function(ev) {
		ev.preventDefault();
		ev.stopPropagation();

		var els = $(".images-container").not(".hidden").find(".images-container-el").not(".selected") // 
		for (var i = 0; i < els.length; i++) {
			var is_alert = (i == (els.length-1) ) ? true : false
			$(els[i]).trigger("click", { "alert": is_alert } ); // allow alert message only for last element if needed
		}
	})

	$( document ).on('click', ".add-selected-images-to-page", function(ev) {
		ev.preventDefault();
		ev.stopPropagation();

		var page_id = $(".images-container-page.add-images-to-page").attr("data-page-id");
		var product_id = $(".images-container-page.add-images-to-page").attr("data-product-id");
		var element_id = $(".images-container-page.add-images-to-page").attr("data-element-id");
		var left_or_right = $(".images-container-page.add-images-to-page").attr("data-left-or-right");

		selected_images_uuids = jQuery.map( $(".images-container-page.add-images-to-page").find(".images-container-el.selected"), function(el, index) {
			// return [ $(el).attr("data-id"), $(el).attr("data-uuid") ];
			return $(el).attr("data-uuid");
		});

		selected_images_ids = jQuery.map( $(".images-container-page.add-images-to-page").find(".images-container-el.selected"), function(el, index) {
			// return [ $(el).attr("data-id"), $(el).attr("data-uuid") ];
			return $(el).attr("data-id");
		});		

		if ( !(g_album.type == "PhotoPrint") ) {

			if ( element_id && (element_id.length > 1) ) {
				// Adding to element
				mobile_add_image_to_el(
					page_id,
					element_id,
					selected_images_ids[0],
					selected_images_uuids[0],
					function() {
						do_back_btn(); 
					}
				);
			} else {
				// Adding to page
				mobile_add_or_remove_images_to_page( 
					page_id, 
					selected_images_uuids,
					null,
					{
						left_or_right: (g_album.type == "Album") ? left_or_right : "both",
					},
					function() { 
						do_back_btn(); 
					} 
				);

			}

		} else {
			// "PhotoPrint"
			
			// mobile_photo_print_add_images(selected_images_ids, selected_images_uuids);
			do_load_page("pages", { product_id: g_product_id }, I18n.t("mobile.editor.editing"), { add_to_history:true, persistent:true, } );
			// do_back_btn(); => this might has been the first time product created - thus pages not created yet

			mobile_photo_print_add_images(selected_images_ids, selected_images_uuids);
		}

	});	

	document.addEventListener('scroll', function (event) {
    if 	( 
    			event && event.target && event.target.classList &&
    			event.target.classList.value.includes("images-container")  &&
    			( $(event.target).closest(".social-import-container").length > 0 )
    		) {

			// this is the scroll we are trying to capture
			if( Math.abs(event.target.scrollTop - (event.target.scrollHeight - event.target.offsetHeight) ) < 35 ) { 
				// end of scrolling
				console.log("scrolling ends ...")
				var next_page_token = $(event.target).attr("data-next-page-token");
				if (next_page_token && (next_page_token.length > 0)) {

					var album_id = $(event.target).attr("data-album-id");

					// so images will not be displayed multiple times
					$(event.target).attr("data-next-page-token", "");

					// g_api_photos_get_medias(next_page_token,function(data) {
					g_api_photos_get_album_medias( {
				    "albumId": album_id,
    				"pageSize": 30,
    				"pageToken": next_page_token,
    				"cb": function(data) { 

							if (data && (data.status == 200)) {
								// TODO - implement conversion of data.result to a general format for all social medias integration
								console.log("TODO - implement conversion of data.result to a general format for all social medias integration");
								var html = handlebars_compile_and_execute("social_images", data.result.mediaItems);
								$(this.container).append(html);

								// for next scroll to end of scrollbar
								$(this.container).attr("data-next-page-token", data.result.nextPageToken);

							} else {
								console.error("google photos error getting medias")
							}

						}.bind( { container:event.target } ),
					}); 
				
				}
			}

		}
	}, true /*Capture event*/ );

	// jquery event delegation will not work on scroll
	// $(document).on('scroll', ".social-import-container .images-container", function(ev) { console.log("scrolled"); });

	$( document ).on('change', ".images-container-page .select select", function(ev) {
		external_images_gallery_selected();
	});

});

function delete_images(images_ids_arr) {
	console.error("TODO - remove images from pages if used");

	mobile_ui_yes_no_modal( {
		modal: true,
		title: I18n.t("mobile.yes_no.delete_used_photos_title"),
		txt: I18n.t("mobile.yes_no.delete_used_photos_txt"),

		sign: "fal fa-info",

		yes_txt: I18n.t("delete"),
		no_txt: I18n.t("cancel"),

		btn_style: "block",

		yes_cb: function() {
			var images_ids_arr = this
	
			g_uploader.delete_multiple_image(this, function(images_ids_arr) {
				for (var i=0; i<images_ids_arr.length; i++) {
					var id = images_ids_arr[i].id;
					var uuid = images_ids_arr[i].uuid;
			  	var els_by_id = ( id && (id.length > 0)) ?  $(".images-container-el[data-id='" + id + "']") : null;
					var els_by_uuid = ( uuid && (uuid.length > 0)) ? $(".images-container-el[data-uuid='" + uuid + "']") : null;
					if (els_by_id) { els_by_id.remove(); }
					if (els_by_uuid) { els_by_uuid.remove(); }
				}
			});
			$('.mobile-modal').addClass('hidden');
	  }.bind(images_ids_arr),

	});	

}

function images_selected_images() {

	var selected_images_uuids = jQuery.map( $(".images-container-page").find(".images-container-el.selected"), function(el, index) {
		return $(el).attr("data-uuid");
	});

	var selected_images_ids = jQuery.map( $(".images-container-page").find(".images-container-el.selected"), function(el, index) {
		return $(el).attr("data-id");
	});

	return { "ids": selected_images_ids, "uuids": selected_images_uuids };
}

function images_page_init_observer() {
	
	if('IntersectionObserver' in window){
	
		var options = {
			root: $(".images-container-page .images-container ").not(".hidden")[0],
			threshold: [0],
		}

		var observer = new IntersectionObserver(function(observerd) {
			var observerd_length = observerd.length;
			for (var i=0; i<observerd_length; i++) {
				var el = observerd[i];
				if (el.isIntersecting) {
					var inner_image_holder = $(el.target).find(".images-container-el-holder");

					if ( (inner_image_holder.attr("data-is-lazy") == 'true') && (!(inner_image_holder.attr("lazy-loaded"))) ) {
						inner_image_holder.css( { "background-image": ( 'url("' + inner_image_holder.attr("data-background-image") + '")' ) } );
						inner_image_holder.attr("lazy-loaded", true);
					}
				}
			}
		}, options);

		var els = $(".images-container-page .images-container .images-container-el");
		var els_length = els.length;
		for (var i=0; i<els_length; i++) {
			observer.observe(els[i]);
		}

	}

}

function images_load_page( container_el, opts ) {

	if ( $(".images-container-page .images-container").length == 0 ) {
		var html = handlebars_compile_and_execute("mobile-images-page", {
			
			images: (!opts.external_images) ?
								g_uploader.get_images() :
								g_uploader.get_external_images( opts.external_galleries[0] ),

			default_gallery: opts.external_images ? opts.external_galleries[0] : null,

			is_external_images: opts.external_images ? true : false,
			
			is_images_page: opts.is_images ? true : false,
			is_add_images_to_page: opts.is_images ? false : true,
		
		});

		$(container_el).html(html);

		// First update of used photos counter
    var data = pages_data_for_media_count();
    g_uploader.set_used_images(data);


    images_page_init_observer();
	}


	$(".images-container-page .images-container").find(".images-container-el.selected").removeClass("selected");

	var html = handlebars_compile_and_execute("mobile-images-add-images-btn", {});
	$(".images-container-page .images-top-buttons-container").html(html);

	if (opts.is_images) {
	
		if (opts.external_images) {
			// $(".images-container-page").attr("data-max-selected-photos", 3);	
			$(".images-container-page").attr("data-max-selected-photos", (opts.max_selected_photos || 100) );	
		} else {
			$(".images-container-page").removeClass("add-images-to-page");
		}

		$(".images-container-page").attr("data-add-selected", opts.add_selected);
	} else {
	
		$(".images-container-page").addClass("add-images-to-page");
		// $(".images-container-page .images-top-buttons-container").empty();
	
		$(".images-container-page").attr("data-page-id", opts.page_id);
		$(".images-container-page").attr("data-product-id", opts.product_id);
		
		$(".images-container-page").removeAttr("data-element-id");
		$(".images-container-page").attr("data-element-id", opts.element_id);
				
		$(".images-container-page").removeAttr("data-max-selected-photos");	

		if (opts.element_id) {

			// for a specifiec element
			$(".images-container-page").attr("data-max-selected-photos", 1);
		
		} else {
			
			if ( g_auto_template && (g_auto_template.max_images() > 0) ) {
			
				// for auto album 
				var max_images = g_auto_template.max_images();	
			
			} else if ( g_album.type == "PhotoPrint") {
				
				// for photo print
				var max_images = 300;
			} else {

				// ??? why 0 ???
				var max_images = 0;
			}

			if (opts.page_id && !opts.max_selected_photos) {
				
				// for a specific page
				var current_images = Object.keys( pages_page_container(opts.page_id).toJSON( {
																						imgs_only:true, 
																						used_img:true,
																						left_or_right: (g_album.type == "Album") ? opts.left_or_right : "both",
																					}) 
																				).length;
			
			} else if ( g_album.type == "PhotoPrint") {
				
				// for photo print
				current_images = pages_count();
			}
			else {
				
				// ??? //
				var current_images = 0;
			}

			if (!opts.max_selected_photos) {
				$(".images-container-page").attr("data-max-selected-photos", (max_images - current_images) )	
			} else {
				
				if ( g_album.type == "PhotoPrint") {

				} else {
					$(".images-container-page").attr("data-max-selected-photos", opts.max_selected_photos )	
				}

			}
		}

		$(".images-container-page").attr("data-left-or-right", opts.left_or_right);
	}

	var html = handlebars_compile_and_execute("mobile-header", {
		left_icon: 		"<i class='fal fa-chevron-left'></i>",
		left_class: 	"mobile-back-btn",

		title: (opts.title || "תמונות להוספה")
	});
	$(".main-container-header").html(html);

	if (opts.is_images) {
		if ( g_album.data.auto_design_required && ( g_uploader.images_count() >= product_min_images() ) ) {
			
			var html = handlebars_compile_and_execute("mobile-footer", {
				title_btn: I18n.t("mobile.images.design_my_album"),
				title_btn_class: "design_book regular-plus language-dir"
			});
			$(".main-container-footer").html(html);	
			$(".main-container-footer").css( { "display":"block" } );
		
		} else if ( !g_album.data.auto_design_required ) {
			
			var html = handlebars_compile_and_execute("mobile-footer", {
				title_btn: I18n.t("mobile.images.continue_to_editor"),
				title_btn_class: "design_continue_to_pages regular-plus language-dir"
			});
			$(".main-container-footer").html(html);	
			$(".main-container-footer").css( { "display":"block" } );

		} else {
			
			$(".main-container-footer").css( { "display":"none" } );
			// images_set_mimimum_images_note_if_needed(null);
		}
	} else {
		var html = handlebars_compile_and_execute("mobile-footer", {
			// title_btn: "הוספת התמונות",
			title_btn: I18n.t("mobile.images.add_selection_button"),
			title_btn_class: "add-selected-images-to-page regular-plus"
		});
		$(".main-container-footer").html(html);	
		$(".main-container-footer").css( { "display":"block" } );		
	}


	// history_push( {}, "editor images", "images" );

}

function images_set_mimimum_images_note_if_needed(images_counter) {

	if ( !value_if_defined('g_album') || !(g_album && g_album.data) ) { return; }

	$(".main-container-notes").removeClass("active");
	$(".main-container-notes").removeClass("error-note");


	if ( 	
				( g_album.data.auto_design_required == true ) && // we are auto design (albums)
				( g_uploader && (g_uploader.images_count() < product_min_images()) ) && // minimal images not yet added
				( is_page_of_name("images") ) // this is images page
		 ) {
		
		$(".main-container-notes").addClass("active");
		
		// var text = "(עלו " + g_uploader.images_count() + " תמונות)" + "</br>";
		// text += "חסרות לפחות " + (product_min_images() - g_uploader.images_count()) + " תמונות לעיצוב הספר";
		var text = "(" + I18n.t("mobile.images_notes.imported_images", { count: g_uploader.images_count() } ) + ")<br>"
		text += I18n.t("mobile.images_notes.required_for_design", { count: (product_min_images() - g_uploader.images_count()) } )
		
		$(".main-container-notes .txt").html(text);
		$(".main-container-notes").addClass("error-note");

	} else {

		if (images_counter && images_counter.total_uploading && (images_counter.total_uploading > 0) ) {
			$(".main-container-notes").addClass("active");
			// var text = I18n.t("text.uploading", {count: images_counter.total_uploading} );
			// if ( is_page_of_name("images") ) {
			// 	text += "</br>";
			// 	text += "ניתן להמשיך בעיצוב";
			// }
			var text = I18n.t("mobile.images_notes.uploading", {count: images_counter.total_uploading} );
			$(".main-container-notes .txt").html(text);
		}

	}

}

function mobile_uploader_selector( container_el ) {

	var html = handlebars_compile_and_execute("mobile-images-uploader-selector", {
		
	});

	$(container_el).html(html);

	var html = handlebars_compile_and_execute("mobile-header", {
		left_icon: 		"<i class='fal fa-chevron-left'></i>",
		left_class: 	"mobile-back-btn",

		title: "מקור העלאה"
	});
	$(".main-container-header").html(html);

	$(".main-container-footer").css( { "display":"none" } );

}


function append_images_container(l_uuid, data, pos, image_left) {

	if (data) {
		
		// append to images page
		var html = handlebars_compile_and_execute("mobile_image", { data:data, lazy:false } );
		if (pos == 0) {
			$(".images-container-page .images-container").prepend(html);
		} else {
			$(".images-container-page .images-container > div:nth-child(" + (pos) + ")").after(html);
		}

		// append to images panel
		var panel_container = $(".pages-container-actions .pages-container-actions-panels-panel[data-container-name='" + 'photos' + "']");
		if (panel_container.length > 0) {
			var html = handlebars_compile_and_execute("mobile_image_panel_image", data);

			if (pos == 0) {
				panel_container.find(".images-container").prepend(html);
			} else {
				panel_container.find(".images-container > div:nth-child(" + (pos) + ")").after(html);
			}
		}

	}

	$(".import-image-count").html(image_left);

	if (image_left == 0) {
		
		if (
				 ( g_uploader.images_count() >= product_min_images() ) && 
				 ( g_album.data.auto_design_required == true ) &&
				 ( g_album.data.album_ready == false)
			 )
		{
			
			var html = handlebars_compile_and_execute("mobile-footer", {
				title_btn: I18n.t("mobile.images.design_my_album"),
				title_btn_class: "design_book regular-plus language-dir"
			});
			$(".main-container-footer").html(html);
			$(".main-container-footer").css( { "display":"flex" } );

		} else {
			// do none - keep the previous add image btn if exist
			// $(".main-container-footer").css( { "display":"none" } );
		}

		$(".mobile-modal").addClass("hidden");
	}

}

function new_images_selected(count) {

	// if ( validate_current_state("uploader-selector") ) {
	if ( validate_current_state("images") ) {	
		// do_back_btn(); => not needed import is now done from images page
	} else {
		console.error("new_images_selected - state error TODO - check");
	}

	var html = handlebars_compile_and_execute("mobile-images-import", {count: count} );
	$(".mobile-modal").removeClass("hidden");
	$(".mobile-modal").html(html);
}

function external_images_gallery_selected(gallery, load_all = false) {
	
	if (load_all == true) {
		var galleries = g_uploader.get_external_galleries();
		for (var i=0; i<galleries.length; i++) {
			var g_name = galleries[i];
			external_images_gallery_selected( g_name, false )	
		}
	}

	if ( !gallery ) {
		gallery = $(".images-container-page .select select").val();
	}

	$(".images-container-page .images-container").addClass("hidden");
	$(".images-container-page .images-container.hidden").css( { "display": "none" } );
	
	if ( $(`.images-container-page .images-container[data-attr-gallery="${gallery}"`).length > 0 ) {
		$(`.images-container-page .images-container[data-attr-gallery="${gallery}"`).removeClass("hidden");
		$(`.images-container-page .images-container[data-attr-gallery="${gallery}"`).css( { "display": "flex" } );
	} else {
		
		var html = handlebars_compile_and_execute("mobile_images", {
			images: g_uploader.get_external_images(gallery),
			gallery_name: gallery,
			is_external: true,
		});

		$(".images-container-page .images-container-page-inner").append(html);
		
		images_page_init_observer();
	}

	
	
};
