
	function helper_mobile_text_font_list(el) {
		var fonts = Object.keys(Fonts.font_list);
    var fonts_length = fonts.length;

    var font_list_elements_str = "";
    
    // TODO assign it
    var selected = null;

    for (var i=0; i<fonts_length; i++) {
      var font_name = fonts[i];

      var font_option = "" + 
          "<li " + 
          "class=\"" + font_name_class(font_name) + ( (font_name == el.attrs.data.font) ? " active" : "") + "\" " +
          "value='" + font_name + "' " +
          ( (selected == font_name) ? "selected" : "") +
          ">" + 
          font_name + " " +
          ( (Fonts.font_list[font_name].language == "heb") ? "(עברית)" : "" ) +
          "</li>";

      font_list_elements_str += font_option;
  
      g_font_manager.load_font_family( font_name, null, function() { /*Do nothing*/ } );
    }

    return new Handlebars.SafeString(font_list_elements_str);
    
	}

	function helper_mobile_image_cropper_container_size(el) {
		var ratio = el.ratio();
		if (ratio <= 1) {
			return new Handlebars.SafeString("width:85%; height:0px; padding-top:" + ratio*85 + "%;");
		} else {
			return new Handlebars.SafeString("width:65%; height:0px; padding-top:" + ratio*65 + "%;");
		}
	}

	function helper_mobile_image_cropper_img_src(el) {
		var img_obj = g_uploader.image_from_ids(el.attrs.img.id, el.attrs.img.uuid);

		// return new Handlebars.SafeString( img_obj.medium );
		return new Handlebars.SafeString( img_obj.big );
	}

	function helper_mobile_img_template_by_type(template) {
		var res = "";
		if (template.type == "Canvas") {
			res = canvas_img_template(template);
		} else if (template.type == "Calendar") {
			res = calendar_img_template(template);
		} else if (template.type == "Album") {
			album_img_template(template);
		}

		return new Handlebars.SafeString( res );
	}

	function helper_mobile_rgb_hex_is_equal(rgb, hex, options) {
		
		var hex_to_rgb_hash = hexToRgb(hex);
		// hex_to_rgb_hash == null => hexToRgb(hex) did not work
		if ( (hex_to_rgb_hash == null) && ( (hex.indexOf("rgb") >= 0) || (hex.indexOf("rgba") >= 0) || (hex.indexOf("RGB") >= 0) ) ) {
			var splited_rgb_arr = hex.split('(')[1].split(')')[0].split(',');
			hex_to_rgb_hash = { "r":splited_rgb_arr[0], "g":splited_rgb_arr[1], "b":splited_rgb_arr[2] };
		}

		if ( rgb && ( (rgb.indexOf("rgb") >= 0) || (rgb.indexOf("rgba") >= 0) || (rgb.indexOf("RGB") >= 0) ) ) {
			var splited_rgb_arr = rgb.split('(')[1].split(')')[0].split(',');
			var rgb_hash = { "r":splited_rgb_arr[0], "g":splited_rgb_arr[1], "b":splited_rgb_arr[2] };
		} else {
			if (rgb == hex) { // rgb is also hex string ??? safari ???
				return options.fn(this);	
			} else {
				var rgb_hash = null;
			}
		}

		if ( rgb_hash && hex_to_rgb_hash && 
					( parseInt(rgb_hash.r) == parseInt(hex_to_rgb_hash.r) ) && 
					( parseInt(rgb_hash.g) == parseInt(hex_to_rgb_hash.g) ) && 
					( parseInt(rgb_hash.b) == parseInt(hex_to_rgb_hash.b) ) 
			) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}
	}

	function canvas_img_template(t) {
		return calendar_img_template(t);		
	}

	function calendar_img_template(t) {
		res = "<div class='img_template " + t.type + "'>\
						<div style='position:absolute; left:0px; top:0px; bottom:0px; right:0px;'>\
							<div class='img_container'>\
								<img class='template-select-img' src='" + t.preview_cover + "'/>\
								<div class='img-template-over' style='position:absolute; left:0px; top:0px; width:100%;'>\
									<img src='" + g_spiral_image_url + "' draggable='false' class='' style='width: 100%; position: absolute; height: 22px; left: 0px; top: -14px;'>\
								</div>\
							</div>\
						</div>\
					</div>"

		return res;
	}

	function album_img_template(t) {
		
	}

	function product_default_name(product_group) {
		return ( product_group.data.name + " - " + new Date().toLocaleDateString() );
	}

	function mobile_calendar_month_selector(calendar) {
		var date_options = { weekday: undefined, year: 'numeric', month: 'long', day: undefined };
		var date = new Date; // current time
		var html_str = "";
		html_str += "\
			<div class='container' style=''>\
				<div class='row'>\
					<div class='language-dir language-txt-align offset-lg-4 col-lg-4 offset-md-3 col-md-6 col-12'>\
						<p>" + I18n.t("mobile.calendar.select_month") + " </p>\
					</div>\
				</div>\
				<div class='row'>\
					<div class='language-dir language-txt-align offset-lg-4 col-lg-4 offset-md-3 col-md-6 col-12'>\
						<form class='calendar-start-date-form form-horizontal language-dir language-txt-align'>\
							<select name='product[from_month]' class='form-control language-dir language-txt-align'>";

		for (var i=0; i<24; i++) {
			var date_str = date.toLocaleDateString(undefined, date_options);
			var date_value = date.getFullYear() + "-" + (date.getMonth() + 1) + "-01";

			html_str += "<option value='" + date_value + "'>" + date_str + "</option>";

			date = new Date(date.setMonth(date.getMonth() + 1));
		}

		html_str += "\
							</select>\
							<input type='submit' value='" + I18n.t("update") + "' class='btn client_button_ok' style='margin-top:1em; margin-left:0; margin-right:0; width:100%' />\
						</form>\
					</div>\
				</div>\
			</div>\
		";

		return new Handlebars.SafeString( html_str );
	}

	function helper_external_images_galleries_options(active) {
		var galleries = g_uploader.get_external_galleries();
		var html = "";
		for (var i=0; i<galleries.length; i++) {
			html += `
				<option value="${galleries[i]}" ${(active==galleries[i])? "selected" : ""}>
					${galleries[i]}
				</option>
			`;
		}

		return new Handlebars.SafeString(html);
	}

	function helper_is_album_with_cover(options) {
		if 	( 	g_album.data.edit_cover && 
						( (g_album.data.edit_cover == "use_cover") || (g_album.data.edit_cover == "cover_size") )
				) {
			return options.fn(this);
		}
		else {
			return options.inverse(this);
		}
	}

	function mobile_register_partials() {
		var source = document.getElementById("mobile-images-page-header").innerHTML;
  	Handlebars.registerPartial("mobile-images-page-header", source);  

  	var source = document.getElementById("mobile-images-add-images-btn").innerHTML;
  	Handlebars.registerPartial("mobile-images-add-images-btn", source);  

  	var source = document.getElementById("mobile_images_external").innerHTML;
  	Handlebars.registerPartial("mobile_images_external", source);

  	var source = document.getElementById("mobile_images").innerHTML;
  	Handlebars.registerPartial("mobile_images", source);
  	
  	var source = document.getElementById("mobile_image").innerHTML;
  	Handlebars.registerPartial("mobile_image", source);

  	var source = document.getElementById("mobile_image_panel_image").innerHTML;
  	Handlebars.registerPartial("mobile_image_panel_image", source);

  	var source = document.getElementById("mobile_templates_panel_image").innerHTML;
  	Handlebars.registerPartial("mobile_templates_panel_image", source);

  	var source = document.getElementById("social_images").innerHTML;
  	Handlebars.registerPartial("social_images", source);

  	var source = document.getElementById("social_image").innerHTML;
  	Handlebars.registerPartial("social_image", source);

  	var source = document.getElementById("mobile-cart-and-projects-product-image").innerHTML;
  	Handlebars.registerPartial("mobile-cart-and-projects-product-image", source);

		/*********************************
	 	*       Register Helpers
	 	**********************************/
	 	Handlebars.registerHelper("mobile_image_cropper_container_size", helper_mobile_image_cropper_container_size);
	 	Handlebars.registerHelper("mobile_image_cropper_img_src", helper_mobile_image_cropper_img_src);
	 	Handlebars.registerHelper("mobile_img_template_by_type", helper_mobile_img_template_by_type);
	 	Handlebars.registerHelper("product_default_name", product_default_name);

		Handlebars.registerHelper("mobile_calendar_month_selector", mobile_calendar_month_selector);	 	

		Handlebars.registerHelper("mobile_text_font_list", helper_mobile_text_font_list);

		Handlebars.registerHelper("mobile_rgb_hex_is_equal", helper_mobile_rgb_hex_is_equal);		

		Handlebars.registerHelper("external_images_galleries_options", helper_external_images_galleries_options);

		console.error("why loadded each page? from: print shared");
		Handlebars.registerHelper("is_album_with_cover", helper_is_album_with_cover);
		
	}
