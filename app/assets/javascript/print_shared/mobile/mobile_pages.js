window.interact_initialized = false;

var mobile_pages_position = { x: 0, y: 0 };
var mobile_pages_beforeY  = 0;
var mobile_pages_afterY   = 0;

var mobile_pages_current_draggable = null;
var mobile_pages_current_draggable_origin = null;

var g_all_album_pages = {};

// function pages_mobile_init() {
//   g_all_album_pages = {};  
// }  

// function pages_page_container(page_id) {
//   return g_all_album_pages[page_id];
// }

// function pages_delete_all_page_container() {
//   var keys = Object.keys(g_all_album_pages);
//   for (var i=0; i<keys.length; i++) {
//     pages_delete_page_container( keys[i] );
//   }
// }

// function pages_delete_page_container(page_id) {
//   delete g_all_album_pages[page_id];
// }

// function pages_count() {
//   return Object.keys(g_all_album_pages).length; 
// }

// function pages_actual_page_count() {
//   var actual_count = 0;
  
//   var all_indexes = $(".page-previewer").map( function() { return $(this).attr("data-index") } );

//   for (var i=0; i<all_indexes.length; i++) {
    
//     var index = all_indexes[i];

//     // var page = pages_page_container(keys[i]);
//     var page =  window[("g_pages_album_" + index)];
    
//     if (page) {   
//       var page_type = (page.page_type || page.init_data.page_type);

//       if (page_type == "album_page_last")    { actual_count += 1; }
//       if (page_type == "album_page_first")   { actual_count += 1; }
//       if (page_type == "album_regular")      { actual_count += 2; }  
//     }

//   }

//   return actual_count;
// }

// function pages_extra_pages() {
//   var page_count = pages_actual_page_count();
//   var extra_pages = ( page_count - g_album.customer_product.num_of_pages );
//   extra_pages = (extra_pages > 0) ? extra_pages : 0;
//   return extra_pages;
// }

// function pages_data_for_media_count() {
//   var res = [] 
//   var keys = Object.keys(g_all_album_pages);

//   for (var i=0; i<keys.length; i++) {
//     var page = pages_page_container(keys[i]);
//     res.push(page.init_data);
//   }

//   return res;
// }

// function pages_get_all_ids() {
//   return Object.keys(g_all_album_pages);
// }


function ignoreTouch(e) {
  //debugger;
  // always prevent default => see https://bugs.webkit.org/show_bug.cgi?id=231161
  // to avoid safari magnifier on drag
  if (e.type == "touchstart") {
    //console.log("allways prevent default");
    //e.preventDefault(); 
    // e.stopPropagation();
  } else {
    // debugger;
  }
  
  if (mobile_pages_current_draggable_origin) {
    // android workaround
    // this is to fix android issue => after clone start moving it is terminated by interact (probably due to scrollable container event)
    // ==> thus if we are during moving of clone we ignore this event
    e.preventDefault(); 
  }
}

function load_interact() {

  if (window.interact_initialized == true) {
    console.error("interact allready initialized");
  }

  if (false) {
    // see https://github.com/taye/interact.js/issues/561
    interact.addDocument(window.document, {
      events: { passive: false },
    });
  }

  // android workaround see above
  
  $("#pages-container-inner")[0].addEventListener('touchstart', ignoreTouch);
  $("#pages-container-inner")[0].addEventListener('touchmove', ignoreTouch);
  $("#pages-container-inner")[0].addEventListener('touchend', ignoreTouch);


  /*************************************
  *              Draggables
  **************************************/
  // interact(".el-container-parent.image-container")
  // interact('.el-container-parent.clone')
  // interact(".el-container-parent-clone")
  interact(".el-container-parent.image-container")
  // interact(".clone")
    .draggable({ 
      manualStart: true,
      // manualstart: false,
      
      // allowFrom: ".el-container-parent",
      
      inertia: false,
      
      // autoScroll: true, 
      // autoScroll: false, 
      autoScroll: {container: document.getElementById('pages-container-inner')},

      listeners: {
        start (event) {
          console.log(event.type, event.target);

          mobile_pages_position.x = 0;
          mobile_pages_position.y = 0;
          mobile_pages_beforeY    = 0;

        },
        end (event) {
          try {
            if (event.target && mobile_pages_current_draggable_origin) {
              $(event.target).remove();

              // this is a workaround to remove all gost clones who might have been "lost"...
              $("#pages-container-inner .clone").remove();

              var src_page_id     = $(mobile_pages_current_draggable_origin).closest(".page").attr("data-page-id");
              var src_el_id       = $(mobile_pages_current_draggable_origin).attr("data-id");
              
              var src_el    = (src_page_id && src_el_id) ? 
                                  pages_page_container(src_page_id).element(src_el_id) :
                                  null;

              var page_id = $(".can-drop").closest(".page-previewer").find(".page").attr("data-page-id");
              var image_obj = src_el.get_image();
              
              var single = $('.can-drop').closest(".page-previewer").hasClass("single");

              // need to remove both when removing or adding
              if ( $('.can-drop').hasClass("removing") || $('.can-drop').hasClass("adding") ) {

                if (image_obj) {
                  var images_uuids = [ image_obj.uuid ];

                  // only remove from page
                  mobile_add_or_remove_images_to_page( 
                    src_page_id, 
                    null, 
                    images_uuids,
                    {
                      left_or_right: ( (g_album.type == "Album") && (!single) ) ? src_el.left_or_right_side() : "both",
                    },
                    function() {
                      console.log("mobile_add_or_remove_images_to_page cb called for removing image");
                    }
                  );

                }
              } 
              if ( $('.can-drop').hasClass("adding") ) {

                var left_or_right = $('.can-drop').hasClass("left") ? "left" : "right";

                if (image_obj) {
                  var images_uuids = [ image_obj.uuid ];

                  mobile_add_or_remove_images_to_page( 
                    page_id, 
                    images_uuids,
                    null, 
                    {
                      left_or_right: ( (g_album.type == "Album") && (!single) ) ? left_or_right : "both",
                    },
                    function() { 
                      console.log("mobile_add_or_remove_images_to_page cb called");
                    }.bind( { src_page_id: src_page_id, images_uuids: images_uuids, } ) 
                  );
                }

              }

              if (false) { 

                if ( $('.can-drop').hasClass("page-drop-area-container") ) {

                  // add & remove from page Or only remove from page

                  var page_id = $(".can-drop").closest(".page-previewer").find(".page").attr("data-page-id");
                  var image_obj = src_el.get_image();

                  if (image_obj) {
                    var images_uuids = [ image_obj.uuid ];

                    if (page_id == src_page_id) {
                      // only remove from page
                      mobile_add_or_remove_images_to_page( 
                        src_page_id, 
                        null, 
                        images_uuids,
                        {
                          left_or_right: src_el.left_or_right_side(),
                        },
                        function() {
                          console.log("mobile_add_or_remove_images_to_page cb called for removing image");
                        }
                      );
                      
                    } else {
                    
                      if (image_obj) {
                        
                        mobile_add_or_remove_images_to_page( 
                          page_id, 
                          images_uuids,
                          null, 
                          {},
                          function() { 
                            console.log("mobile_add_or_remove_images_to_page cb called");

                            // now remove them from orig page
                            mobile_add_or_remove_images_to_page( 
                              this.src_page_id, 
                              null, 
                              this.images_uuids,
                              {},
                              function() {
                                console.log("mobile_add_or_remove_images_to_page cb called for removing image");
                              }
                            );

                          }.bind( { src_page_id: src_page_id, images_uuids: images_uuids, } ) 
                        );
                      }

                    }

                  } // if (image_obj)

                }

              } 

              if ( $('.can-drop').hasClass("el-container-parent") ) {
              
                // replace between images 
                var target_page_id  = $('.can-drop').closest(".page").attr("data-page-id");
                var target_el_id    = $('.can-drop').attr("data-id");

                var target_el = (target_page_id && target_el_id) ?
                                  pages_page_container(target_page_id).element(target_el_id) :
                                  null;

                // if ( (src_page_id != target_page_id) && (src_el_id != target_el_id) ) {
                if (src_el_id != target_el_id) {  
                  var src_el_img    = src_el ? src_el.get_image() : null;
                  var targer_el_img = target_el ? target_el.get_image() : null;

                  src_el.mobile_remove_img();
                  target_el.mobile_remove_img();
                  if (src_el_img) {
                    target_el.mobile_img(src_el_img);
                  }
                  if (targer_el_img) {
                    src_el.mobile_img(targer_el_img);
                  }
                  
                }

              }

            }

            $(".can-drop").removeClass("can-drop");
            mobile_pages_current_draggable_origin = null;
            console.log("all can-drop removed");


            // do pages general dropables areas
            $(".page-previewer .page-actions-container").addClass("visible");

            return false;
          
          } catch (e) {
            console.error(e);
          }

        }, 
        move (event) {
          try {
            if (event.interaction.interacting()) {
              // console.log('moving');
              mobile_pages_position.x += event.dx
              mobile_pages_position.y += event.dy

              event.target.style.transform =
                `translate(${mobile_pages_position.x}px, ${mobile_pages_position.y}px)`;

              do_images_move_draggable(event);
            } else {
              console.log('not interacting')
            }

          } catch (e) {
            console.error(e);
          }
            
        }        
      }
    })
    
    

  if (window.interact_initialized == false) {  // do not initialize hold event multiple time while it is probably handled by dom which once is enough
    // interact('.el-container-parent:not(.clone)').on('hold', 
    
    // interact(".el-container-parent.image-container").pointerEvents({
    //   holdDuration: 400, // timeout to receive hold event
    // })
    // .on('hold',
    // interact(".el-container-parent.image-container").on('hold',   
    interact(".el-container-parent.image-container").pointerEvents({
      // timeout to receive hold event
      // holdDuration: 100,
      // holdDuration: 25, // too short for scrolling
      // holdDuration: 50, // too short in windows chrome - trigger hold on click
      holdDuration: 225, // found it to be best fot both windows and osx
    }).on('hold',   
      function (event) { 
        try {
          event.preventDefault();
          event.stopPropagation();
          
          console.log("hold");

          var interaction = event.interaction;
          
          var target = $(event.target).closest(".el-container-parent")[0];

          var page_id = $(event.target).closest(".page").attr("data-page-id");
          var el_id = $(event.target).closest(".el-container-parent").attr("data-id");
          var el = pages_page_container(page_id).element(el_id);

          var width =  $(target).width();
          var height = $(target).height();

          var clone = target.cloneNode(true);
          
          // var img = el.get_cropped( { widht:width, height:height, } );
          // just for testing
          // var img = "https://wowpro.ams3.digitaloceanspaces.com/dev/media/Album/0e8eec3d-286e-4fe3-811f-0a079793f5d8/10273/big/7883CDC6-C50F-43C7-8683-22ACEF85A31B.jpeg"

          // var clone = $(
          //                 "<div class='el-container-parent-clone'> \
          //                   <img src='" + img + "' style='width:100%; height:100%'>\
          //                 </div>"
          //              )[0];
          // var clone = $(
          //                 "<div class='el-container-parent image-container' style='background-color:black;'> \
          //                 </div>"
          //              )[0];        

          // do not copy id
          $(clone).attr("id", "dummy-123");

          var top = $("#pages-container-inner").scrollTop() + ( $(target).offset().top - $("#pages-container-inner").offset().top );
          var left  = $(target).offset().left - $("#pages-container-inner").offset().left;

          var resize_ratio = ( ((width >= height) && (width < 80)) || ((height > width) && (height < 80)) ) ?
                              1.35 : // very small => make it bigger
                              ( ((width >= height) && (width < 120)) || ((height > width) && (height < 120)) ) ?
                                1.2 : // little small => make it little bigger  
                                ( ((width >= height) && (width < 150)) || ((height > width) && (height < 150)) ) ?
                                1.1 : // medium => make it little smaller
                                0.85; // big...

          var clone_width   = parseInt(width*resize_ratio);
          var clone_height  = parseInt(height*resize_ratio);

          var clone_left  = left - (clone_width - width)/2;
          var clone_top   = top - (clone_height - height)/2;

          $(clone).css( { 

            position:"absolute",

            // width: $(target).width(), 
            // width: width,
            width: clone_width,
            // height: $(target).height(), 
            // height: height,
            height: clone_height,

            // top: top,
            // left: left,
            top: clone_top,
            left: clone_left,

            opacity: 0.7,

            "z-index": 99999,

            "box-shadow":
              "0 1px 0.5px rgba(0, 0, 0, 0.28),\
              0 2.7px 1.5px rgba(0, 0, 0, 0.4),\
              0 3.5px 2.5px rgba(0, 0, 0, 0.48),\
              0 4.8px 3.9px rgba(0, 0, 0, 0.60),\
              0 7.8px 5.4px rgba(0, 0, 0, 0.68),\
              0 15px 12px rgba(0, 0, 0, 0.96)",

            "-webkit-box-shadow":  
              "0 1px 0.5px rgba(0, 0, 0, 0.28),\
              0 2.7px 1.5px rgba(0, 0, 0, 0.4),\
              0 3.5px 2.5px rgba(0, 0, 0, 0.48),\
              0 4.8px 3.9px rgba(0, 0, 0, 0.60),\
              0 7.8px 5.4px rgba(0, 0, 0, 0.68),\
              0 15px 12px rgba(0, 0, 0, 0.96)",

              "-ms-touch-action": "none",
              "touch-action": "none",
          });

          $(clone).find('*').css( { "-ms-touch-action": "none", "touch-action": "none",} );


          $(clone).addClass("clone");

          // $(clone).find("*").css( { "opacity":1, } );

          // $(".pages-container-inner")[].appendChild(clone);
          $("#pages-container-inner").append(clone);

          // $(target).children().css( {"visibility":"hidden"} );
          // $(target).addClass("can-drop");
          $(target).css( {"border":"1px solid black"} );

          interaction.start({ name: 'drag' }, event.interactable, clone);
          mobile_pages_current_draggable = clone;
          mobile_pages_current_draggable_origin = target;

          // do pages general dropables areas
          if (pages_page_container(page_id).page_type != "album_cover") {

            $(".page-previewer:not(.cover) .page-actions-container").removeClass("visible");
            
            // $(".page-previewer:not(.cover) .page-actions-container").removeClass("removing");
            // $(".page-previewer:not(.cover) .page-actions-container").addClass("adding");
            $(".page-previewer:not(.cover) .page-drop-area-container .left").removeClass("removing");
            $(".page-previewer:not(.cover) .page-drop-area-container .right").removeClass("removing");
            $(".page-previewer:not(.cover) .page-drop-area-container .left").addClass("adding");
            $(".page-previewer:not(.cover) .page-drop-area-container .right").addClass("adding");
            
            
            // var actions_el = $(event.target).closest(".page-previewer").find(".page-actions-container");
            var drop_area_container_el = $(event.target).closest(".page-previewer").find(".page-drop-area-container");

            if ( (drop_area_container_el).closest(".page-previewer").hasClass("double") ) {
              if (el.left_or_right_side() == "left") {
                drop_area_container_el.find(".left").removeClass("adding");
                drop_area_container_el.find(".left").addClass("removing");
              } else {
                drop_area_container_el.find(".right").removeClass("adding");
                drop_area_container_el.find(".right").addClass("removing");
              }
            } else {
              drop_area_container_el.find(".right").removeClass("adding");
              drop_area_container_el.find(".left").removeClass("adding");
              drop_area_container_el.find(".right").addClass("removing");
              drop_area_container_el.find(".left").addClass("removing");              
            }

            // actions_el.removeClass("adding");
            // actions_el.addClass("removing");
            
          }
          
          return false;

        } catch (e) {
          console.error(e);
        }
      }
    );

    interact(".el-container-parent.image-container").on('tap',   
      function (event) { 
        try {
          event.preventDefault();
          event.stopPropagation();

          var element_id  = $(event.currentTarget).attr("data-id");
          var page_id     = $(event.currentTarget).closest(".page").attr("data-page-id");
         
          if ( $(event.currentTarget).hasClass('contain') ) {
            do_load_page("image-edit", { page_id: page_id, element_id: element_id, }, I18n.t("mobile.image_edit.title"), { add_to_history:true, persistent:true, } );
          } else {

            do_load_page("add-images", { product_id: g_product_id, page_id:page_id, element_id:element_id }, 
                            I18n.t("mobile.images.select_photo"), { add_to_history:true, persistent: false } );

          }

          return false;

        } catch (e) {
          console.error(e);
        }

      }
    );

    interact(".el-container-parent.text-container").on('tap',
      function (event) { 
        try {
          event.preventDefault();
          event.stopPropagation();
          console.log("text-tapped");

          var element_id  = $(event.currentTarget).attr("data-id");
          var page_id     = $(event.currentTarget).closest(".page").attr("data-page-id");

          do_load_page("text-edit", { page_id: page_id, element_id: element_id, }, I18n.t("mobile.text_editor.title"), { add_to_history:true, persistent:false, } );
        } catch (e) {
          console.error(e);
        }
      }
    )



  }

  interact(document.getElementById('pages-container-inner')).on('scroll', function (event) {
    try {
      console.log("scrolling");

      mobile_pages_beforeY = mobile_pages_afterY;
      // mobile_pages_beforeX = mobile_pages_afterX;

      if(mobile_pages_afterY==0 && mobile_pages_beforeY==0) //needed because otherwise it works only if you scroll the first time
        mobile_pages_beforeY = this.scrollTop;
      // if(mobile_pages_afterX==0 && mobile_pages_beforeX==0) //needed because otherwise it works only if you scroll the first time
        // mobile_pages_beforeX =this.scrollLeft;
      
      mobile_pages_afterY = this.scrollTop;
      // mobile_pages_afterX = this.scrollLeft;

      mobile_pages_position.y = ( mobile_pages_position.y + (mobile_pages_afterY - mobile_pages_beforeY) );  

      if (mobile_pages_current_draggable) {
        console.log("change dragable !!!");
        mobile_pages_current_draggable.style.transform =
                `translate(${mobile_pages_position.x}px, ${mobile_pages_position.y}px)`;
      }

    } catch (e) {
      console.error(e);
    }  
    
  });
  /*************************************
  *           END - Draggables
  **************************************/



  /*************************************
  *              Dropables
  **************************************/
  if (false) {
    interact('.el-container-parent').dropzone({
      // only accept elements matching this CSS selector
      accept: '.el-container-parent',
      // Require a 75% element overlap for a drop to be possible
      overlap: 0.5,

      // listen for drop related events:

      ondropactivate: function (event) {
        // add active dropzone feedback
        // event.target.classList.add('drop-active')
      },
      ondragenter: function (event) {
        var draggableElement = event.relatedTarget;
        var dropzoneElement = event.target;

        $(dropzoneElement).addClass("can-drop");

        // feedback the possibility of a drop
        // dropzoneElement.classList.add('drop-target')
        // draggableElement.classList.add('can-drop')
        // draggableElement.textContent = 'Dragged in'
        console.log("drop enter");
        console.log("drop enter");
        console.log("drop enter");
      },
      ondragleave: function (event) {
        // remove the drop feedback style
        // event.target.classList.remove('drop-target')
        // event.relatedTarget.classList.remove('can-drop')
        // event.relatedTarget.textContent = 'Dragged out'

        var draggableElement = event.relatedTarget;
        var dropzoneElement = event.target;

        $(dropzoneElement).removeClass("can-drop");

        console.log("drop leave");
        console.log("drop leave");
        console.log("drop leave");
      },
      ondrop: function (event) {
        // event.relatedTarget.textContent = 'Dropped'
        console.log("dropped");
        console.log("dropped");
        console.log("dropped");
        
        var target_page_id  = $(event.target).closest(".page").attr("data-page-id");
        var target_el_id    = $(event.target).attr("data-id");

        var src_page_id     = $(mobile_pages_current_draggable_origin).closest(".page").attr("data-page-id");
        var src_el_id       = $(mobile_pages_current_draggable_origin).attr("data-id");

        var target_el = pages_page_container(target_page_id).element(target_el_id);
        var src_el = pages_page_container(src_page_id).element(src_el_id);

        var src_el_img    = src_el.get_image();
        var targer_el_img = target_el.get_image();

        if (src_el_img && targer_el_img) {
          $(event.target).removeClass("can-drop");
          target_el.img(src_el_img);
          
          $(mobile_pages_current_draggable_origin).removeClass("can-drop")
          src_el.img(targer_el_img);

          
        }

      },
      ondropdeactivate: function (event) {
        // remove active dropzone feedback
        // event.target.classList.remove('drop-active')
        // event.target.classList.remove('drop-target')
      }
    })

    interact.dynamicDrop(true);
  } else {

    // $(document).on('touchmove', '.el-container-parent.clone', function(ev) {
    
    if (false) {
      $(document).on('touchmove', '.el-container-parent.image-container', function(ev) {
      // $(document).on('touchmove', '.el-container-parent-clone', function(ev) {      
        ev.stopPropagation();
        ev.preventDefault();
        
        // if (false) {
        if (mobile_pages_current_draggable_origin) {
          var touch = ev.originalEvent.touches[0];
          
          // var el = $(document.elementFromPoint(touch.clientX, touch.clientY));
          var els = document.elementsFromPoint(touch.clientX, touch.clientY);
          // var els = [];
          
          $(".can-drop").removeClass("can-drop");

          for (var i=0; i<els.length; i++) {
            // var parent = $(els[i]).closest(".el-container-parent, .page-drop-area-container");
            var parent = $(els[i]).closest(".el-container-parent, .page-drop-area-container .left, .page-drop-area-container .right");
            if ( (parent.length > 0) && !( $(parent[0]).hasClass("clone") ) ) {
              
              $(parent[0]).addClass("can-drop");
              break;
              
            }
          }
        }
        return false;
      });
    }

  }
  /*************************************
  *            END Dropables
  **************************************/
  

  window.interact_initialized = true;  
}

function do_images_move_draggable(interact_event) {
  
  var touch = null;

  if (mobile_pages_current_draggable_origin) {
    
    // var touch = ev.originalEvent.touches[0];
    if (interact_event.client && interact_event.client.x && interact_event.client.y) {
      touch = {
        clientX: interact_event.client.x,
        clientY: interact_event.client.y,
      };
    }

    if (touch) {

      var els = document.elementsFromPoint(touch.clientX, touch.clientY);
      
      $(".can-drop").removeClass("can-drop");

      for (var i=0; i<els.length; i++) {
        
        // var parent = $(els[i]).closest(".el-container-parent, .page-drop-area-container");
        // var parent = $(els[i]).closest(".el-container-parent, .page-drop-area-container .left, .page-drop-area-container .right");
        var parent = $(els[i]).closest(".el-container-parent.image-container, .page-drop-area-container .left, .page-drop-area-container .right");

        if ( (parent.length > 0) && !( $(parent[0]).hasClass("clone") ) ) {
          
          $(parent[0]).addClass("can-drop");
          break;
          
        }
      }
    } else {
      console.warn("images move dragable with no touch value");
    }

  }

}

function previewer_height_in_px(opts) {
  var container = opts.container;
  var data = opts.data;
  var page_count = opts.page_count;

  var page_ratio = data.sizes.ratio;

  var el_width = $(container).width();
  var max_hight = $(container).closest(".pages-container-inner").height();

  if ( !page_count || (page_count > 1) ) {
    if ( ( data.product && (data.product.type == "Album") ) || (g_album.type == "Album") ) {
    
      if ( (opts.data.page_type == "album_page_first") || (opts.data.page_type == "album_page_last") ) {
        var height = Math.min( (max_hight - 80 - 32 - 8), ( (el_width / 2) / page_ratio) )
      } else {
        var height = Math.min( (max_hight - 80 - 32 - 8), (el_width / page_ratio) )
      }
    
    } else {
      var height = Math.min( (max_hight - 80 - 32 - 20), (el_width / page_ratio) )
      // var height = (max_hight - 80 - 32 - 8 - 50);
      // console.log("max_height=" + max_hight + " height=" + height);
    }
  } else {
    var height = Math.min( (max_hight - 80 - 32 - 8 - 30), (el_width / page_ratio) )
  } 

  return Math.floor(height);
}

function previewer_el_str( opts ) {

  var page_type = opts.page_type;
  var index = opts.index;
  var load_counter = (opts.load_counter ? opts.load_counter : 1);
  var page_id = (opts.page_id ? opts.page_id : null);

  var flat_album = ( (g_album.type != "Album") || (g_album.customer_product.product_group.render_as_pages == true) ) ? false : true

  var add_images_and_layout_btns_to_center = (
    // !( (g_album.type == "Album") && (page_type == "album_cover") ) ||
    ( (g_album.type == "Album") && flat_album && (page_type != "album_cover") ) ||
    ( page_type == "album_page_first") || ( page_type == "album_page_last") || 
    (g_album.type != "Album")
  );

  var page_str = "";

  page_str += "<div class='page-previewer " + ((page_type == "album_cover") ? "cover" : "sheet") + " " +
                (add_images_and_layout_btns_to_center ? "single" : "double") + " " + 
                ((g_album.type == "Album") ? "album" : "") +
                " page-previewer-" + index + "' " + 
                    "id='page-previewer-" + index + "' style='' " + "data-index='" + index + "' " + 
                    "data-load-counter='" + (load_counter) + "' " + 
              ">";

  // addvanced edit button
  if ( !is_mobile_device() && (g_album.type == "Album") ) {
    page_str +=    "<div class='page-actions-container-top'>";
    page_str +=        "<a href='#' class='btn advanced-edit-btn' style='' title=''>";
    page_str +=          "<span>Advance edit</span>";  
    page_str +=          "<i class='fal fa-pen'></i>";
    page_str +=        "</a>";
    page_str +=    "</div>";
  }


  page_str +=    ( "<div class='page empty'" + (page_id ? (" data-page-id='"  + page_id + "' ") : "") + "></div>" );
  
  page_str +=    "<div class='page-actions-container' style=''>"

  // Left side
  page_str +=      "<div class='btn-group page-actions left' style='height:100%; vertical-align:top; direction:ltr;'>";
  // if (page_type != "album_cover") {
  
  // if ( (page_type != "album_cover") && ( !flat_album || (g_album.type != "Album") ) ) {
  // if ( (page_type != "album_cover") && !flat_album ) {  
  // if ( (g_album.type == "Album") && (page_type != "album_cover") && !flat_album )  {    
  
  // debugger;
  if ( !add_images_and_layout_btns_to_center && (page_type != "album_cover") ) {
    page_str +=        "<a href='#' class='btn add-image-to-page-btn' style='' title=''>";
    page_str +=          "<svg xmlns='http://www.w3.org/2000/svg' aria-hidden='true' role='img' width='1em' height='1em' preserveAspectRatio='xMidYMid meet' viewBox='0 0 24 24'><g transform='translate(24 0) scale(-1 1)'><g stroke-width='1.5' fill='none'><path d='M13 21H3.6a.6.6 0 0 1-.6-.6V3.6a.6.6 0 0 1 .6-.6h16.8a.6.6 0 0 1 .6.6V13' stroke='currentColor' stroke-linecap='round' stroke-linejoin='round'/><path d='M3 16l7-3l5.5 2.5' stroke='currentColor' stroke-linecap='round' stroke-linejoin='round'/><path d='M16 10a2 2 0 1 1 0-4a2 2 0 0 1 0 4z' stroke='currentColor' stroke-linecap='round' stroke-linejoin='round'/><path d='M16 19h3m3 0h-3m0 0v-3m0 3v3' stroke='currentColor' stroke-linecap='round' stroke-linejoin='round'/></g></g></svg>"
    page_str +=        "</a>";
  }  

  // if ( (page_type != "album_cover") || flat_album ){
  // if ( ( (page_type != "album_cover") && !flat_album ) || ( (page_type == "album_cover") && flat_album ) ) {  
  // if ( (g_album.type == "Album") && ( (page_type == "album_cover") || !flat_album) ) {    
  if ( !add_images_and_layout_btns_to_center ) {
    page_str +=        ("<a href='#' class='btn auto-layout-btn " + ( (g_album.type != "Album") ? "both" : "" ) +  "' style='' title=''>");
    page_str +=          "<i class='fal fa-random' style=''></i>";
    page_str +=        "</a>";    
  }  
  page_str +=      "</div>";
  // END - Left side


  // if ( (g_album.type == "Album") || (g_album.type == "Calendar") ) {
  if ( true ) {  
    
    page_str +=      "<div class='btn-group page-actions page-actions-main-btns' style='height:100%; vertical-align:top;'>";

    if ( (page_type != "album_cover") && ( ( (g_album.type == "Album") && flat_album ) || ( (g_album.type != "Album") /*&& (g_album.type != "Calendar")*/ ) ) ) {
      page_str +=        "<a href='#' class='btn add-image-to-page-btn' style='' title=''>";
      page_str +=          "<svg xmlns='http://www.w3.org/2000/svg' aria-hidden='true' role='img' width='1em' height='1em' preserveAspectRatio='xMidYMid meet' viewBox='0 0 24 24'><g transform='translate(24 0) scale(-1 1)'><g stroke-width='1.5' fill='none'><path d='M13 21H3.6a.6.6 0 0 1-.6-.6V3.6a.6.6 0 0 1 .6-.6h16.8a.6.6 0 0 1 .6.6V13' stroke='currentColor' stroke-linecap='round' stroke-linejoin='round'/><path d='M3 16l7-3l5.5 2.5' stroke='currentColor' stroke-linecap='round' stroke-linejoin='round'/><path d='M16 10a2 2 0 1 1 0-4a2 2 0 0 1 0 4z' stroke='currentColor' stroke-linecap='round' stroke-linejoin='round'/><path d='M16 19h3m3 0h-3m0 0v-3m0 3v3' stroke='currentColor' stroke-linecap='round' stroke-linejoin='round'/></g></g></svg>"
      page_str +=        "</a>";
    }

    if ( ( (g_album.type == "Album") && flat_album && (page_type != "album_cover") ) || ( (g_album.type != "Album") /*&& (g_album.type != "Calendar")*/ ) )  {
      page_str +=        ("<a href='#' class='btn auto-layout-btn " + ( (g_album.type != "Album") ? "both" : "" ) +  "' style='' title=''>");
      page_str +=          "<i class='fal fa-random' style=''></i>";
      page_str +=        "</a>";      

    }

    // sort pages icon
    if ( (g_album.type == "Album") && (page_type != "album_cover") && (page_type != "album_page_first") && (page_type != "album_page_last") ) {
      page_str +=        "<a href='#' class='btn sort-pages-btn' style='' title=''>";
      page_str +=          "<i class='fal fa-arrows-alt-v'></i>";
      page_str +=        "</a>";
    }

    // plus page icon
    if ( (g_album.type == "Album") && (page_type != "album_cover") && (page_type != "album_page_last") ) {
      page_str +=        "<a href='#' class='btn add-page-btn' style='' title=''>";
      page_str +=          "<i class='fad fa-file-plus' style=''></i>";
      page_str +=        "</a>";
    }
    
    // minus page icon
    if ( (g_album.type == "Album") && (page_type != "album_cover") && (page_type != "album_page_first") && (page_type != "album_page_last") ) {
      page_str +=        "<a href='#' class='btn delete-page-btn' style='' title=''>";
      page_str +=          "<i class='fad fa-file-minus' style=''></i>";
      page_str +=        "</a>";
    }
    
    page_str +=      "</div>";
  }
  
  // Right side
  page_str +=      "<div class='btn-group page-actions right' style='height:100%; vertical-align:top; direction:rtl;'>";
  // if (g_album.type == "Album") {
    // page_str +=      "<div class='btn-group page-actions right' style='height:100%; vertical-align:top; direction:rtl;'>";
    // if (page_type != "album_cover") {
    
    // if ( (page_type != "album_cover") && !flat_album && (g_album.type == "Album") ) {  
    // if ( (g_album.type == "Album") && (page_type != "album_cover") || !flat_album ) {
    // if ( (g_album.type == "Album") && (page_type != "album_cover") && !flat_album )  {  
    if ( !add_images_and_layout_btns_to_center && (page_type != "album_cover") ) {  
      page_str +=        "<a href='#' class='btn add-image-to-page-btn' style='' title='delete page'>";      
      page_str +=          "<svg xmlns='http://www.w3.org/2000/svg' aria-hidden='true' role='img' width='1em' height='1em' preserveAspectRatio='xMidYMid meet' viewBox='0 0 24 24'><g transform='translate(24 0) scale(-1 1)'><g stroke-width='1.5' fill='none'><path d='M13 21H3.6a.6.6 0 0 1-.6-.6V3.6a.6.6 0 0 1 .6-.6h16.8a.6.6 0 0 1 .6.6V13' stroke='currentColor' stroke-linecap='round' stroke-linejoin='round'/><path d='M3 16l7-3l5.5 2.5' stroke='currentColor' stroke-linecap='round' stroke-linejoin='round'/><path d='M16 10a2 2 0 1 1 0-4a2 2 0 0 1 0 4z' stroke='currentColor' stroke-linecap='round' stroke-linejoin='round'/><path d='M16 19h3m3 0h-3m0 0v-3m0 3v3' stroke='currentColor' stroke-linecap='round' stroke-linejoin='round'/></g></g></svg>"
      page_str +=        "</a>";
    }
    // if ( (page_type != "album_cover") || flat_album ) {
    // if ( ( (page_type != "album_cover") && !flat_album && (g_album.type == "Album") ) || ( (page_type == "album_cover") && flat_album ) ) {    
    // if ( (g_album.type == "Album") && ( (page_type == "album_cover") || !flat_album) ) {  
    if ( !add_images_and_layout_btns_to_center ) {  
      page_str +=        "<a href='#' class='btn auto-layout-btn' style='' title=''>";
      page_str +=          "<i class='fal fa-random' style=''></i>";
      page_str +=        "</a>";
    }  
    // page_str +=      "</div>";
  // }
  page_str +=      "</div>";
  // END - Right side
  

  page_str +=      "<div class='page-counter-container'>"

  page_str +=      "</div>";

  page_str +=      "<div class='page-drop-area-container' style='direction:ltr;'>";
  // page_str  +=        "<span class=''>+ הוסף לדף</span>";
  page_str +=        "<div class='left'></div>"
  page_str +=        "<div class='right'></div>"
  page_str +=      "</div>"; // page-drop-area-container - END

  page_str +=    "</div>"; // page-actions-container - END

  page_str += "</div>"; // page-previewer - END


  return page_str;
}

function handle_actions_header_click(el) {
  var header_el = $(el).closest('.pages-container-actions');
  if (header_el.hasClass("open")) {
    header_el.removeClass("open");
  } else {
    header_el.addClass("open");
    activate_bottom_panel_btn();
  }
}

function mobile_set_mask_as_inner(page_el) {
  
  var sheet_width = $(page_el).outerWidth(); // with crop area
  var sheet_height = $(page_el).outerHeight(); // with crop area
  var sheet_inner_width = $(page_el).find(".page_inner").outerWidth(); // without crop area 
  var sheet_inner_height = $(page_el).find(".page_inner").outerHeight(); // without crop area

  // var width_inner_percentages = (sheet_inner_width / sheet_width)*100;
  // var height_inner_percentages = (sheet_inner_height / sheet_height)*100;

  var width_diff =  (sheet_width - sheet_inner_width);
  var height_diff = (sheet_height - sheet_inner_height);

  // name shortcuts
  // var wip = width_inner_percentages;
  // var hip = height_inner_percentages;
  var wd = width_diff;
  var hd = height_diff;

  $(page_el).css( { 
    "clip-path": ( "inset(" + ( (hd - 3)/2 + "px " + (wd - 3)/2 + "px " + (hd - 3)/2 + "px " + (wd - 3)/2 + "px " ) + ")" ),
    // - 3 => this is to force at list 1 px smaller clip-path so we can see the "page border"
  } );
}
function mobile_inner_mask_observer_set(el) {
  g_mutation_observer.observe(el, g_mutation_observer_config);
}

function mobile_load_all_pages(product_id, container_el, cb, cb_page_loaded) {
  var path = "/products/" + product_id + "/pages";
  var url = g_editor_url + path;

  // TODO - we allready have it ... (we changed as_json used from product#edit)
  // TODO - no need for this call.

  // load album pages
  $.ajax({
    type:   "GET",
    async:  true,
    // url: path,
    url:    url,
    cache:  false,
    success: function(data) {

      var pages_str = `<div class='pages-container visible full' style='overflow:hidden;'>\
                        <div id='pages-container-inner' class='pages-container-inner hidden-scrollbar'>\
                          <div id='pages-container-sheets' 
                               class='pages-group pages-container-sheets ${g_album.type.toLowerCase()} language-dir language-txt-align' 
                               style='/*overscroll-behavior: none; touch-action: none;*/
                          '>`;

      pages_str += "<style> \
                      .page-drop-area-container .left::before {\
                        content:" + '"' + I18n.t("mobile.editor.add_to_page_html") + '";' + "\
                      }\
                      .page-drop-area-container .right::before {\
                        content:" + '"' + I18n.t("mobile.editor.add_to_page_html") + '";' + "\
                      }\
                      .page-drop-area-container .left::after {\
                        content:" + '"' + I18n.t("mobile.editor.remove_from_page_html") + '";' + "\
                      }\
                      .page-drop-area-container .right::after {\
                        content:" + '"' + I18n.t("mobile.editor.remove_from_page_html") + '";' + "\
                      }\
                    </style>";

      if ( g_album.type != "PhotoPrint" ) {
        for (var i=0; i<data.length; i++) {

          var page_str = previewer_el_str( { 
            page_type: data[i].page_type, 
            index: i,
            load_counter: 1,
            page_id: data[i].id,
          } );
          pages_str += page_str;

        }
      }
      
      pages_str +=        "</div>";
      pages_str +=      "</div>";

      if (g_album.type == "Calendar") {
        pages_str += "<div class='pages-container-configuration' style=''>";
        pages_str +=   "<i class='fad fa-cog horizontal-vertical-center-absolute' style=''></i>";
        pages_str += "</div>";
      }
      
      pages_str +=      "<div class='pages-container-actions'>";
      pages_str +=        "<div class='pages-container-actions-header'>";
      pages_str +=          "<div class='pages-container-actions-header-open-close-btn' onclick='handle_actions_header_click(this); return false;'   >";
      pages_str +=            "<i></i>"
      pages_str +=          "</div>";      
      pages_str +=        "</div>";
      pages_str +=        "<div class='pages-container-actions-panels-header'>";
      pages_str +=          "<div class='pages-container-actions-panels-header-inner'>";
      pages_str +=            "<div class='panel-btn' data-container-name='photos'>";
      pages_str +=              I18n.t("mobile.editor.photos");
      pages_str +=            "</div>";
      pages_str +=            "<div class='panel-btn' data-container-name='templates'>";
      pages_str +=              I18n.t("mobile.editor.templates");
      pages_str +=            "</div>";
      pages_str +=            "<div class='panel-btn' data-container-name='backgrounds'>";
      pages_str +=              I18n.t("mobile.editor.backgrounds");
      pages_str +=            "</div>";         
      pages_str +=          "</div>";
      pages_str +=        "</div>";
      pages_str +=        "<div class='pages-container-actions-panels'>";
      pages_str +=          "<div class='pages-container-actions-panels-panel' data-container-name='photos' style=''>";
      pages_str +=          "</div>";
      pages_str +=          "<div class='pages-container-actions-panels-panel' data-container-name='templates' style=''>";
      pages_str +=          "</div>";
      pages_str +=          "<div class='pages-container-actions-panels-panel' data-container-name='backgrounds' style='/*background:green;*/'>";
      pages_str +=          "</div>";      
      pages_str +=        "</div>";            
      pages_str +=      "</div>";
      
      pages_str +=    "</div>";

      $(container_el).html(pages_str);

      if ( g_album.type != "PhotoPrint" ) {
        for (var i=0; i<data.length; i++) {
          var el_container = $(container_el).find("#" + "page-previewer-" + i)[0];
          var height = previewer_height_in_px( { container: el_container, data: data[i], page_count: data.length,})
          $(el_container).css( { "height": (height + "px") } );
        }

        for (var i=0; i<data.length; i++) {
          var page_id = data[i].id;
          
          // ready_counter[i] = 0;
          // g_ready_timeout[i] = null;
          ready_counter[page_id] = 0;
          g_ready_timeout[page_id] = null;

          g_load_all_with_index_counter[i] = 0;
        }

        // load_all_pages_async(data, 0, "#page_preview");
        for (var i=0; i<data.length; i++) {

          var previewer_container_selector = ("#" + "page-previewer-" + i);
          var page_container_selector = previewer_container_selector + " .page";

          window[("g_pages_album_" + i)] = new AlbumPages( {  
                                                  // elements_container_selector: ".page-previewer .page",
                                                  elements_container_selector: page_container_selector,
                                                  name: ("g_pages_album_" + i),

                                                  is_editor: false,
                                                  // use_crop_cb: true,

                                                  init_data: data[i],
                                               } );

          g_all_album_pages[ data[i].id ] = window[("g_pages_album_" + i)];
          
          if ( !('IntersectionObserver' in window) ) {
            load_all_pages_async( [ data[i] ], 0, "#page_preview", false, function() {
              return {
                selector: this.selector,
                container_str: this.container_str,
              };
            }.bind( { selector: $(".page-previewer-" + i)[0], container_str: ("g_pages_album_" + i) } ) );
          }

        }

        setTimeout( function() {
          load_interact();

          if ( !('IntersectionObserver' in window) ) {
            for (var i=0; i<data.length; i++) {
              var page_el = $(".pages-container .page-previewer-" + i + " .page")

              page_el.closest(".page-previewer").find(".page-actions-container").css( {
                "width"   : page_el.find(".page_inner").width(),
                // "width"   : (g_album.type == "Album") ? page_el.find(".page_inner").width() : page_el.width(),
                "visibility": "visible",
              });
              page_el.closest(".page-previewer").find(".page-actions-container").addClass("visible");

            }
          } else {
            var options = {
              root: $("#pages-container-inner")[0],
              // root: $(".pages-container")[0],
              threshold: [0],
            }
            var observer = new IntersectionObserver(function(observerd) {
              var observerd_length = observerd.length;

              for (var i=0; i<observerd_length; i++) {
                if (observerd[i].isIntersecting == true) {

                  if (  !($(observerd[i].target).find(".page").length > 0) || 
                        $(observerd[i].target).find(".page").hasClass("empty")  
                     ) 
                  {
                    // load page
                    var page_id = $(observerd[i].target).attr("data-index");
                    var page =  window[("g_pages_album_" + page_id)];
                    var data = page.init_data;

                    var selector = $(".page-previewer-" + page_id)[0];

                    load_all_pages_async( [ data ], 0, "#page_preview", false, 
                      function() {
                        return {
                          selector: this.selector,
                          container_str: this.container_str,
                        };
                      }.bind( { selector: selector, container_str: ("g_pages_album_" + page_id) } ), 
                      {
                        mobile_all_page_elements_loaded: function(page_id) {                    
                          mobile_update_page(page_id);
                          if (this.cb_page_loaded) { this.cb_page_loaded(page_id); }
                        }.bind( { cb_page_loaded: this.cb_page_loaded } )
                      }
                    );

                    var page_el = $(selector).find(".page");

                    mobile_inner_mask_observer_set( selector );

                    page_el.closest(".page-previewer").find(".page-actions-container").css( {
                      "width"   : page_el.find(".page_inner").width(),
                      // "width"   : (g_album.type == "Album") ? page_el.find(".page_inner").width() : page_el.width(),
                      "visibility": "visible",
                    });
                    page_el.closest(".page-previewer").find(".page-actions-container").addClass("visible");

                    setTimeout( function() {
                      // sign it as allready loaded once
                      $(this.selector).attr("data-load-counter", 2);
                    }.bind( { selector: selector } ), 3*1000);
                  
                  }
                }
              }

            }.bind(this), options);

            for (var i=0; i<this.data.length; i++) {
              observer.observe( $("#pages-container-inner .page-previewer-" + i)[0] );
            }
          
          }
          
          mobile_start_sortable();

          if (this.cb) { // cb for perliminary load
            this.cb(true);
          }

        // }.bind( { data:data } ), 3*1000); // TBD do it imidiatly on all elemnts loaded
        // }.bind( { data:data } ), 1*1000); // TBD do it imidiatly on all elemnts loaded
        }.bind( { data:data, cb:cb, cb_page_loaded: cb_page_loaded } ), 1*200); // TBD do it imidiatly on all elemnts loaded
      } else {
        photo_print_load_all_photos( "#pages-container-sheets", data );
        cb(true);
      }
    },
    error: function (jqXHR, ajaxOptions, thrownError) {
      if (cb) {
        cb(false);
      }
    }
  }); 

  // var html = handlebars_compile_and_execute("mobile-header", {
  
  //   left_icon:    "<i class='fal fa-chevron-left'></i>",
  //   left_class:   "mobile-back-btn",

  //   title: " "
  // });
  // $(".main-container-header").html(html);

}
