$( document ).on("click", ".template_select_container .template", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();
	
	$(".template_select_container .template.selected").removeClass("selected");	
	$(this).addClass("selected");
});

$( document ).on("click", ".select-template-btn", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	if ( $(".template_select_container .template.selected").length == 0 ) {
		alert("please select a template first");
	} else {
		var selected_id = $(".template_select_container .template.selected").attr("data-product-id");
	
		var path = "/customers/" + g_customer_id + "/tokenized_users/" + g_tokenized_user_id + "/products/" + g_product_id + ".json";						
		var url = g_editor_url + path;

		$.ajax({
	    url: url,
	    method: "PUT",
	    async: true,
	    cache: false,
	    contentType: 'application/json',

		  data: JSON.stringify( {
		  	origin_template_id: selected_id,
		  	// authenticity_token: window._token
		  }),

		  success: function(data) {
		  	if ( (g_album.type == "Calendar") && (!g_album.from_month) ) {
		  		do_load_page("calendar-configutation", { product_id: g_album.id }, I18n.t("mobile.calendar.calendar_configuration_page_title"), { add_to_history:false, persistent:false, replace:this.replace } ); // current page is create album/product => no need to keep it in history !!!				
		  	} else {
			  	// start_editor(g_product_id, null, { replace: true });
			  	start_editor(g_product_id, g_user_item_id, { replace: true });
			  }
			},
			error: function() {
				console.error("failed updateing page !!!"); 
				console.error("failed updateing page !!!");
				console.error("failed updateing page !!!"); 
			}

		}).done(function( msg ) {
			
	  });


	}

});

function templates_load_page( container_el, opts ) {
	
	var html = handlebars_compile_and_execute("mobile-header", {
		left_icon: 		"<i class='fal fa-chevron-left'></i>",
		left_class: 	"mobile-back-btn",

		title: (opts.title || "תמונות להוספה")
	});
	$(".main-container-header").html(html);

	var path = "/customers/" + g_customer_id + "/tokenized_users/" + g_tokenized_user_id + "/products/" + g_product_id + "/product_templates.json";						
	var url = g_editor_url + path;
	$.ajax( {
	  type: "get",
	  async: true,
	  url: url,
	  cache:    false,
	  success: function(data) {						
	  	var html = handlebars_compile_and_execute("mobile-templates", data);
	  	$(container_el).html(html);
		},
	});

	var html = handlebars_compile_and_execute("mobile-footer", {
		title_btn: "בחר תבנית",
		title_btn_class: "select-template-btn regular-plus"
	});
	$(".main-container-footer").html(html);	
	$(".main-container-footer").css( { "display":"block" } );		

}