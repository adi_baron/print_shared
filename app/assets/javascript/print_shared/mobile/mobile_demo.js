// var g_customer_products_data;


$( document ).on(
	( (value_if_defined('g_is_gallery_shop') == true) ? 'turbolinks:load' : 'ready'), // gallery shop use turbolink
	function(ev) {	

	mobile_editor_ready();

	var current_url = new URL( window.location.toString() );
	var product_id = current_url.searchParams.get("p_id") || g_product_id;

	var last_product_id = getCookie("last_p_id");
	
	if ( !value_if_defined("g_user_item_id") || ( value_if_defined("g_user_item_id") && (value_if_defined("g_user_item_id").length == 0)  ) ) {  
		g_user_item_id = getCookie("last_ui_id");
	}

	if (product_id) {
		// support from starting editor without root page loaded first => for debugging / developing
		// start_editor( product_id, undefined, { replace: false, } );
		start_editor( product_id, g_user_item_id, { replace: false, } );
	
	} else if ( last_product_id && (last_product_id.length > 0) ) {
		// cookie stored for last product
		start_editor( last_product_id, g_user_item_id, { 
			replace: false, 
			cb: function(res) {
				if (res == false) {
					console.warn("Failed starging editor for last product " + last_product_id + " - loading root");
					load_root();
				} else {
					// $(".mobile-modal").addClass("hidden");
				}
			},
		} );
	
	} else {
		// just load root ...
		load_root();	
	}

	if (true) {
		console.error("does not load products");
	} else {

		var path = "/customers/" + g_customer_id + "/tokenized_users/" + g_tokenized_user_id + "/products/";
		$.ajax({
		  type: 	"get",
		  async: 	true,
		  url: 		path,
		  cache:  false,
		  data: {
		  	include_media_count: true,
		  },
		  success: function(data) {
		  	for (var i=0; i<data.products.length; i++) {
		  		var html = "\
		  			<p class='product-link'>\
		  				<a href='' data-id='" + data.products[i].id + "'>\
		  					" + data.products[i].id + "<br><b>" + data.products[i].media_count + " photos - " + ( (data.products[i].album_ready == true) ? "ready -" : "" ) + " click & edit</b>\
		  				</a>\
		  			</p>\
		  		"
		  		$(".main-container-side-menu-content").append(html);
		  	}
		  },
			error: function (jqXHR, ajaxOptions, thrownError) {
				alert("error loading app");
			}
		});

	}

});

// $( document ).on('click', ".main-container-side-menu .mobile-link", function(ev) {
$( document ).on('click', ".mobile-link", function(ev) {	
	ev.preventDefault();
	ev.stopPropagation();

	var url = $(this).attr("data-url");
	var template = $(this).attr("data-template");
	var title = $(this).attr("data-title");

	$(".main-container-side-menu").removeClass("open");

	if (template) {
		if (template == "html") {
			do_load_page( "page-url", { url:url, }, title, { persistent: false, add_to_history: false } );
		} else {
			do_load_page( "page-url", { url:url, template:template, }, title, { persistent: false, add_to_history: false } );
		}
	} else {
		
		if (url) {
			load_url(url, function(res, data) {
				console.log("mobile link executed");
			});
		}
	
	}

});


$( document ).on('click', ".product-link a.product-link-data-actions-edit", function(ev) {

	var user_item_id = $(this).attr("data-user-item-id");

	if ( value_if_defined("g_is_gallery_shop") == true) {

		var url = `/user_items/${user_item_id}/editor?gallery=${g_gallery_id}&store=true`;
		Turbolinks.visit( url );

	} else {
		ev.preventDefault();
		ev.stopPropagation();

		mobile_close_menu();

		mobile_ui_spinner_modal();

		var product_id = $(this).attr("data-id"); // || g_product_id;
		// var user_item_id = $(this).attr("data-user-item-id"); // || g_user_item_id;
		start_editor(product_id, user_item_id, { replace: false, } );
	}
});

$( document ).on('click', ".product-link a.product-link-data-actions-delete", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	mobile_ui_yes_no_modal( {
		modal: true,
		title: I18n.t("mobile.yes_no.delete_project_title"),
		txt: I18n.t("mobile.yes_no.delete_project_txt"),

		sign: "fal fa-info",

		yes_txt: I18n.t("delete"),
		no_txt: I18n.t("cancel"),

		// btn_style: "block",

		yes_cb: function() {
			
			var user_item_id = $(this._this).attr('data-user-item-id');
			var path = "/user_items/" + user_item_id + "?mobile=true";

		  $.ajax({
		    type:     "delete",
		    async:    true,
			  url:      path,
		    cache:    false,
		    success:  function(data) {	
		    	do_load_page( "page-url", { url:"/user_items.json", template:"mobile-projects-template"  }, I18n.t("dashboard.active_projects"), { persistent: false, add_to_history: false } );
		    },
		    error: function (jqXHR, ajaxOptions, thrownError) {
		    	console.error('error deleting address');
		    },
		  });

		}.bind( { _this: this } ),		

	});	

});

function mobile_open_menu() {
	$('.main-container-side-menu').addClass("open");
}
function mobile_close_menu() {
	$('.main-container-side-menu').removeClass("open");
}
$( document ).on('click', ".store-menu", function() {
	mobile_open_menu();
});
$( document ).on('click', ".close-menu-icon", function() {
	mobile_close_menu();
});



$( document ).on('click', ".product-create-btn, .produc-block-create", function() {
	if (g_root_products_style == 'blocks') {
		
		var product_id = $(this).attr("data-product-id");	
		do_load_page( "create_product_from_blocks", { data: { product_id: product_id } }, "", { persistent: false } ); 
	
	} else if (g_root_products_style == 'cards') {
		
		var product_group_id = $(this).closest(".tab-content").attr("id");
		for (var i=0; i<g_mobile_products.length; i++) {	
			if (g_mobile_products[i].id == product_group_id) {	
				var product_group = g_mobile_products[i];
				break;
			}
		}
		do_load_page( "create_product", { data: product_group }, "", { persistent: false } ); 
	}
	
});

$( document ).on('submit', ".mobile-product-create form", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	// remove focus from element
	$(ev.target).find('input:focus').blur();

	return false;
});

$( document ).on('click', ".product-create-continue-btn", function() {
	
	// Set default value to name (same as placeholder) - if no name / value set to input
	if ( $(".mobile-product-create input[name='product-name']").val() == "" ) {
		var default_name = $(".mobile-product-create input[name='product-name']").attr("data-default-name");
		$(".mobile-product-create input[name='product-name']").val(default_name);
	}

	$(".product-name-container").addClass("hidden");
	$(".product-selector-container").removeClass("hidden")
});

$( document ).on('change', ".product-selector-product input[type=radio][name='product-id']", function() {
	$(".product-selector-product label").removeClass("checked");
	$(".product-selector-product input[name='product-id']:checked").closest("label").addClass("checked");
});

$( document ).on('click', ".product-create-create-btn", function() {
	console.log('click product-create-create-btn');

	var product_name = $(".mobile-product-create input[name='product-name']").val();
	var product_id   = $(".mobile-product-create input[name='product-id']:checked").val();
	var product_data = $(".mobile-product-create input[name='product-id']:checked").attr("data-data");

	if (product_name && (product_name.length > 0) && product_id) {

		$(".mobile-modal").removeClass("hidden");

		// var path = "/customers/" + g_customer_id + "/tokenized_users/" + g_tokenized_user_id + "/products/";
		var path = g_create_product_url;
		var params = create_product_product_params( { "name": product_name, "product_id": product_id, "product_data": product_data } );

	  $.ajax({
		  type: "POST",
		  async: true,
		  url: path,
		  cache: false,
		  data: 
		  	params,
		  	// {	
			  // 	customer_product_id: product_id,
			  // 	product: {
					// 	edit_cover: 					"no_cover",
					// 	opening_single_page: 	false,
					// 	closing_single_page:  false,
					// 	direction:            "rtl",
			  // 	}
		  	// },
		  success: function(data) {
		  	mobile_product_created(data);
		  },
  		error: function (jqXHR, ajaxOptions, thrownError) {
				alert( I18n.t("mobile.new_product.err_creating_product") );
				$(".mobile-modal").addClass("hidden");
			}
		});

	} else {
		alert( I18n.t("mobile.new_product.error_product_needed") );
	}

});

$( document ).on('click', ".how-to-container .how-to-gif-trigger", function() {
	var gif_src = $(this).closest(".how-to-container").attr('data-gif-src');
	$(this).closest(".how-to-container").find("img").attr("src", gif_src);
	$(this).css( { "display": "none", } );
});


function mobile_product_created(data) {

	// this happens (resume string) when using returned with turbo link
	if ( !(typeof data === 'string') ) {

		if (data.user.editor_tokenized_user_id) {
			g_tokenized_user_id = data.user.editor_tokenized_user_id;
		}


		var product_id 		= data.id;
		var user_item_id 	= data.user_item_id;
		start_editor(product_id, user_item_id, { replace: true, } );

	}
}

function load_url(url, cb) {
  $.ajax({
	  type: 		"get",
	  async: 		true,
	  url: 			url,
	  cache:    false,
	  success: 	function(data) {
	  	if (cb) {
	  		cb(true, data);
	  	} 
	  },
	  error: function (jqXHR, ajaxOptions, thrownError) {
  		cb(false);
	  },
	});

}

function mobile_create_product( data, container_el) {

	var html = handlebars_compile_and_execute("mobile-product-create", {
		data: data,		
	});
	$(container_el).html(html);

	var html = handlebars_compile_and_execute("mobile-header", {
		left_icon: 		('<i class="fal fa-chevron-left" data-after="' + I18n.t('mobile.back') + '"></i>'),
		left_class: 	"mobile-back-btn",
		
		title: I18n.t("mobile.new_product.title"),
	});
	$(".main-container-header").html(html);
	
	$(".main-container-footer").css( { "display":"none" } );
}

function mobile_create_product_from_blocks( data, container_el) {
	
	// var html = handlebars_compile_and_execute("mobile-product-create", {
	// 	data: data,		
	// });
	// $(container_el).html(html);

	 $.ajax({
	  type: "get",
	  async: true,
	  url: ("/products/" + data.product_id + "?mobile=true"),
	  cache:    false,
	  success: function(data) {
	  	$(container_el).html(data);
	  },
		error: function (jqXHR, ajaxOptions, thrownError) {
			alert("failed loading product: " + data.product_id);
		}
	});

	var html = handlebars_compile_and_execute("mobile-header", {
		left_icon: 		('<i class="fal fa-chevron-left" data-after="' + I18n.t('mobile.back') + '"></i>'),
		left_class: 	"mobile-back-btn",
		
		title: I18n.t("mobile.new_product.title"),
	});
	$(".main-container-header").html(html);
	
	$(".main-container-footer").css( { "display":"none" } );
}

function load_root() {
	// console.error("load_root force return");
	// return;

	// var path = "/customers/" + g_customer_id + "/product_groups/";

 //  $.ajax({
	//   type: "get",
	//   async: true,
	//   url: path,
	//   cache:    false,
	//   success: function(data) {
	//   	g_customer_products_data = data;

	// 		setTimeout( function() {	  	
	// 			do_load_page( "root", { data: this.data }, "", { persistent: false } ); 
	//   		$(".mobile-modal").addClass("hidden");
	//   	}.bind( { data: data } ), 350 );

	//   },
	// 	error: function (jqXHR, ajaxOptions, thrownError) {
	// 		alert("error loading app");
	// 	}
	// });


	// if ( (value_if_defined("g_is_mobile") == true) && ( !value_if_defined("g_is_gallery_shop") ) ) {
	if ( value_if_defined("g_is_mobile") == true ) {

		if ( value_if_defined("g_initialize_with_cart") == true ) {
			do_load_page( "page-url", { url:"/cart?layout=false&mobile=true" }, I18n.t("mobile.cart.title"), { persistent: false, add_to_history: false } );
		} else if ( value_if_defined("g_initialize_created_projects") == true ) {
			do_load_page( "page-url", { url:"/user_items.json", template:"mobile-projects-template"  }, I18n.t("dashboard.active_projects"), { persistent: false, add_to_history: false } );
		} else {
			if ( !value_if_defined("g_is_gallery_shop") ) {
				do_load_page( "root", { data: g_mobile_products }, "", { persistent: false, add_to_history: true, } ); 	
			}
		}

		$(".mobile-modal").addClass("hidden");

	}

}

function mobile_root( data, container_el ) {

	if (g_root_products_style === 'cards') {
		var html = handlebars_compile_and_execute("mobile-root-cards", {
			data: data,		
		});
	} else if (g_root_products_style === 'blocks') {
		var html = handlebars_compile_and_execute("mobile-root-blocks", {
			data: data,		
		});
	} else {
		console.error('unknow root products style');
	}

	$(container_el).html(html);

	$($(container_el).find(".btn-tab-link")[0]).trigger("click");

	var html = handlebars_compile_and_execute("mobile-header", {
		// left_icon: 		"LOGO",
		left_icon: 		("<img src='" + g_logo_url + "' style='' />"),
		left_class: 	"logo",

		title: " ",

		right_icon: 		"<i class='fal fa-bars'></i>",
		right_class: 		"store-menu",
	});
	$(".main-container-header").html(html);

	var html = handlebars_compile_and_execute("mobile-footer", {
		// left_icon: "<i class='fal fa-shopping-cart'></i>",
		// left_class: "mobile-cart-btn /*horizontal-vertical-center-absolute*/",
		left_icons: ["<i class='fal fa-shopping-cart'></i>","<i class='fal fa-briefcase'></i>"],
		left_classes: ["mobile-cart-btn", "mobile-projects-btn"],

		// title_btn: "עצבו לי את הספר >",
		// title_btn_class: "design_book"
		title: " ",


		// right_icon: "<i class='fal fa-user'></i>",
		// right_class: "mobile-profile-btn /*horizontal-vertical-center-absolute*/",
		// right_icons: ["<i class='fal fa-user'></i>","<i class='fal fa-briefcase'></i>"],
		right_icons: ["<i class='fal fa-user'></i>"],
		right_classes: ["mobile-profile-btn", "mobile-projects-btn"],

	});
	$(".main-container-footer").html(html);
	$(".main-container-footer").css( { "display":"flex" } );

	mobile_update_cart_and_projects_counters();

}

function mobile_update_cart_and_projects_counters() {
	// update cart & projects icons
	$.ajax({
	  type: 	"get",
	  async: 	true,
	  url: 		"/cart.json?include_projects=true",
	  cache:  false,
	  success: function(data) {
  		$(".mobile-cart-btn i").attr("data-after", (data.user_items_count > 0) ? data.user_items_count : 0 );
	  	$(".mobile-projects-btn i").attr("data-after", (data.non_cart_user_items_count > 0) ? data.non_cart_user_items_count : 0 );
	  },
	});	
}



function demo_show_tab(ev, tab_name) {
	ev.preventDefault();
	ev.stopPropagation();

	var link = ev.target;

	$(link).closest(".tabs").find(".btn-tab-link").removeClass("active");
	$(link).addClass("active")

	$(link).closest(".tabs-container").find(".tab-content").removeClass("active");
	$(link).closest(".tabs-container").find(("#" + tab_name)).addClass("active");

}


