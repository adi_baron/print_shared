
//= require print_shared/mobile/inobounce

// no bootstrap with mobile solution 

//XXX= require handlebars/handlebars-v4.0.11.min.js

//XXX= require cropperjs-master_new/dist/cropper.js
//XXX= require print_shared/cropperjs-1.5.1/dist/cropper.js

//XXX= require print_shared/crop

//XXX= require uuid-random-master/uuid-random.min.js

//XXX= require handlebars_helpers

//XXX= require page
//XXX= require page_general
//XXX= require general_helpers

//XXX= require sortable_pages

//XXX= require product_data

//XXX= require album_elements/album_pages

//XXX= require album_elements/base_el
//XXX= require album_elements/image_el
//XXX= require album_elements/text_el
//XXX= require album_elements/sticker_el
//XXX= require album_elements/background_el
//XXX= require album_elements/shape_el
//XXX= require album_elements/container_el
//XXX= require album_elements/custom_el
//XXX= require album_elements/calendar_el

//XXX= require auto_templates

//XXX= require uploader/resizer_libjpeg
//XXX= require uploader/resizer_canvas
//XXX= require uploader/uploader_libjpeg
//XXX= require exif/ExifRestorer
//XXXXXX= require exif-js/exif
//XXX= require exifreader/exif-reader.js
//XXX= require uploader/shop_uploader
//XXXXXX= require sortable/sortable.min.js
//XXX= require sortable/sortable.js
//XXXXXX= require html5sortable-new/DragDropTouch.js
//XXXXXX= require html5sortable-new/html5sortable.js

//XXX= require web-font-loader/webfontloader.js
//XXX= require fonts

// ***************************
//       I18n javascript     *
// ***************************
// This is optional (in case you have `I18n is not defined` error)
// If you want to put this line, you must put it BEFORE `i18n/translations`
//XXX= require i18n
// Some people even need to add the extension to make it work, see https://github.com/fnando/i18n-js/issues/283
//XXX= require i18n.js
//
// This is a must
//XXX= require i18n/translations
// ***************************
//    END - I18n javascript  *
// ***************************

//= require print_shared/mobile/mobile_images
//= require print_shared/mobile/mobile_templates
//= require print_shared/mobile/mobile_calendar
//= require print_shared/mobile/mobile_image-edit
//= require print_shared/mobile/mobile_text_edit
//= require print_shared/mobile/mobile_pages
//= require print_shared/mobile/mobile_pages_actions
//= require print_shared/mobile/mobile_cover_page
//= require print_shared/mobile/mobile_page_update
//= require print_shared/mobile/mobile_cover_selector

//= require print_shared/mobile/history_management
//= require print_shared/mobile/mobile_handlebars_helpers
//= require print_shared/mobile/mobile_sortable_pages

//= require print_shared/mobile/mobile_address

//= require print_shared/mobile/mobile_projects_page

//= require print_shared/mobile/mobile_demo

//= require print_shared/simulations/canvas

//= require print_shared/social/social_base
//= require print_shared/social/social_google_photos
//= require print_shared/social/social_general

//= require print_shared/general/caching
//= require print_shared/general/utilities
//= require print_shared/general/ui_utilities

console.error("inobounce patched issues with horizontal scrolling - google iNoBounce horizontal scrolling");

// $(document).ajaxSuccess(function(ev, jqXHR, ajaxOptions, data) {
//   debugger;
// });
// $(document).ajaxSuccess(function() {
//   debugger;
// });

// debugger;
// $(document).on("ajax:success", ".edit_cart", function(ev, jqXHR, ajaxOptions, data) {
// $(document).on("ajax:success", "[data-remote='true']", function(ev, jqXHR, ajaxOptions, data) {	
// $(document).on("ajax:success", "*", function(ev, jqXHR, ajaxOptions, data) {		
// // $('body').on("ajax:success", "[data-remote='true']", function(ev, jqXHR, ajaxOptions, data) {
//   debugger;
// });
// document.addEventListener("ajax:success", (event) => {
//   // const [data, status, xhr] = event.detail;
//   debugger;
// });
$(document).ajaxSuccess(
  function(event, xhr, settings){ 
    if (xhr.responseJSON && xhr.responseJSON.redirect) {
    	do_load_page( "page-url", { url: (xhr.responseJSON.redirect) }, "", { persistent: false, add_to_history: false } );
    	// do_load_page( "page-url", { url: (xhr.responseJSON.redirect) }, "", { persistent: false, add_to_history: true } );
    }
  }
);


window.addEventListener("orientationchange", function(event) {
 	// CRASH!!! console.log("the orientation of the device is now " + event.target.screen.orientation.angle);
 	console.log("orientationchange");

 	if ( !is_fullscreen() ) {
 		console.log("running - not fullscreen mode");
	  
 		var delay = 200
	  for (var i=0; i<4; i++) {
	  	// 4 delays => 200, 400, 800, 1600 msec
	
	  	delay = delay*2

		  setTimeout(function() {
		  	window.scrollTo(40,0);
		  	console.log("scrolling");
		  }, delay);

		}

	}

});

$( document ).on('click', ".mobile-info-page", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	var html = handlebars_compile_and_execute("mobile-info-page");
	$(".mobile-modal").html(html);
	$(".mobile-modal").removeClass("hidden");
});

$( document ).on('click', ".mobile-preview", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	if ( is_mobile_device() && (g_album.type == "Album") ) {	
	  var html = handlebars_compile_and_execute("mobile-preview-page", {});
	  $(".mobile-modal").html(html);
	  $(".mobile-modal").removeClass("hidden");
	} else {
		mobiie_load_preview(g_product_id, true)
	}
});

$( document ).on('click', ".advanced-edit-btn", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	var page_id = btn_page_id(this);
	
	// alert( "advanced edit for page: " + page_id );

	mobiie_load_advanced_editor(g_customer_id, g_tokenized_user_id, g_product_id, page_id);
});

// $( document ).on('click', ".product-edit", function(ev) {
// 	ev.preventDefault();
// 	ev.stopPropagation();

// 	debugger;
// });

$( document ).on('click', ".product-edit", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	// mobile_ui_spinner_modal();

	mobile_save_all_changed(false);
	// mobile_stop_timed_save();

	var url = `/user_items/${g_user_item_id}/edit?mobile=true&form_only=true&store=${g_is_gallery_shop ? "true" : "false"}&gallery=${g_gallery_id}`;

	// do_load_page( "page-url", { url:url }, "edit product", { persistent:false, add_to_history:false, sidebar:true, } );
	do_load_page( "page-url", { url:url }, "edit product", { persistent:false, add_to_history:true, sidebar:true, } );
	// do_load_page( "page-edit", { url:url }, "edit product", { persistent:false, add_to_history:true, sidebar:true, } );
	// do_load_page( "page-url", { url:url }, "edit product", { persistent:false, add_to_history:false, history:true, sidebar:true, } );

});

$( document ).on('click', ".mobile-add-to-cart-btn, .product-add-to-cart", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	if (history.state.page == "page-url") {
		// if we are in product edit inside gallery store (side poopup) and we add to cart 
		// => doing back from cart would load the product edit page => we do not want it !!!
		// => Triggering back before doing the "add to cart" will be a workaround while removing product edit from history !!!
		history.back();
	}

	mobile_ui_spinner_modal();
	
	mobile_save_all_changed( false, true, function(res) {
		
		console.warn(`mobile add to cart mobile_save_all_changed resumed with: ${res}`);	

		if (res == true) {
	
			mobile_stop_timed_save();	

			// var path = "/customers/" + g_customer_id + "/tokenized_users/" + g_tokenized_user_id + "/products/" + g_product_id + "/verify_order_ready.json" + "?locale=" + I18n.locale;
			var path = 
			`/customers/${g_customer_id}/
					tokenized_users/${g_tokenized_user_id}/
						products/${g_album.id}/
							verify_order_ready.json?locale=${I18n.locale}&include_pages=true`;
			var url = g_editor_url + path;

			$.ajax({
			  type: 		"get",
			  async: 		true,
			  url: 			url, 
			  cache: 		false,
			  success: function(res) {
			  	console.warn(`mobile add to cart verify_order_ready resumed`);
			  	console.warn(`mobile add to cart verify_order_ready resumed with: ${res.verified}`);

			  	if (res.verified == true) {

			  		var path = "/cart_append.json";
			  		var url = path;

			  		if (g_album.type == "PhotoPrint") {
			        
			        var p_count = 0
			        var p_data = [];
			        for (var i=0; i<res.pages.length; i++) {
			        	var quantity = res.pages[i].quantity;
			        	p_data.push( {
			        		id:       res.pages[i].id,
			        		size:     res.pages[i].sizes,
			        		quantity: quantity,
			        	} );
			        	p_count += quantity;
			        }

			  			var editor_data = {
					  		extra_pages: p_count,
					  		total_pages: p_count,
			  				pages_data: p_data,
			  			}

			  		} else {
			  			var editor_data = {
					  		extra_pages: pages_extra_pages(),
					  		total_pages: pages_actual_page_count(),			  				
			  			}
			  		}
			  		
						$.ajax({
						  type: 		"post",
						  async: 		true,
						  url: 			url, 
						  cache: 		false,
						  dataType: 'json',
						  data: {
						  	item: g_user_item_id,
						  	editor_data: editor_data,
						  },
						  success: function(data) {
						  	mobile_ui_spinner_modal_close();
						  	// on general ajax sucees event
						  	// do_load_page( "page-url", { url: (data.redirect) }, "סל הקניות", { persistent: false, add_to_history: false } );
						  },
							error: function (jqXHR, ajaxOptions, thrownError) {
								mobile_ui_spinner_modal_close();
								alert("failed adding to cart");
								// resume periodic saves if failed adding to cart and we are still in editor
								mobile_start_timed_save();
							}
						});

			  	} else {

			  		mobile_ui_spinner_modal_close();

			  		mobile_start_timed_save();
			  		
			  		var upload_counter = g_uploader.uploading_count();
			  		if (upload_counter && (upload_counter > 0)) {
			  			mobile_ui_alert_modal({
			  				title: I18n.t("mobile.alerts.still_uploading_title"),
			  				txt: I18n.t("mobile.alerts.still_uploading_txt"),
			  			});
			  		} else {

			  			mobile_ui_alert_modal({
			  				title: I18n.t("mobile.alerts.add_to_cart_failed_title"),
			  				txt: res.txt,
			  			});

			  			var container = $(pages_page_container(res.p_id).elements_container_selector).closest(".page-previewer")[0];
			  			var topPos = container.offsetTop;

			  			document.getElementById('pages-container-inner').scrollTop = topPos;
			  			
			  		}

			  	}
			  },
			  error: function (jqXHR, ajaxOptions, thrownError) {
					mobile_ui_spinner_modal_close();
					alert("error adding to cart");
					// resume periodic saves if failed adding to cart and we are still in editor
					mobile_start_timed_save();
			  },
			});

		} else {
			alert("failed storing changes");
		}

	});

});

$( document ).on('click', ".preview-container .close", function(ev) {

	if ( $(this).closest(".preview-container.page-editor").length > 0 ) {
		
		document.getElementById('picndo_print_iframe').contentWindow.postMessage( { cmd: "external_save", }, "*");
		var page_id = $(this).closest(".preview-container.page-editor").attr("data-page-id");

		// TODO - wait for message from iframe that save performed and the close the ifrane viewer and reload the page
		// Till postmessage implemented - just create a timer and reload the page when done
		setTimeout( function() {
			
			viewer_hidden();
			if (g_viewer_visible_pending_resize) {
				mobile_load_all_pages_in_time();
			}
			viewer_visible_pending_resize(false);
			
			var path = ("/customers/" + g_customer_id + "/tokenized_users/" + g_tokenized_user_id + "/products/" + g_product_id + "/pages/" + this.page_id + ".json"); 
			var url = g_editor_url + path;

			$.ajax({
			  type: 		"get",
			  async: 		true,
			  url: 			url, 
			  cache: 		false,
			  dataType: 'json',
			  success: function(data) {
			  	// debugger;
			  	var selector = $(".page[data-page-id='" + this.page_id + "']").closest(".page-previewer");
			  	
			  	load_all_pages_async( [ data ], 0, "#page_preview", false, function() {
	          return {
	            selector: this.selector,
	            container_str: this.container_str,
	          };
        	}.bind( { selector: selector, container_str: pages_page_container(this.page_id).name } ) );
			  
			  }.bind( { page_id: page_id } ),
				error: function (jqXHR, ajaxOptions, thrownError) {
					alert("error adding to cart");
				}
			});

		}.bind( { page_id: page_id } ), 300);

	} else {
		viewer_hidden();
		if (g_viewer_visible_pending_resize) {
			mobile_load_all_pages_in_time();
		}
		viewer_visible_pending_resize(false);
	}
	
	
	return false;
});


function btn_page_id( btn_el ) {
	return $(btn_el).closest(".page-previewer").find(".page").attr("data-page-id")
}

$( document ).on('click', ".mobile-add-images-btn", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	do_load_page( "images", { product_id: g_product_id }, I18n.t("mobile.images.photos"), { add_to_history:true, persistent:true } );
});

$( document ).on('click', ".delete-page-btn", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	var page_id = $(this).closest(".page-previewer").find(".page").attr("data-page-id");
	var el = $(this).closest(".page-previewer")[0]; 

	mobile_ui_yes_no_modal( {
		modal: true,
		title: I18n.t("mobile.yes_no.delete_page_title"),
		txt: I18n.t("mobile.yes_no.delete_page_txt"),

		sign: "fal fa-info",

		yes_txt: I18n.t("mobile.yes_no.delete_page_yes_btn"),
		no_txt: I18n.t("cancel"),

		btn_style: "block",

		yes_cb: function() {

			
			var page_id = this.page_id;

			var path = "/customers/" + g_customer_id + "/tokenized_users/" + g_tokenized_user_id  + "/products/" + g_product_id + "/pages/" + page_id;
			var url = g_editor_url + path;

		  $.ajax({
		    type:     "delete",
		    async:    true,
			  url:      url,
		    cache:    false,
		    success:  function(data) {	
		    	pages_delete_page_container(this.page_id);
		    	$(this.el).remove();
		    }.bind( { page_id:this.page_id, el:this.el } ),
		  });

		}.bind( { page_id: page_id, el: el, } ),

	});
		
});


$( document ).on('click', ".mobile-add-sheet-btn, .add-page-btn", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	var add_after_id = null;
	var add_after_previewer_el = null;

	if ($(this).hasClass("mobile-add-sheet-btn")) { // footer btn

		var base_offset = // now holds center of .pages-container
			(document.querySelector('.pages-container').clientHeight/2) + // .pages-container height/2
			document.querySelector('.pages-container').getBoundingClientRect().top ; // .pages-container distance from document top

		base_offset -= 140; // now the new page will probably be created and shown in the middle and not bottom of .pages-container

		for (var i=0; ((i<20)&&(add_after_previewer_el==null)); i++) {
			var els = document.elementsFromPoint(
									(document.querySelector('.pages-container').clientWidth / 2),
									(base_offset - i*10)
								)

			for (var j=0; j<els.length; j++) {
				if ( $(els[j]).closest(".page-previewer").length > 0 ) {
					add_after_previewer_el = $(els[j]).closest(".page-previewer")[0];
					add_after_id = $(add_after_previewer_el).find(".page").attr("data-page-id");
					break;
				}
			}

		}

	} else {
		add_after_previewer_el = $(this).closest(".page-previewer")[0];
		add_after_id = $(add_after_previewer_el).find(".page").attr("data-page-id");
	}

	var path = "/products/" + g_product_id + "/pages";
	var url = g_editor_url + path;
	$.ajax({
	  type: "POST",
	  async: true,
	  url: url,
	  cache: false,
	  data: { 
    	current_page: add_after_id,  
    	page_type: "album_regular",
  	},
	  success: function(data) {

	  	var page_index = (pages_count() + 1);

			if (this.add_after_previewer_el) {
				
				var page_str = previewer_el_str( { 
					page_type: "dummy_not_cover_string", 
					index: page_index, /*TODO which index???*/
					load_counter: 1,
				} );
				$(this.add_after_previewer_el).after(page_str);

				var previewer_container_selector = ("#" + "page-previewer-" + page_index);

				var el_container = $(".pages-container").find("#" + "page-previewer-" + page_index)[0];
				var height = previewer_height_in_px( { container: el_container, data: data.page, page_count: data.length,})
        $(el_container).css( { "height": (height + "px") } );				

        var page_container_selector = previewer_container_selector + " .page";
			
				window[("g_pages_album_" + page_index)] = new AlbumPages( {  
                                                // elements_container_selector: ".page-previewer .page",
                                                elements_container_selector: page_container_selector,
                                                name: ("g_pages_album_" + page_index),

                                                is_editor: false,
                                             } );

				g_all_album_pages[ data.page.id ] = window[("g_pages_album_" + page_index)];

				var selector = $(".page-previewer-" + page_index)[0];

        load_all_pages_async( [ data.page ], 0, "#page_preview", false, function() {
          return {
            selector: this.selector,
            container_str: this.container_str,
          };
        }.bind( { selector: selector, container_str: ("g_pages_album_" + page_index) } ) );

        var page_el = $(selector).find(".page");

        mobile_inner_mask_observer_set( selector );

        page_el.closest(".page-previewer").find(".page-actions-container").css( {
          "width"   : page_el.find(".page_inner").width(),
          "visibility": "visible",
        });
        page_el.closest(".page-previewer").find(".page-actions-container").addClass("visible");
        
        // no need to keep it in 1 like on all pages load - (done on all pages load for ios black images bug)
        $(selector).attr("data-load-counter", 2); 
        

			} else {
				alert("Failed adding page - page position unknown");
			}

			mobile_update_pages_counters();

	  }.bind( { add_after_previewer_el:add_after_previewer_el, } ),

	});

});

$( document ).on('click', ".sort-pages-btn", function(ev) {
	ev.preventDefault(); // do none to not fuck up with sortable pages
	// ev.stopPropagation();
});

$( document ).on('click', ".auto-layout-btn", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	var right_only 	= ($(this).closest(".right").length > 0)	? true : false;
	var left_only 	= ($(this).closest(".left").length > 0)	? true : false;
	var both = ($(this).closest(".both").length > 0)	? true : false;

	var left_or_right = both ? "both" : ( (right_only ? "right" : left_only ? "left" : "both") );

	var page_id = btn_page_id(this);
	var page = pages_page_container(page_id);

  if ( (page.page_type == 'album_cover') && (g_album.customer_product.product_group.render_as_pages == false) ) {	
  	
  	do_load_page("cover-slector", { page_id: page_id, left_or_right: left_or_right, templates: g_auto_template.get_templates_data(true) }, I18n.t("mobile.images.title"), { add_to_history:true, persistent:false, } );

  } else {

		var img_container_count = Object.keys( g_all_album_pages[page_id].toJSON( {
			imgs_only:true, 
			used_img:false,
			left_or_right: left_or_right,
		}) ).length

		var template = g_auto_template.get_template(img_container_count);

		if (template) {

			g_auto_template.current_page_from_template(template, {
				add_margin: 		false,
				left_or_right: 	left_or_right,

				page_id:        page_id, // used only for mobile
				album_page:     page, // used only for mobile
				selector:       $(this).closest(".page-previewer")[0],
				editor:         false,
			});

		} else {
			alert("could not find a template for " + img_container_count + " photos");
			// $(".page-container").removeClass("inactive");
		}

	}

});

$( document ).on('click', ".add-image-to-page-btn", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	var right_only 	= ($(this).closest(".right").length > 0)	? true : false;
	var left_only 	= ($(this).closest(".left").length > 0)	? true : false;
	var left_or_right = (right_only ? "right" : left_only ? "left" : "both");

	var page_id = btn_page_id(this);
	do_load_page("add-images", { product_id: g_product_id, page_id:page_id, left_or_right: left_or_right, }, 
									I18n.t("mobile.images.photos_to_add"), { add_to_history:true, persistent: false } );
});

$( document ).on('click', ".mobile-editor-info-btn", function(ev) {

	var html = handlebars_compile_and_execute("mobile-editor-info");
	$(".mobile-modal").html(html);
	$(".mobile-modal").removeClass("hidden");
		
});


// this is to avoid android context menu on image tag long press
$( document ).on('contextmenu', 'img', function(ev) {
	ev.preventDefault();
  ev.stopPropagation(); // not necessary in my case, could leave in case stopImmediateProp isn't available? 
  ev.stopImmediatePropagation();
  return false;
});


// var g_product_id = "933dddc7-fdab-45db-9e3d-2b1d0b1d83c2";
var g_product_id;
var g_user_item_id;


function mobile_editor_ready() {
	if ( is_fullscreen() ) {
  	$('body').addClass('full_screen'); 	
  }

	if (false) {
		console.error("does not compile and register handlebars");
	} else {

		// var source = document.getElementById("rotator_template").innerHTML;
	  // Handlebars.registerPartial("rotator_template", source);  

	  register_shared_helpers();

	  mobile_register_partials();

	}

	if ('MutationObserver' in window) {
		const mutation_observer_callback = function(mutationList, observer) {
			
			try {	
				
				if (mutationList.length > 0) {
					for (var i=0; i<mutationList.length; i++) {
						if ( $(mutationList[i].target).closest(".page-previewer").length > 0 ) {

							if ( $(mutationList[i].target).hasClass("page") ) {
								console.log("mutation_observer_callback cb Found");
								var page_el = mutationList[i].target;
								mobile_set_mask_as_inner(page_el);
								return;
							}
										
						}
					}
				}
			} catch (error) {
				console.error(error);
			}

		};

		g_mutation_observer = new MutationObserver(mutation_observer_callback);
	
	}	
}

$( document ).on('ready', function(ev) {	
	// console.log("jquery ready");

	// mobile_editor_ready();

});

function product_image_allready_assigned() {
	for (var i=0; i<g_album.pages_data.length; i++) {
		var p = g_album.pages_data[i];
		var els_keys = Object.keys(p.data);
		for (j=0; j<els_keys.length; j++) {
			var el = p.data[ els_keys[j] ];
			if ( el.img && (el.type == "AlbumImageEl") )  {
				return true;
			}
		}
	}
	return false;
}

var g_mobile_assets_uploaded = false;

function start_editor(product_id, user_item_id, opts) {

	function start_editor_load_editor(product_id) {

		mobile_editor_general_init( {
			replace: this.replace
		} );

	} // END - function start_editor_load_editor

	function start_editor_init_editor_if_needed(cb) {
		if (g_mobile_assets_uploaded == false) {

		  // var path = "/customers/" + g_customer_id + "/mobile_assets";
		  var path = "/customers/" + g_customer_id + "/tokenized_users/" + g_tokenized_user_id + "/products/" + product_id + "/mobile_assets";
			var url = g_editor_url + path;

		  $.ajax({
	  		type: 	"get",
	  		async: 	false,
	  		url: 		url, 
	  		cache: 	false,
		  	success: function(data) {

		  		// load the html - probably only for handlebars
					$.ajax({
				  	type: 	"get",
				  	async: 	false, // not async => this when we load js (below) allready exists for sure
					  url: 		(g_editor_url + data.shared_html), 
					  cache: 	false,	  
					  success: function(data) {
					  	$('body').append(data);
					  },
					  error: function (jqXHR, ajaxOptions, thrownError) {
							alert("error loading shared html");
							cb(false);
						},	
					});

					// loading the javascript
		  		jQuery.getScript( (g_editor_url + data.asset_js), function() {
		  			
		  			g_mobile_assets_uploaded = true;

		  			console.log("editor script loaded ");
		  			
		  			console.log("should this code be here ???");
		  			console.log("should this code be here ???");
		  			console.log("should this code be here ???");
						var source = document.getElementById("rotator_template").innerHTML;
		  			Handlebars.registerPartial("rotator_template", source);  
		  			var source = document.getElementById("bleeds_template").innerHTML;
		  			Handlebars.registerPartial("bleeds_template", source);  

		  			// start_editor_load_editor(product_id);
		  			cb(true);
		  		} );

		  	},
		  	error: function (jqXHR, ajaxOptions, thrownError) {
					alert("error loading editor resources");
					cb(false);
				}
			});
		} else {

			// start_editor_load_editor(product_id);
			cb(true);
		
		}

	} // END - function start_editor_init_editor_if_needed
	

	var path = "/customers/" + g_customer_id + "/tokenized_users/" + g_tokenized_user_id + "/products/" + product_id + "?edit=true";
	var url = g_editor_url + path;

	load_url( url, function(res, data)  {

		// pages_mobile_init();
		// mobile_start_timed_save();


		if (res == true) {
			g_product_id = product_id;
			g_user_item_id = user_item_id;

			g_album = data;
			
			// mobile_start_timed_save();

			if 	( 	
						(g_album.template_required == true) && 
						!g_album.origin_template_id &&
						(g_album.pages.length == 0) && // really needed ???
						// !g_is_gallery_shop
						!(value_if_defined("g_is_gallery_shop") == true)
					) {

				$(".mobile-modal").addClass("hidden");
				do_load_page("templates", { product_id: g_album.id }, I18n.t("mobile.editor.templates"), { add_to_history:false, persistent:false, replace:this.replace } ); // current page is create album/product => no need to keep it in history !!!				

			} else {

				if ( (g_album.type == "Calendar") && (!g_album.from_month) ) {
					$(".mobile-modal").addClass("hidden");
					do_load_page("calendar-configutation", { product_id: g_album.id }, I18n.t("mobile.calendar.calendar_configuration_page_title"), { add_to_history:false, persistent:false, replace:this.replace } ); // current page is create album/product => no need to keep it in history !!!				
				} else {
					if (g_album.album_ready || !g_album.data.auto_design_required) {
			    	console.error("TODO - move into do_load_page function");
			    	// do_load_page("pages", { }, "עריכה", { add_to_history:true, persistent:true, replace:this.replace } ); // current page is create album/product => no need to keep it in history !!!
			    } else {
			    	// do_load_page("images", { }, I18n.t("mobile.images.upload_photos"), { add_to_history:true, persistent:false, replace:this.replace } ); // current page is create album/product => no need to keep it in history !!!
			    }
			  
					start_editor_init_editor_if_needed(function(res) {
			    	if (res == true) {

			    		pages_mobile_init();
			    		mobile_start_timed_save();

			    		if (opts && opts.cb) { opts.cb(true); }

			    		start_editor_load_editor(product_id);

			    		// set cookie to resume last product if needed (on app loading);
			    		if ( !value_if_defined('g_is_gallery_shop') ) {
				    		setCookie("last_p_id", g_product_id, 1);
				    		setCookie("last_ui_id", g_user_item_id, 1);
				    	}

			    	} else {
			    		
			    		console.warn("Failed starting editor");
			    		if (opts && opts.cb) { opts.cb(false); }
			    		
			    	}
			    });

			  }

			}

		} else {
			console.warn("Failed loading editor url");
			if (opts && opts.cb) { opts.cb(false); }
		}
	
	});

}

function mobile_editor_general_init( opts ) {
	// remove_pages_product_tabs_if_exist();
	remove_all_prev_product_tabs_if_exist();

	AlbumPages.general_init_all();

	if (window['g_uploader'] && g_uploader) {
		// terminate if allready initialized
		g_uploader.terminate();
		g_uploader = null;

		// remove $(".main-container-notes") => might still be visible from previous un terminated previous uploader
		images_set_mimimum_images_note_if_needed(null);
	}

	if (g_album.type == 'PhotoPrint') {

		photo_print_start();

	} else {

		g_uploader = new ShopUploader( {
		  trigger_selector:       '.images-btn',

		  album_id:               g_album.id,
		  
		  external_source_url:    g_album.external_source_url,
		  external_source_name:   g_album.external_source_name,
		  external_source_id:     g_album.external_source_id,
		  upload_only:            (g_album.upload_only == true) ? true : false,

		  skip_gui:               false,
		  uploader_template:      "mobile-images-page",

		  cb_translations: function( str, options ) {
		  	return I18n.t( str, options );
		  },

		  cb_trigger_selector: function() {
		  	// images_load_page( container_el );
		  	do_load_page( "images", { product_id: g_product_id }, I18n.t("mobile.images.title"), { add_to_history:true, persistent: true } );
		  },

		  cb_initialized: function(data) {
		    console.log("cb_initialized");
		  },
		  
		  // new images imported - (from file input ...)
		  cb_new_images_selected: function(count) {
		  	new_images_selected(count);
		  	
		  	// adi baron => initialize the face detector here ???
		  	var pre_load_scripts = [
				  "https://cdn.jsdelivr.net/npm/@mediapipe/face_detection",
			 	 	"https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-core",
			  	// "https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-backend-webgl",
			  	"https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-backend-wasm",
			  	"https://cdn.jsdelivr.net/npm/@tensorflow-models/face-detection",
				];

		  	face_detection_init(pre_load_scripts, function() {
		  		// debugger;
		  	});
		  },
		  
		  // new image imported (by resizer ???)
		  cb_new_image: function(l_uuid, data, pos, image_left) {
		  	append_images_container(l_uuid, data, pos, image_left);
		  	
		  	images_set_mimimum_images_note_if_needed(null);
		  },
		  cb_updated_image: function(data, update_ready, update_page) {
		  	// debugger;
		  },
		  cb_initialized_external: function(data) {

		  },

		  // uploader initialization completed
		  cb_all_initialized: async function(data) {
		    console.log("cb_all_initialized");

		    var images_uuid_to_reupload = g_uploader.get_images().map( function(image) {
		    	return (
		    		(image.small == null) && 
		    		(image.medium == null) && 
		    		(image.big == null) && 
		    		(image.download.indexOf("DUMMY") != -1) 
		    	) ? image.uuid : null;
		    } ).filter(n => n);
		    
		    TimeDiffPrinter.print_with_str("before reupload_from_cache");
		    await g_uploader.reupload_from_cache( images_uuid_to_reupload );
		    TimeDiffPrinter.print_with_str("after reupload_from_cache");


		    if 	(
		    			( (g_album.album_ready || !g_album.data.auto_design_required) && (g_uploader.images_count() > 0) )
		    		)
		    {
		    	
		    	console.error("TODO - move into do_load_page function");
		    	// mobile_load_all_pages(g_product_id, "#page_preview");
		    	do_load_page("pages", { product_id: g_product_id }, I18n.t("mobile.editor.editing"), { add_to_history:true, persistent:true, replace:this.replace } ); // current page is create album/product => no need to keep it in history !!!
		    }
		    else if (
		    					( 
		    						(g_album.album_ready || !g_album.data.auto_design_required) && 
		    						( /*g_album.data.gallery_store_use_media*/ false || (value_if_defined('g_is_gallery_shop') == true) ) 
		    					)
		    				)
		    {

			    if ( (g_album.data.gallery_store_use_media) || product_image_allready_assigned() ) {
			    	do_load_page("pages", { product_id: g_product_id, add_media: g_album.data.gallery_store_use_media }, I18n.t("mobile.editor.editing"), { add_to_history:true, persistent:true, replace:this.replace } ); // current page is create album/product => no need to keep it in history !!!
			    } else {
			    	
			    	// do_load_page("images", { product_id: g_product_id }, I18n.t("mobile.images.title"), { add_to_history:false, persistent:false, replace:this.replace } ); // current page is create album/product => no need to keep it in history !!!	
			    	// do_load_page("add-images", { product_id: g_product_id }, I18n.t("mobile.images.title"), { add_to_history:false, persistent:false, replace:this.replace } ); // current page is create album/product => no need to keep it in history !!!	
			    	if (value_if_defined('g_is_gallery_shop') == true) {
			    		// do_load_page("add-images", { product_id: g_product_id, page_id: g_album.pages_data[0].id, max_selected_photos: 1 }, I18n.t("mobile.images.title"), { add_to_history:false, persistent:false, replace:this.replace } ); // current page is create album/product => no need to keep it in history !!!		
			    		do_load_page("images", { product_id: g_product_id, add_selected: true, max_selected_photos: 1 }, I18n.t("mobile.images.title"), { add_to_history:false, persistent:false, replace:this.replace } ); // current page is create album/product => no need to keep it in history !!!		
			    	}
			    	else {
			    		console.error("unknown state in cb_all_initialized");
			    	}
			    
			    }
		    } 
		    else {
		    	var max_selected_photos = (g_album.data.auto_design_required && (g_album.data.single_image_auto_template == true)) ?
		    																g_album.customer_product.max_num_of_pages :
		    																null;

		    	do_load_page("images", { product_id: g_product_id, max_selected_photos: max_selected_photos, }, I18n.t("mobile.images.title"), { add_to_history:false, persistent:false, replace:this.replace } ); // current page is create album/product => no need to keep it in history !!!
		    }

		    $(".mobile-modal").addClass("hidden");

	      load_auto_templates( {
	      		mobile: (g_is_mobile == true) ? true : false,
	      		ready_cb: function() {
	        		console.log("auto templates loaded");
	      		},
	      	}
	      );
		  
		  }.bind( {
		  	replace: ( (opts && opts.replace) ? true : false ), 
		  } ),

		  // image uploaded to server
		  cb_upload_complete: function(images_counter, uploaded_uuid) {
				images_set_mimimum_images_note_if_needed(images_counter);
		  },

		  cb_update_used_status: function(id, uuid, count) {
		  	var els_by_id = $(".images-container-el[data-id='" + id + "']");
		  	els_by_uuid = $(".images-container-el[data-uuid='" + uuid + "']");
		  	if ( count && (count > 0) ) {
		  		els_by_id.addClass("used");
		  		els_by_uuid.addClass("used");
		  		els_by_id.find(".img-used").attr("data-used-count", count);
		  		els_by_uuid.find(".img-used").attr("data-used-count", count);
		  	} else {
		  		els_by_id.removeClass("used");
		  		els_by_uuid.removeClass("used");
		  	}
		  },

		});

	}
}

function mobile_add_image_to_el(page_id, element_id, image_id, image_uuid, success_cb) {
	var page = pages_page_container(page_id);
	var el = page.element(element_id);

	var img = g_uploader.image_from_ids(image_id, image_uuid);

	if (img) { el.img(img); }

	if (success_cb) { success_cb(); }
}

function mobile_add_or_remove_images_to_page(page_id, add_images_uuids, remove_images_uuids, opts, success_cb) {
	// just assigning ....
	var images_uuids = add_images_uuids;
	// END - just assigning ....

	var left_or_right = opts ? opts.left_or_right : null;

	var page = pages_page_container(page_id);

	// var prev_elements_count = page.elements_count();
	var prev_elements_count = Object.keys( g_all_album_pages[page_id].toJSON( {
		imgs_only:true, 
		used_img:true,
		left_or_right: left_or_right,
	}) ).length

	var new_elements_count = (	prev_elements_count
															+ 
															(images_uuids ? images_uuids.length : 0) 
															- 
															(remove_images_uuids ? remove_images_uuids.length : 0)
													 );
	var template = null;
	if (new_elements_count == 0) {
		// probably no template for 0 elements =>
		// we find template with 1 element and remove the first (and only) element
		var alternative_template = g_auto_template.get_template(1);
		template = CloneObj( alternative_template ); // not to change the actual template
		delete template.data[ Object.keys(template.data)[0] ];
	} else {
		template = g_auto_template.get_template(new_elements_count);
	}

	if (template) {

		g_auto_template.current_page_from_template(template, {
			add_margin: 		false,
			left_or_right: 	left_or_right,

			page_id:        page_id, // used only for mobile
			album_page:     page, // used only for mobile
			selector:       $(".page[data-page-id='" + page_id + "']").closest(".page-previewer")[0],
			editor:         false,

			new_image_uuids: images_uuids,
			remove_images_uuids: remove_images_uuids,
		});

		if (success_cb) { success_cb(); }

	} else {
		
		console.error("could not find a template for " + new_elements_count + " photos");
		
	}

}

var g_face_detection_scripts_loaded = false;
function loadScripts (scripts, cb) {

	this.scripts = scripts;
	this.cb = cb;
	this.fail_count = 0;
	this.success_count = 0;

	this.error_cb = function(e) {
		this.fail_count += 1;
		if (this.fail_count == 1) {
			// call on first error
			this.cb(false);
		}
	}

	this.success_cb = function(e) {
		this.success_count += 1;
		if (this.success_count == this.scripts.length) {
			// call only once when all loaded
			this.cb(true);
		}
	}

	this.go = function() {
		if ( (g_face_detection_scripts_loaded == false) && (this.scripts.length > 0) ) {
			for (var i=0; i<this.scripts.length; i++) {
				var e = document.createElement("script");
		  	e.src = this.scripts[i];
	  		e.type = "text/javascript";
	  		e.async = false;
	  		e.addEventListener('error', this.error_cb.bind(this) );
	  		e.addEventListener('load', this.success_cb.bind(this) );
	  		document.getElementsByTagName("head")[0].appendChild(e);
			}
		} else {
			this.cb();
		}
	}
	
	this.go();
}

var general_sleep = (delay) => new Promise((resolve) => setTimeout(resolve, delay))
var general_detection_index = 0;
var general_detection_working_mutex = false;
var general_images_to_detect = 0;
var g_faces_found = {};

var g_face_detector;
var g_estimationConfig;



async function face_detection_init(pre_load_scripts, cb) {
	
	if (!g_face_detector  && (pre_load_scripts.length > 0) ) {

		new loadScripts(pre_load_scripts, async function(success) {

			if (success && !g_face_detector) {

				TimeDiffPrinter.print_with_str("getting the model");
				const model = faceDetection.SupportedModels.MediaPipeFaceDetector;
				const detectorConfig = {
									  runtime: 'mediapipe',
									  modelType: 'full',
									  maxFaces: 20,
									  solutionPath: 'https://cdn.jsdelivr.net/npm/@mediapipe/face_detection',
								              // or 'base/node_modules/@mediapipe/face_detection' in npm.
							};

				TimeDiffPrinter.print_with_str("creating the detector");			
				g_face_detector = await faceDetection.createDetector(model, detectorConfig);
				// g_face_detector = faceDetection.createDetector(model, detectorConfig);
				TimeDiffPrinter.print_with_str("detector created");
				if (cb) { cb(); }

			} else {
				// allready initialized
				if (cb) { cb(); }
			}

		})

	} else {
		if (cb) { cb(); }
	}

}

async function face_detection_detect(image, cb) {

	var estimationConfig = {flipHorizontal: false};
	var faces = await g_face_detector.estimateFaces(image, estimationConfig);
	cb(faces);
}
	
async function findFaces(medias_to_detect, cb) {

	g_faces_found = {};
	general_detection_index = 0;
	
	// var images = g_uploader.get_images();
	var images = medias_to_detect
	general_images_to_detect = images.length;

	while (general_detection_index < images.length) {
		
		if (general_detection_working_mutex == false) {

			general_detection_working_mutex = true;
			var image = new Image();

			image.crossOrigin="anonymous";
			image.setAttribute("data-id", images[general_detection_index].id);
			image.setAttribute("data-uuid", images[general_detection_index].uuid);
			image.setAttribute("data-external-source", images[general_detection_index].external_source)

			image.onload = function() {

				face_detection_detect(this, function(faces) {
					var id = this.getAttribute("data-id");
					var uuid = this.getAttribute("data-uuid");
					var external_source = this.getAttribute("data-external-source");
					
					var key = ( external_source && (external_source=="picndo") ) ? 
												id :
												[id, uuid];
					
					g_faces_found[key] = { "size":[this.width, this.height], "faces_data":faces };
					general_detection_index +=1;
					general_detection_working_mutex = false;
					ui_set_design_status(I18n.t("mobile.editor.progress_face_detection") + " " + Math.round( (general_detection_index / general_images_to_detect)*100 ) + "%" );
				}.bind(this));

			}
			image.onerror = function() {
				console.error("face detection media load error");
				general_detection_index +=1;
				general_detection_working_mutex = false;	
			}
			image.src = (images[general_detection_index].data || images[general_detection_index].medium);
		}

		await general_sleep(100);
	}

	cb(g_faces_found);

}

function ui_set_upload_percents(upload_percents) {
	$(".wait-for-desing-spinner-container .design-progress").text( I18n.t("mobile.editor.progress_sending_data") + " " + upload_percents + "%");
}

function ui_set_design_status(status_str) {
	$(".wait-for-desing-spinner-container .design-progress").text(status_str);	
}

function do_design_book(options) {

	function send_design(opts) {

		var path = `/customers/${g_customer_id}/tokenized_users/${g_tokenized_user_id}/products/${g_product_id}/design`;
		if ( (opts.force === true) || (opts.force === false) ) {
			path = path + "?force=" + opts.force
		}
		var url = g_editor_url + path;		

		$.ajax({
		  type: "POST",
		  async: true,
		  // url: path,
		  url: url,
		  cache:    false,
		  data: opts.post_data,
		  success: function(data) {

		  	TimeDiffPrinter.print_with_str("design request accepted in service");

		  	ui_set_upload_percents(100);

				var design_status_interval = setInterval( function() {
					var path = "/customers/" + g_customer_id + "/tokenized_users/" + g_tokenized_user_id + "/products/" + g_product_id + "/design_status";
					var url = g_editor_url + path + "?include_face_data=true";

				  $.ajax({
					  type: "get",
					  async: true,
					  // url: path,
					  url: url,
					  cache:    false,
					  success: function(data) {
					  	
					  	ui_set_design_status(data.album_auto_design_status_description);

					  	if (data.album_auto_design_status == "ready") {
					  		clearInterval(this.design_status_interval);

					  		// new design - clear pages
					  		pages_delete_all_page_container();
					  		// clear images used counters 
					  		g_uploader.clear_used();


					  		if (data.face_data) {
					  			g_uploader.set_face_data(data.face_data);
					  		}

								if (g_album.data.album_ready == true) {

									do_load_page("pages", { product_id: g_product_id }, I18n.t("mobile.editor.editing"), { add_to_history:true, persistent:true, force_rebuild: true, } );

								} else {

									g_album.data.album_ready = true; // it is ready now

						  		// mobile_load_all_pages(g_product_id, "#page_preview");
						  		do_load_page("pages", { product_id: g_product_id }, I18n.t("mobile.editor.editing"), { add_to_history:true, persistent:true, } );
						  		console.error("TODO - move into do_load_page function");
						  		
						  	}

						  	$(".mobile-modal").addClass("hidden");

					  	} else if (data.album_auto_design_status == "design_error") {
					  		
					  		clearInterval(this.design_status_interval);
					  		$(".mobile-modal").addClass("hidden");
					  		
					  		alert("Error creating design");
					  		
					  	}

					  }.bind( {design_status_interval:design_status_interval} ),
						error: function (jqXHR, ajaxOptions, thrownError) {
							alert("error design status");
						}.bind( {design_status_interval:design_status_interval} )
				  });

				}, 5000);

		  },
			error: function (jqXHR, ajaxOptions, thrownError) {
				$(".mobile-modal").addClass("hidden");
				
				if ( jqXHR.responseJSON && (jqXHR.responseJSON.error == "missing original time") ) {
				  
					mobile_ui_yes_no_modal( {
						modal: true,
						title: I18n.t("mobile.yes_no.design_manual_or_auto_title"),
						txt: I18n.t("mobile.yes_no.design_manual_or_auto_txt"),

						sign: "fal fa-info",

						yes_txt: I18n.t("mobile.yes_no.design_manual_or_auto_btn_auto"),
						no_txt: I18n.t("mobile.yes_no.design_manual_or_auto_btn_manual"),

						btn_style: "block",

						yes_cb: function() {
							do_design_book( { force:true, } );
						},
						no_cb: function() {
							do_design_book( { force:false, } );
						},
					});

				} else {
					alert("error design book");
				}

			},
			xhr: function () {
        var xhr = $.ajaxSettings.xhr();
        // xhr.onprogress = function e() {
        //     // For downloads
        //     if (e.lengthComputable) {
        //         console.log(e.loaded / e.total);
        //     }
        // };
        xhr.upload.onprogress = function (e) {
          // For uploads
          if (e.lengthComputable) {
            var upload_percents = Math.round( (event.loaded / event.total) * 100 );
            console.log("uploaded: " + upload_percents + "%");
            ui_set_upload_percents(upload_percents);
          }
        };
        return xhr;
	    }

	  });

	}

	function do_design_book_inner(options) {
		var force = options && options.force

		var html = handlebars_compile_and_execute("mobile-waiting-for-design", {} );
		$(".mobile-modal").removeClass("hidden");
		$(".mobile-modal").html(html);

		/********************************************
		 						Do Face detection
		********************************************/
		
		var medias_to_detect = [];	
		var medias = g_uploader.get_images();
		
		for (var i=0; i<medias.length; i++) {
			
			if (!medias[i].faces) {
			// if (true) {	

				var uuid = medias[i].uuid;
				var face_data = g_uploader.get_face_data(null, uuid);
				if (!face_data) {
					medias_to_detect.push(medias[i]);
				}

			}

		}

		if (medias_to_detect.length > 0) {
			var pre_load_scripts = [
			  "https://cdn.jsdelivr.net/npm/@mediapipe/face_detection",
		 	 	"https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-core",
		  	// "https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-backend-webgl",
		  	"https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-backend-wasm",
		  	"https://cdn.jsdelivr.net/npm/@tensorflow-models/face-detection",
			];
		} else {
			var pre_load_scripts = [];
		}

		TimeDiffPrinter.print_with_str("ready to load scripts");

		face_detection_init(pre_load_scripts, function() {

			var uploader_medias = g_uploader.get_images();
			// medias = uploader_medias.map(m => { "uuid": m.uuid, "time": m.time, "width": m.width, "height": m.height, "data": m.data } );
			// medias = uploader_medias.map( function(m) { return { "uuid": m.uuid, "time": m.time, "width": m.width, "height": m.height, "data": m.data }; } );

			// no need to send data while face detection done on client
			var medias_data = 	uploader_medias.map( function(m) { 
									return ( m.external_source && (m.external_source.length > 0) ) ?
										{ "external_sournce":true, "id":m.id, "time": m.original_time, "name":m.name, "width":m.width, "height":m.height, "added_to_position":0 } :
										{ "uuid":m.uuid, "time":m.time, "name":(m.file && m.file.name), "width":m.width, "height":m.height, "added_to_position":m.position, }; 
									} 
								);

			var post_data = { };
			// post_data = {
			// 	"medias": JSON.stringify( medias ),
			// };
			
			var cover_image = (value_if_defined('g_is_gallery_shop') == true) ? 
											$(".set-cover-image-btn.selected").closest(".images-container-el") :
											$(".images-container .images-container-el.selected");
			

			if (cover_image.length > 0) {
				if (value_if_defined('g_is_gallery_shop') == true) {
					var m = g_uploader.image_from_external( $(cover_image[0]).attr("data-id"), $(cover_image[0]).attr("data-uuid") )
					if (m) {
						// post_data["cover_image"] = JSON.stringify( { "external_sournce":true, "id":m.id, "time": m.original_time, "name":m.name, "width":m.width, "height":m.height, "added_to_position":0 } );
						var cover_image_data = { "external_sournce":true, "id":m.id, "time": m.original_time, "name":m.name, "width":m.width, "height":m.height, "added_to_position":0 };
					}
				} else {
					// post_data["cover_image"] = JSON.stringify( { id: $(cover_image[0]).attr("data-id"), uuid: $(cover_image[0]).attr("data-uuid"), } )
					var cover_image_data = { id: $(cover_image[0]).attr("data-id"), uuid: $(cover_image[0]).attr("data-uuid"), }
				}
			}

			if (options["density"]) {
				post_data["density"] = options["density"]
			}

			if ( g_face_detector || (this.medias_to_detect.length == 0) ) {

				TimeDiffPrinter.print_with_str("load scripts resumed");

				findFaces( this.medias_to_detect, function(faces_found) {

					TimeDiffPrinter.print_with_str("find faces resumed");
							
					// normalize the faces_found data to current structure and send it with media data for creating the book

					var l_keys = Object.keys(faces_found);
					for (var i=0; i<l_keys.length; i++) {
						var data = faces_found[ l_keys[i] ];
						var face_count = data.faces_data.length;
						var faces = [];
						for (var j=0; j<face_count; j++) {
							faces.push( {
								"face_rectangle": {
									"left": 		data.faces_data[j].box.xMin,
									"top":   		data.faces_data[j].box.yMin,
									"width":  	data.faces_data[j].box.width,
									"height":   data.faces_data[j].box.height,
								}
							});
						}
						faces_found[ l_keys[i] ]["face_data"] = {
							"face_num": face_count,
							"faces": faces,
						} 
						delete faces_found[ l_keys[i] ]["faces_data"]; //the original data

					}
					g_uploader.append_to_face_data(faces_found);

					// call the ajax below with the face data to create a book 
					post_data["face_data"] = JSON.stringify( faces_found );

					// force should be explicity defined - if undefined it will not be added
					// if ( (this.force === true) || (this.force === false) ) {
					// 	path = path + "?force=" + this.force
					// }
					// var url = g_editor_url + path;

					post_data["medias"] = JSON.stringify( this.medias );
					post_data["cover_image"] = JSON.stringify( this.cover_image );
					
					send_design( { force:this.force, post_data: post_data } );

				}.bind( { force:this.force, medias: medias_data, cover_image:cover_image_data, } ) );

				// TimeDiffPrinter.print_with_str("face data normalized starting design");
			} else {
				// var url = `${g_editor_url}/${path}?force=true`;
				
				mobile_ui_alert_modal( {
					"title": "",
					"txt": "Face Detection failed - design results might not be optimized",
					"yes_cb": function() {
						mobile_ui_spinner_modal();
						send_design( { force:true, post_data: this.post_data } );
					}.bind( { post_data:post_data, } ),
				} );

			}
				
		}.bind( { medias_to_detect: medias_to_detect, force: force, } ) );

		/********************************************
		 					 END - Do Face detection
		********************************************/
	}


	var force = options && options.force

	TimeDiffPrinter.print_with_str("starting design");
	
	if 	( /*( !( g_album.pages[0] && (Object.values(g_album.pages[0])[0] == "album_cover") ) ) &&*/
				( 
					( value_if_defined('g_is_gallery_shop') == true ) && 
					( $(".set-cover-image-btn.selected").length == 0 ) &&
					( g_album.data.edit_cover == "use_cover" )
				) 
				||
				(
					( value_if_defined('g_is_gallery_shop') != true ) &&
					( $(".images-container .images-container-el.selected").length == 0 )	
				)
			)
	{
		
		// do_load_page("cover-page", { product_id: g_product_id }, "עיצוב הכריכה", { persistent: true } );
		alert( I18n.t("mobile.images.select_cover_image") );

	} else {

		if ( g_album.pages_data && (g_album.pages_data.length > 0) ) {
			// design already exist
			mobile_ui_yes_no_modal( {
				modal: true,
				
				// title: I18n.t("mobile.yes_no.design_manual_or_auto_title"),
				title: "האלבום כבר עוצב",
				// txt: I18n.t("mobile.yes_no.design_manual_or_auto_txt"),
				txt: "עיצוב מחדש ימחק את כל השינויים שבצעת עד כה",

				sign: "fal fa-info",

				// yes_txt: I18n.t("mobile.yes_no.design_manual_or_auto_btn_auto"),
				yes_txt: "עצב מחדש",
				// no_txt: I18n.t("mobile.yes_no.design_manual_or_auto_btn_manual"),

				btn_style: "block",

				yes_cb: function() {
					
					mobile_ui_density_page_design_modal( {
						cb: function(value) {
							do_design_book_inner( { force:force, density:value, } );
						}
					});
					
				},

			});
		
		} else {

			mobile_ui_density_page_design_modal( {
				cb: function(value) {
					do_design_book_inner( { force:force, density:value, } );
				}
			});
	
		}

	}

}

$( document ).on('click', ".design_book", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	do_design_book();
});

$( document ).on('click', ".design_continue_to_pages", function(ev) {
	ev.preventDefault();
	ev.stopPropagation();
	
	var data = {
		product_id: g_product_id,

	}

	var seleted_images = images_selected_images();

	if ( 	//g_is_gallery_shop && 
				( value_if_defined("g_is_gallery_shop") == true ) &&
				( (g_album.type == "Canvas") || (g_album.type == "Frame") ) && 
				($(".images-container-page").attr("data-add-selected") == 'true') &&
				(seleted_images.ids.length > 0) && 
				(seleted_images.uuids.length > 0)
	) {
		data["add_selected"] = true;
		data["selected_images_arr"] = seleted_images;
	}

	do_load_page("pages", data, I18n.t("mobile.editor.editing"), { add_to_history:true, persistent:true, force_rebuild: false, } );


});


function mobiie_load_preview(product_id, close_btn) {

	close_btn = (typeof close_btn === "undefined") ? false : close_btn

	// var html = "<iframe id='picndo_print_iframe' name='picndo_print_iframe' src='" + g_editor_url + "/products/" + product_id + "/?mobile=true' " +
	// 							"style='position:absolute; width:100%; height:100%;' frameborder='0'></iframe>";							

	// var html = handlebars_compile_and_execute("mobile-preview-page", {
	var html = handlebars_compile_and_execute("mobile-preview-page_viewer", {	
		url: (g_editor_url + "/products/" + product_id + "/?mobile=true"),
		close_btn: close_btn,
		page_editor: false,
	} );

	viewer_visible();

	$(".main-viewer").html(html);

}

function mobiie_load_advanced_editor(customer_id, tokenized_user_id, product_id, page_id) {
	
	var html = handlebars_compile_and_execute("mobile-preview-page_viewer", {	
		url: (g_editor_url + "/customers/" + customer_id + "/tokenized_users/" + tokenized_user_id + "/products/" + product_id + "/pages/" + page_id + "/edit"),
		close_btn: true,
		page_editor: true,
		page_id: page_id,
	} );

	viewer_visible();

	$(".main-viewer").html(html);
}

function viewer_visible() {
	$(".mobile-edior .main-container").css( { "display":"none", } );
	$(".mobile-edior .main-viewer").css( { "display":"flex", } );	
}

function viewer_hidden() {
	$('.mobile-edior .main-container').css( { 'display':'flex', } ); 
	$('.mobile-edior .main-viewer').css( { 'display':'none', } );
}

function is_viewer_visible() {
	var container_display = $('.mobile-edior .main-container').css( 'display' ); 
	var viewer_display = $('.mobile-edior .main-viewer').css( 'display' );

	return ( (viewer_display == "flex") && (container_display == "none") );
}


var g_viewer_visible_pending_resize = false;
function viewer_visible_pending_resize( val ) {
	g_viewer_visible_pending_resize = val;
}




// Create the query list.
const mediaQueryList = window.matchMedia("(orientation: portrait)");

// Define a callback function for the event listener.
function handleOrientationChange(evt) {
  
  console.log("handleOrientationChange !!!");

  if ( is_mobile_device() ) {

	  if ( 
	  		( history && history.state && (history.state.page == "pages") ) && 
	  		( $(".body-tab-panel.opened").hasClass("pages") ) // fix / workaround - while when being on cart checkout address page - virtual keyboard opening triggered 
	  																											// orientation event => not allwing to type address.

	  	) {

		  if (evt.matches) {
		    console.log("portrait");
		    viewer_hidden();

		  } else {
				viewer_visible();

		    // The viewport is currently in landscape orientation
		    console.log("landscape");
		    mobiie_load_preview(g_product_id);
		  }

		} else {
			console.log("Do none !!!");
		}

	} else {

		// not mobile - should we do anything ?

	}

}

var g_pages_resize_timeout = null;

var g_mutation_observer = null;
const g_mutation_observer_config = { attributes: true, childList: true, subtree: true };

function mobile_load_all_pages_in_time() {

	if ( history && history.state && (history.state.page == "pages") ) {
	
		if (!is_viewer_visible()) {

			mobile_ui_spinner_modal();

			if (g_pages_resize_timeout) {
				clearTimeout(g_pages_resize_timeout);
			}

			g_pages_resize_timeout = setTimeout( function() {

				if ( history && history.state && (history.state.page == "pages") ) {	
					console.warn("handle resize non mobile on pages");
					
					var el = $(".main-container-body-tabs .body-tab-panel.pages")[0];
					
					mobile_load_all_pages(g_product_id, el, function(res) {

						// g_pages_resize_timeout = null;
						$(".mobile-modal").addClass("hidden");

					});
				}

				g_pages_resize_timeout = null;
				// $(".mobile-modal").addClass("hidden");
			
			}, 2000);

		} else {
			// viewer is visible we will do the resize later
			viewer_visible_pending_resize(true);
		}

	}

}

function handleResize() {
	if ( !is_mobile_device() ) {
		console.warn("handle resize non mobile");
		mobile_load_all_pages_in_time();
	}
}


// Run the orientation change handler once.
handleOrientationChange(mediaQueryList);

// Add the callback function as a listener to the query list.
mediaQueryList.addEventListener('change', handleOrientationChange);
window.addEventListener('resize', handleResize);

console.error("loaded 2 - mobile-editor");
var page_template;
var images_template;
var images_galleries_menu_template;
var slide_template;
var backgrounds_template;
var stickers_template;

// var g_pages_album;

// var ready_counter = [];
// var g_ready_timeout = [];
// var g_page_load_params = {
// 			load_page_data_resumed: false,
// 			all_element_cb_resumed: false
// 		}

// var g_load_all_with_index_counter = [];

var g_uploader;

// helper function
// this perfom object cloning (only data not functions) - should be OK for data
function duplicate_obj(obj) {
  return JSON.parse(JSON.stringify(obj));
}

