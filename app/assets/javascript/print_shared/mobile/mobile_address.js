
$( document ).on('ready', function(ev) {	

	$( document ).on('click', ".js-delete-address-link", function(ev) {

		mobile_ui_yes_no_modal( {
			modal: true,
			title: I18n.t("mobile.yes_no.delete_address_title"),
			txt: I18n.t("mobile.yes_no.delete_address_txt"),

			sign: "fal fa-info",

			yes_txt: I18n.t("delete"),
			no_txt: I18n.t("cancel"),

			btn_style: "block",

			yes_cb: function() {

				var path = $(this._this).attr('data-address');
				var path = (path + "?mobile=true");

			  $.ajax({
			    type:     "delete",
			    async:    true,
				  url:      path,
			    cache:    false,
			    success:  function(data) {	
			    	console.log('address successfully deleted');
			    },
			    error: function (jqXHR, ajaxOptions, thrownError) {
			    	console.error('error deleting address');
			    },
			  });


			}.bind( { _this: this } ),		

		});

	});

});
