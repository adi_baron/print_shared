$( document ).on('ready', function(ev) {	
	
});

function mobile_projects_page_init() {

	if('IntersectionObserver' in window){
		
		var options = {
			// root: $("#projects-container")[0],
			root: $("#projects-container").parent()[0],
			threshold: [0],
		}
		
		var observer = load_carts_or_projects_observer(options);

		var pr_links = $("#projects-container .product-link");
		var pr_links_length = pr_links.length;

		for (var i=0; i<pr_links_length; i++) {
			var pr_link = pr_links[i];
			observer.observe(pr_link);
		}

	}

}

function mobile_cart_page_init() {

	if('IntersectionObserver' in window){

		var options = {
			// root: $("#line_items")[0],
			// root: $(".shopping-cart").parent()[0],
			root: $(".photo-print-content-tiles")[0],
			threshold: [0],
		}
		
		var observer = load_carts_or_projects_observer(options);

		var pr_links = $("#line_items .shopping-cart-item");
		var pr_links_length = pr_links.length;

		for (var i=0; i<pr_links_length; i++) {
			var pr_link = pr_links[i];
			observer.observe(pr_link);
		}

	}

}

function load_carts_or_projects_observer(options) {

	var observer = new IntersectionObserver(function(observerd) {
		console.log("observerd");
		var observerd_length = observerd.length;
		for (var i=0; i<observerd_length; i++) {
			if (observerd[i].isIntersecting == true) {
				
				if ( $(observerd[i].target).hasClass("shopping-cart-item") ) {
					var parent_class = "shopping-cart-item";
				} else {
					var parent_class = "product-link";
				}

				// $(observerd[i].target).find(".product-link-image-container-image").css( { "display": "block" } );
				$(observerd[i].target).find("." + parent_class + "-image-container-image").css( { "display": "block" } );

				// if ( !($(observerd[i].target).find(".product-link-image-container-image").attr("data-loaded") == "true") ) {
				if ( !($(observerd[i].target).find("." + parent_class + "-image-container-image").attr("data-loaded") == "true") ) {	
					// do load image
					var path = "/customers/" + g_customer_id + "/tokenized_users/" + g_tokenized_user_id + "/products/" + $(observerd[i].target).attr("data-product-id") + ".json";						
					var url = g_editor_url + path;
					$.ajax( {
					  type: "get",
					  async: true,
					  url: url,
					  cache:    false,
	  			  data: {
					  	"include_cover_sizes": true,
					  },
					  success: function(data) {

							if ( data.thumb_media_url && (data.thumb_media_url.length>0) ) {
								
								if (data.type == "Canvas") {
									var html = canvas_simulation_html(data);
									$(this).find("." + parent_class + "-image-container-image").html(html);
								} else {
									
									// $(this).find(".product-link-image-container-image").addClass(data.type);
									$(this).find("." + parent_class + "-image-container-image").addClass(data.type);
									// $(this).find(".product-link-image-container-image").addClass(data.direction);
									$(this).find("." + parent_class + "-image-container-image").addClass(data.direction);
									// $(this).find(".product-link-image-container-image > img").attr("src", data.thumb_media_url);
									$(this).find("." + parent_class + "-image-container-image > img").attr("src", data.thumb_media_url);
								}

								// $(this).find(".product-link-image-container-image").css( { "visibility":"visible" } );
								$(this).find("." + parent_class + "-image-container-image").css( { "visibility":"visible" } );

								if ( (data.type == "Album") && data.cover_sizes ) {
									var cs = data.cover_sizes
									
									// TODO
									// set shopping-cart-item-image-container-image position:relative; width:100%; height:0; padding-top:ration between height/width

									var half_size = (cs.width / 2);
									var half_size_to_show = ( (cs.width / 2) - 1*cs.bleed - (cs.cover_spine / 2) - 1*cs.cover_spine_fold ) ;


									var height_percentages_to_show_from_width = (cs.crop_height / half_size_to_show)*100;

									$(this).find("." + parent_class + "-image-container-image").css( { "padding-top": `${height_percentages_to_show_from_width}%` } );

									// var zoom = ( (cs.width / 2) / ( (cs.width / 2) - 1*cs.bleed - (cs.cover_spine / 2) - 1*cs.cover_spine_fold ) );
									var zoom = (half_size / half_size_to_show);
									var zoom = ( ( (zoom - 1) / 2 ) + 1 );
									// TODO
									// var zoom = (1 / ((cs.crop_width - cs.cover_spine) / cs.crop_width) );

									var left_percentages = ( (zoom - 1) / 2 )*100
									// TODO
									// set top position absolute
									// set image to left / right according to spine size if ltr or right according to bleed if rtl
									// set also top according to bleed
									// XXX var left_percentages = ( (cs.cover_spine / cs.crop_width) / 2 )*100;
									
									$(this).find("." + parent_class + "-image-container-image > img").css( { "transform": ( "scale(" + zoom + ")" ) } );	
									
									$(this).find("." + parent_class + "-image-container-image > img").css( { "left": ( left_percentages + "%" ) } );	
								}

							}

						}.bind(observerd[i].target),
					});

					// $(observerd[i].target).find(".product-link-image-container-image").attr("data-loaded", "true"); // => do not load again
					$(observerd[i].target).find("." + parent_class + "-image-container-image").attr("data-loaded", "true"); // => do not load again
				}
			
			} else {
				// $(observerd[i].target).find(".product-link-image-container-image").css( { "display": "none" } );
				$(observerd[i].target).find("." + parent_class + "-image-container-image").css( { "display": "none" } );
			}
		}
	}, options);

	return observer;

}
