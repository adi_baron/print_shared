
function mobile_ui_density_page_design_modal( options ) {
	options = options || {};
	options["modal"] = true;
	
	var html = handlebars_compile_and_execute("mobile-density-page-design", options );
	$(".mobile-modal").html(html);

  // $(".mobile-modal").removeClass("bright");
  // $(".mobile-modal").addClass("dark");
  $(".mobile-modal").removeClass("dark");
  $(".mobile-modal").addClass("bright")	;
	$(".mobile-modal").removeClass("hidden");

	$("#mobile-density-page-design-form").on('submit', function(ev) {
		ev.stopPropagation();
		ev.preventDefault();
		
		var val = $(this).find("input[name='density']:checked").val();
		options.cb(val);
		return false;
	});
}


function mobile_ui_spinner_modal() {
	var html = handlebars_compile_and_execute("mobile-waiting-spinner", {} );
	$(".mobile-modal").html(html);
  $(".mobile-modal").removeClass("hidden");
}

function mobile_ui_spinner_modal_close() {
	$(".mobile-modal").addClass("hidden");	
}

function mobile_ui_yes_no_modal_close() {
	$(".mobile-modal").removeClass("dark");
	$(".mobile-modal").addClass("bright");
	$(".mobile-modal").addClass("hidden");	
}

function mobile_ui_alert_modal(options) {
	var html = handlebars_compile_and_execute("mobile-alert-popup", {
		modal: (options.modal || true),
		title: ( (options.title === undefined) ? "missing title" : options.title ),
		txt: 	 (options.txt || "missing txt"),
		btn_style: (options.btn_style || ""),
	});

  $(".mobile-modal").html(html);
  
  $(".mobile-modal").removeClass("bright");
  $(".mobile-modal").addClass("dark")
  
  $(".mobile-modal").removeClass("hidden");

  $(".mobile-modal .close, .mobile-modal .modal-close-btn").click( function(ev) {
  	ev.preventDefault();
		ev.stopPropagation();

		mobile_ui_yes_no_modal_close();

		if (this.yes_cb) { this.yes_cb(); }
  }.bind(options));

}

function mobile_ui_yes_no_modal(options) {

	var html = handlebars_compile_and_execute("mobile-yes-no-popup", {
  	modal: (options.modal || true),
  	title: (options.title || "missing title"),
  	txt: 	 (options.txt || "missing title"),

  	sign: (options.sign || null),

  	yes_txt: (options.yes_txt || "yes"),
  	no_txt:  (options.no_txt || "no"),

  	btn_style: (options.btn_style || ""),
	});

  $(".mobile-modal").html(html);
  
  $(".mobile-modal").removeClass("bright");
  $(".mobile-modal").addClass("dark")
  
  $(".mobile-modal").removeClass("hidden");

  $(".mobile-modal .yes-no-popup .modal-yes-btn").click( function(ev) {
  	ev.preventDefault();
		ev.stopPropagation();

		mobile_ui_yes_no_modal_close();

  	if (this.yes_cb) { this.yes_cb(); }
  }.bind(options));

  $(".mobile-modal .yes-no-popup .modal-no-btn").click( function(ev) {
  	ev.preventDefault();
		ev.stopPropagation();

		mobile_ui_yes_no_modal_close();

  	if (this.no_cb) { this.no_cb(); }
  }.bind(options));

  $(".mobile-modal .close").click( function(ev) {
  	ev.preventDefault();
		ev.stopPropagation();

		mobile_ui_yes_no_modal_close();
  })
  

}
