
async function cache_store(storage_name, url, data) {
	
	if (typeof caches != "undefined") {

	  var res = new Response( 
	  	data, 
	  	{
				headers: {
	  		'content-type': 'text/plain'
				}
			}
		);

		await caches.open(storage_name).then(function(cache) {	    
	    cache.put(url, res);
		});

		return true;

	} else {
		return false;
	}

}

async function cache_fetch(storage_name, url) {

	if (typeof caches != "undefined") {
		const cache = await caches.open(storage_name);
		const response = await cache.match(url); 

		if (response) {
			let data = await response.text();
			return data;
		} else {
			return null;
		}
	} else {
		return null;
	}
	
}

async function cache_delete(storage_name, url) {

	if (typeof caches != "undefined") {
		const cache = await caches.open(storage_name);
		const res = await cache.delete(url);
		if (res == false) {
			console.warn("cached image did not delete");
		}
	}

}