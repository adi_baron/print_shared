var g_SocialGooglePhotos;
var g_SocialGoogleDrive;
var g_SocialFacebook;
var g_SocialGeneral;

function init_social_general() {

	if ( (!g_SocialGooglePhotos) && window["SocialGooglePhotos"] ) {
		g_SocialGooglePhotos = new window["SocialGooglePhotos"](
																	{
																		container: ".social-container-inner"
																	});
	}

}

function import_social_images( img_arr ) {
	g_uploader.add_social_files( img_arr );
}
