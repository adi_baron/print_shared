function SocialGooglePhotos( opts ) {
	
	this.medias_template = "mobile-social-google-photos";
	this.albums_template = "mobile-social-google-albums";

	SocialBase.call(this, opts);
}

SocialGooglePhotos.prototype = Object.create(SocialBase.prototype);
SocialGooglePhotos.prototype.constructor = SocialGooglePhotos;

SocialGooglePhotos.instance = function() {
	return window[this.name].singletone_instance;
}

SocialGooglePhotos.prototype.album_clicked = function(el) {
	var album_id = $(el).attr("data-id");

	g_api_photos_get_album_medias( {
    "albumId": album_id,
    "pageSize": 30,
    "cb": function(data) { 
    	// debugger;
    	if (data && (data.status == 200)) {
				data.result["album_id"] = this.album_id;
				var html = handlebars_compile_and_execute(this._this.medias_template, data.result);
				$(this._this.container).html(html);
    	}

    }.bind( { album_id: album_id, _this:this, } ),
	});	
}

SocialGooglePhotos.prototype.import_clicked = function() {
	
	// if(
	// 		(g_auth_user_is_signed_in() == true) &&
	// 		(g_auth_is_user_has_permissions(g_auth_current_user(), []) == true)
	// 	)
	// {
	
		// g_api_photos_get_medias(null, function(data) {
		g_api_photos_get_albums( function(data) {

			if (data && (data.status == 200)) {
				// TODO - implement conversion of data.result to a general format for all social medias integration
				console.log("TODO - implement conversion of data.result to a general format for all social medias integration");
				
				data.result.albums = [ { "title": "כל התמונות" } ].concat(data.result.albums);
				// var albums = [ { "title": "כל התמונות" } ].concat(data.result)

				// var html = handlebars_compile_and_execute(this.template, data.result);
				var html = handlebars_compile_and_execute(this.albums_template, data.result);
				// var html = handlebars_compile_and_execute(this.albums_template, albums);
				$(this.container).html(html);
			
			} else {
				console.error("google photos error getting medias");
				if (data.status == 401) {
					google_api_gsi_request_token( { 
						scope: 'https://www.googleapis.com/auth/photoslibrary.readonly',
						cb: function(response) {
							if (response.access_token) {
								this.import_clicked();
							}
						}.bind(this)
					});
				}
			}

		}.bind(this));

	// } else {
		
	// 	g_auth_sign_in( ["https://www.googleapis.com/auth/photoslibrary.readonly"], function(res) {
	// 		this.import_clicked();
	// 	}.bind(this) );

	// }

}
