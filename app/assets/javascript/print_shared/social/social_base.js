function SocialBase( opts ) {

	this.container = opts.container;

	// implements singeltone
	window[this.constructor.name].singletone_instance = this; 
}

SocialBase.prototype.constructor = SocialBase;

SocialBase.instance = function() {
	alert('single tone instance not implemented');
}
