function canvas_simulation_html(data) {


	var background_body 						= "#f4f4f4"
	var background_canvas_wig_type 	= "color"
	var background_canvas_wig 			= "transparent"
	
	if ( data.frame_type && (data.frame_type == 'black') ) {
		background_canvas_wig = "#000" 
	}
	
	
	// white
	if ( data.frame_type && (data.frame_type == 'white') ) {		
		background_canvas_wig = "#fff" 
	}

	if ( data.frame_type && (data.frame_type == 'wrap') ) {
		background_canvas_wig = "wrap" 
	}

	if ( data.frame_type && (data.frame_type == 'arava_light_wood') ) {
		background_canvas_wig_type = "image"
		background_canvas_wig = "url(#{image_url('arava_pine_wood.jpg')})" 
	}

	
	var background_canvas_main 			= "#e0e0e0"

	var canvas_width                 = data.width;
	var canvas_height                = data.height;

	// var canvas_ratio               	= (canvas_height.to_f / canvas_width.to_f).round(4)
	var canvas_ratio               	= 1/data.ratio

	// @canvas_wig_size           		= @product.product_group.frame_width
	// # @canvas_wig_size           		= 2.5
	var canvas_wig_size = 2.5
	

	// # we devide percentages by 2 ( / 2 ) while they are shown in 45% angle
	var canvas_wig_right_percentage	= ( ( (canvas_wig_size / canvas_width)*100 ) / 2);
	var canvas_wig_top_percentage		= ( ( (canvas_wig_size / canvas_height)*100 ) / 2);

	// var canvas_image_url             = @product.thumb_media.image_url(:url)
	var canvas_image_url            = data.thumb_media_url;

//	body {
//		margin:0;
//		padding:0;	
//		/*background-color: var(--main-bg-color);*/
//  }	

	
	var main_bg_color = background_body;
		
	var main_bg_canvas_wig = (background_canvas_wig == "wrap") ? "transparent" : background_canvas_wig;


	var main_bg_canvas_main	= background_canvas_main;

	// --main-canvas-relative-width        : <%=browser.device.mobile? ? ( (@canvas_ratio >= 1) ? "65vw" : "80vw" ) : ( (@canvas_ratio >= 1) ? "25vw" : "50vw" ) %>;
	main_canvas_relative_width = "100%";
	main_canvas_relative_width_percents = "100";

	var main_canvas_width = canvas_width;
	var	main_canvas_height =canvas_height;				

	var main_canvas_ratio = canvas_ratio;

	var main_canvas_wig_right_percentages = canvas_wig_right_percentage;
	var main_canvas_wig_right_half_percentages = (canvas_wig_right_percentage/2);
		
	var main_canvas_wig_top_percentages = canvas_wig_top_percentage;
	var main_canvas_wig_top_half_percentages = (canvas_wig_top_percentage/2);
		
	var main_image_url = "url(" + canvas_image_url + ")";
	
	var top_css_class = "canvas-simulation-" + Math.floor(Math.random() * 1000000);

	var css_str = "\
\
		<style>\
\
			." + top_css_class + "{\
				transform: rotateX(0deg) rotateY(-0deg) rotateZ(0deg);\
			}\
			." + top_css_class + ".ceter-color {\
				background-color:" + main_bg_canvas_main + ";\
			}\
\
			." + top_css_class + ", ." + top_css_class + " * {\
			  transform-style: preserve-3d;\
			}\
\
			." + top_css_class + " {\
			  background-image:" + main_image_url + ";\
\
			  background-size: cover;\
			  background-position: top right;\
\
			  margin: -15px auto auto;\
\
			  position: relative;\
\
			  width:" + main_canvas_relative_width + ";\
				padding-top: calc(" + main_canvas_relative_width + "*" + main_canvas_ratio + ");\
\
				box-shadow: 7px 7px 20px 0px rgb(0, 0, 0, .5);\
			}\
			." + top_css_class + " .top { \
			  display: block; \
			  content: \'\'; \
			  position: absolute; \
			  transform: scaleY(-1) skew(45deg, 0);\
			  transform-origin: 0% 50%;\
\
			  margin-left:" + main_canvas_wig_right_half_percentages + "%;\
\
			  top: calc( 0% - " + main_canvas_wig_top_percentages + "% + 0px);"


	if (background_canvas_wig == "wrap") {
  	css_str += "\
  		background-image:" + main_image_url + ";\
			filter: blur(3px);\
			-webkit-filter: blur(3px);\
\
			clip-path: inset(0px 0px 0px 0px) !important;"
  }

  css_str += "\
			  background-size: cover;\
			  background-position: top right;\
\
			  width: 100%;\
\
			  height:" + main_canvas_wig_top_percentages + "%;\
\
			}";

  css_str += "\
				." + top_css_class + " .top:after {\
			  	display: block;\
				  content: '';\
				  position: absolute;\
				  position: absolute;\
\
				  width: 100%;\
				  height: 100%;\
\
				  background:" + main_bg_canvas_wig + ";\
				}\
				."+ top_css_class + " .right {\
				  display: block;\
				  content: '';\
				  position: absolute;\
				  top:0px;\
\
				  transform: scaleX(-1) skew(0, 45deg);\
				  transform-origin: 0% 50%;\
\
				  margin-left:" + main_canvas_wig_right_percentages + "%;\
				  margin-top: calc( 0% - " + main_canvas_wig_right_percentages + "%);\
\
				  left: 100%;"
					  
if (background_canvas_wig == "wrap") {
	css_str += "\
					background-image:" + main_image_url + ";\
		  		filter: blur(3px);\
		  		-webkit-filter: blur(3px);\
\
  				clip-path: inset(0px 0px 0px 0px) !important;"
}
	
	css_str += "\
				 	background-size: cover;\
				 	background-position: top right;\
\
			  	width:" + main_canvas_wig_right_percentages + "%;\
\
				  height: 100%;\
				}\
				." + top_css_class + " .right:after {\
				  display: block;\
				  content: '';\
				  position: absolute;\
\
				  width: 100%;\
				  height: 100%;\
\
				  background:" + main_bg_canvas_wig + ";\
				}\
\
		</style>\
		"

	var html_str = css_str + " \
		<div class='" + top_css_class + " ceter-color' style=''>\
\
			<div class='top'></div>\
			<div class='right'></div>\
\
		</div>\
	";	

	// return (css_str + html_str);
	return html_str;

	if (false) {
		
		// <html>
			
		// 	<head>

		// 	</head>

		// 	<body style="/*for rotatio*/ perspective:80vw;     /*padding:50px;*/">
		// 		<!-- <div class="canvas horizontal-vertical-center-absolute ceter-color" style="/*width:12%; padding-top:12%;*/"> -->
					
		// 		<%-if params[:action] == "book"%>
		// 			<%-@margin="60px 60px"%>
		// 		<%else%>
		// 			<%-@margin = "60px auto"%>
		// 		<%end%>

		// 		<div class="canvas ceter-color" style="margin:<%=@margin%>; <%="transform:rotateY(22deg);" if ( @product.product_group.frame_type == Canvas::FRAME_TYPES[:arava_glass] ) %>">

		// 			<%-if ( @product.product_group.frame_type == Canvas::FRAME_TYPES[:arava_glass] )%>
		// 				<%=render :partial => "products/simulations/customers/arava/arava_glass"%>
		// 			<%end%>

		// 			<div class="top"></div>
		// 			<div class="right"></div>

		// 		</div>

		// 		<!-- 	
		// 					Style using 
		// 					https://www.nickmcburney.co.uk/blog/css-3d-picture-canvas-mockup/
		// 					https://codepen.io/NickMcBurney/pen/ygbvYg/
		// 		 -->
		// 		<style>

		// 			body {
		// 				margin:0;
		// 				padding:0;	
		// 				/*background-color: var(--main-bg-color);*/
		// 			}	

		// 			:root {
		// 				--main-bg-color											: <%=@background_body%>;
		// 				/*--main-bg-canvas-wig								: <%=@background_canvas_wig%>;*/
		// 				--main-bg-canvas-wig								: <%=(@background_canvas_wig == "wrap") ? "transparent" : @background_canvas_wig %>;*/

		// 				--main-bg-canvas-main								: <%=@background_canvas_main%>;

		// 				/*--main-canvas-relative-width        : <%=(@canvas_ratio >= 1) ? "35%" : "50%"%>;*/
		// 				/*--main-canvas-relative-width        : <%=(@canvas_ratio >= 1) ? "25vw" : "50vw"%>;*/
		// 				--main-canvas-relative-width        : <%=browser.device.mobile? ? ( (@canvas_ratio >= 1) ? "65vw" : "80vw" ) : ( (@canvas_ratio >= 1) ? "25vw" : "50vw" ) %>;

		// 				--main-canvas-width									: <%=@canvas_width%>;
		// 				--main-canvas-height								: <%=@canvas_height%>;				

		// 				--main-canvas-ratio       					: <%=@canvas_ratio.to_s%>;

		// 				--main-canvas-wig_right_percentages 			: <%=@canvas_wig_right_percentage%>%;
		// 				--main-canvas-wig_right_half_percentages 	: <%=@canvas_wig_right_percentage/2%>%;
						
		// 				--main-canvas-wig_top_percentages 				: <%=@canvas_wig_top_percentage%>%;
		// 				--main-canvas-wig_top_half_percentages 		: <%=@canvas_wig_top_percentage/2%>%;
						
		// 				--main-image-url													: url("<%=@canvas_image_url%>");
		// 			};

		// 			body {
		// 				position:relative;
		// 			}

		// 			.horizontal-vertical-center-absolute {
		// 			  position: absolute !important;
		// 			  left: 50%;
		// 			  top: 50%;
		// 			  transform: translate(-50%, -50%) !important;
		// 			  -webkit-transform: translate(-50%, -50%) !important;
		// 			}

		// 			.ceter-color {
		// 				background-color: var(--main-bg-canvas-main);
		// 			}

		// 			.canvas {
		// /*				width: 			 30%;
		// 				padding-top: calc( 30% * var(--main-canvas-ratio) );*/

		// 				/*transform: rotateX(0deg) rotateY(-10deg) rotateZ(0deg);*/
		// 				/*transform: rotateX(0deg) rotateY(-7deg) rotateZ(0deg);*/
		// 				transform: rotateX(0deg) rotateY(-0deg) rotateZ(0deg);*/

		// 			}

		// 			.canvas, .canvas * {
		// 			  transform-style: preserve-3d;
		// 	      /*perspective: 99vw;*/

		// 			}

		// 			.canvas {
		// 			  /*background-image: url(https://unsplash.it/560/280?image=1039);*/
		// 			  /*background-image: url("http://localhost:5010/uploads/2020-05-24/15-59-36-130/thub/sweet-ice-cream-photography-413305-unsplash(1).jpg");*/
		// 			  /*background-image: url("http://localhost:5010/uploads/2020-05-24/11-17-21-595/orig/omar-lopez-ibKI6M5XZz4-unsplash.jpg");*/
		// 			  background-image: var(--main-image-url);

		// 			  /*background:#ddd;*/
					  
		// 			  background-size: cover;
		// 			  background-position: top right;
					  
		// 			  /*margin: -6.5px auto auto;*/
		// 			  margin: -15px auto auto;

		// 			  /*position: absolute;*/
		// 			  position: relative;

		// /*			  width: 460px;
		// 			  height: 230px;*/
		// 			  width: 			 var(--main-canvas-relative-width);
		// 				padding-top: calc( var(--main-canvas-relative-width) * var(--main-canvas-ratio) );
					  
		// 			  /*box-shadow: 5px 0px 25px 3px #000;*/
		// 				/*box-shadow: 5px -3px 15px 6px rgba(0,0,0,.4)*/
		// 				box-shadow: 7px 7px 20px 0px rgb(0, 0, 0, .5);
		// 			  /*box-shadow: 5px -3px 12px 6px rgba(0,0,0,.3)*/
		// 			  /*box-shadow: 5px 0px 25px 3px #fff;*/
		// 			}
		// 			.canvas .top {
		// 			  display: block;
		// 			  content: "";
		// 			  position: absolute;
		// 			  transform: scaleY(-1) skew(45deg, 0);
		// 			  transform-origin: 0% 50%;
					  
		// 			  /*margin-left: 6.5px;*/
					  
		// 			  /*margin-left: 15px;*/
					  
		// 			  /*margin-left: 2%;*/
		// 			  /*margin-left: calc( var(--main-canvas-wig_top_percentages) / 2 );*/
		// 			  /*margin-left: var(--main-canvas-wig_top_half_percentages) ;*/
		// 			  margin-left: var(--main-canvas-wig_right_half_percentages) ;
					  
					  
		// 			  /*margin-left: var(--main-canvas-wig_top_percentages);
					  
		// 			  /*top: -13px;*/
					  
		// 			  /*top: -30px;*/
					  
		// 			  /*top: -4%;*/
		// 			  top: calc( 0% - var(--main-canvas-wig_top_percentages) + 0px);
					  
		// 			  /*background-image: url(https://unsplash.it/560/280?image=1039);*/
		// 			  /*background-image: url("http://localhost:5010/uploads/2020-05-24/15-59-36-130/thub/sweet-ice-cream-photography-413305-unsplash(1).jpg");*/
		// 			  <%-if (@background_canvas_wig == "wrap")%>
		// 			  	background-image: var(--main-image-url);
		// 			  	/* Add the blur effect */
		//   				filter: blur(4px);
		//   				-webkit-filter: blur(4px);

		//   				clip-path: inset(0px 0px 0px 0px) !important;
		// 			  <%end%>


		// 			  background-size: cover;
		// 			  background-position: top right;
					  
		// 			  /*width: 460px;*/
		// 			  width: 100%;
					  
		// 			  /*height: 13px;*/
					  
		// 			  /*height: 30px;*/
					  
		// 			  /*height: 4%;*/
		// 			  height: var(--main-canvas-wig_top_percentages);
		// 			}
		// 			.canvas .top:after {
		// 			  display: block;
		// 			  content: "";
		// 			  position: absolute;
		// 			  position: absolute;
					  
		// 			  /*width: 460px;*/
		// 			  width: 100%;
					  
		// 			  /*height: 13px;*/
					  
		// 			  /*height: 30px;*/
		// 			  height: 100%;
					  
		// 			  /*background: rgba(0, 0, 0, 0.25);*/
		// 			  background: var(--main-bg-canvas-wig);
		// 			}
		// 			.canvas .right {
		// 			  display: block;
		// 			  content: "";
		// 			  position: absolute;
		// 			  top:0px;

		// 			  transform: scaleX(-1) skew(0, 45deg);
		// 			  transform-origin: 0% 50%;
					  
		// 			  /*margin-left: 13px;
		// 			  margin-top: -13px;*/
					  
		// 			  /*margin-left: 30px;
		// 			  margin-top: -30px;*/
					  
		// 			  /*margin-left: 3%;*/
		// 			  /*margin-top: -3%;*/
					  
		// 			  margin-left: var(--main-canvas-wig_right_percentages) ;
		// 			  margin-top: calc( 0% - var(--main-canvas-wig_right_percentages) );
		// 			  /*top: calc( 0% - var(--main-canvas-wig_top_percentages) );*/
					  

		// 			  left: 100%;
					  
		// 			  /*background-image: url(https://unsplash.it/560/280?image=1039);*/
		// 			  <%-if (@background_canvas_wig == "wrap")%>
		// 			  	background-image: var(--main-image-url);
		// 			  	/*background-image: url("http://localhost:5010/uploads/2020-05-24/15-59-36-130/thub/sweet-ice-cream-photography-413305-unsplash(1).jpg");*/

		// 			  	/* Add the blur effect */
		//   				filter: blur(4px);
		//   				-webkit-filter: blur(4px);

		//   				clip-path: inset(0px 0px 0px 0px) !important;
		// 			  <%end%>

		// 			  background-size: cover;
		// 			  background-position: top right;
					  
		// 			  /*width: 13px;*/
					  
		// 			  /*width: 30px;*/
		// 			  /*width: 3%;*/
		// 			  width: var(--main-canvas-wig_right_percentages);

		// 			  /*height: 230px;*/
		// 			  height: 100%;
		// 			}
		// 			.canvas .right:after {
		// 			  display: block;
		// 			  content: "";
		// 			  position: absolute;
					  
		// 			  /*width: 13px;*/
		// 			  /*width: 30px;*/
		// 			  width: 100%;

		// 			  /*height: 230px;*/
		// 			  height: 100%;
					  
		// 			  /*background: rgba(0, 0, 0, 0.38);*/
		// 			  background: var(--main-bg-canvas-wig);
		// 			}			


		// 		</style>

		// 	</body>
		// </html>

	}

	

}